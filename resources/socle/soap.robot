*** Settings ***
Documentation
...                 Cette ressource met à disposition les mots-clé pour réaliser facilement des appels SOAP

Library           SoapLibrary
Library           String

Resource          Log.robot
Resource          xml.robot
Resource          %{CNP_SOAP_CONFIGURATIONS}

*** Keywords ***
Créer un client SOAP
    [Arguments]  ${composant}  ${environnement}  ${serviceName}  ${ssl_verify}=${False}

    ${soapUrl}=     Set Variable  ${${composant}_${environnement}.baseUrl}${${composant}_${environnement}.sercicesUri}${serviceName}
    Set Test Variable    ${soapUrl}

    Create Soap Client  ${soapUrl}?wsdl    ssl_verify=${ssl_verify}

Faire un appel SOAP
    [Documentation]  Permet de réaliser un appel Appel depuis le client SOAP qui a été précédemment créer
    ...              `xml_root` : est un objet `XML.Parse Xml`, (lxml.etree)
    ...              `activateLog (facultatif)` : Booléen permettant de tracer un appel ou non. Par défaut, les traces sont actives.
    [Arguments]      ${xml_root}  ${activateLog}=${True}

    ${xml_string}    Element To String  ${xml_root}

    # Effectuer l'appel
    ${response}             SoapLibrary.Call SOAP Method With String XML      ${xml_string}     status=anything
    ${responseString}       Element To String    ${response}
    ${prettyResponse}       xml.Indenter une chaîne XML    ${responseString}

    IF    ${activateLog}
        Log.Debug    Appel SOAP ${soapUrl}\n${xml_string}   darkblue
        Log.Debug    Réponse SOAP\n${prettyResponse}        darkblue
    END

    # Pareil que le retour de `XML.Parse XML`
    [Return]  ${response}

Générer L'URI de base
    [Documentation]    Fixer l'url de base donnant accès au service concerné
    ...                (Exemples: idd, id1 ...)

    [Arguments]    ${service_concerne}

    ${environnement}        Convert To Upper Case    %{ENVIRONMENT}
    IF  '%{SILO}' != 'No_silo'
        ${silo}     Convert To Upper Case    %{SILO}
        ${environnement}    Catenate    SEPARATOR=${EMPTY}  ${environnement}  ${silo}
    END

    ${url}=    Collections.Get From Dictionary    ${SOAP_${service_concerne}}    ${environnement}
    [Return]    ${url}


Créer la connexion SOAP
    [Documentation]    Se connecter au service concerné avec l'url de base donnant accès au service concerné

    [Arguments]    ${url}    ${ssl_verify}=False
    Set Test Variable     ${soapUrl}  ${url}
    Create Soap Client    ${url}    ssl_verify=${ssl_verify}
