# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource wrapper de la librairie String de base.
...
...                 Le fichier string.robot permet de centraliser la bibliothèque [http://robotframework.org/robotframework/latest/libraries/String.html|String].

Library     String
Library     base64

*** Keywords ***
Supprimer Les Retours A La Ligne
    [Documentation]     Prendre une chaîne de caractères avec plusieurs lignes où chaque ligne est une case de la liste.
    ...
    ...                 *Arguments :*
    ...                 - ``myVar``    plusieurs mots/phrases a la lignes
    ...                 *Return* : une liste comportant les lignes
    ...
    ...                 Exemple :
    ...                 | _myVar_ vaut :
    ...                 | \ \ \ nomFichier1
    ...                 | \ \ \ nomFichier2
    ...                 | \ \ \ nomFichier3
    ...                 *Return* renvoie :
    ...                 | nomFichier1 | nomFichier2 | nomFichier3 |
    [Arguments]   ${myString}

    ${stringList}=    String.Split To Lines    ${myString}

    [Return]    @{stringList}


Faire Une Liste Par Rapport A Un Separateur
    [Documentation]     Récuperer une variable avec plusieurs mots/phrases séparés par un séparateur
    ...                 pour la transformer en une liste.
    ...
    ...                 *Arguments :*
    ...                 - ``myString``          est la chaîne de caractères.
    ...                 - ``mySeparateur``      est le séparateur à supprimer.
    ...                 *Return* : une liste sans le séparateur ``mySeparateur``.
    ...
    ...                 Exemple :
    ...                 | _myVar_ vaut : \ \ \ nomFichier1, nomFichier2, nomFichier3
    ...                 *Return* renvoie :
    ...                 | nomFichier1 | nomFichier2 | nomFichier3 |
    [Arguments]   ${myString}   ${mySeparator}

    ${myString}=        string.Supprimer les espaces    ${myString}
    ${stringList}=      String.Split String    ${myString}   ${mySeparator}

    [Return]    @{stringList}


Supprimer Les Espaces
    [Documentation]     Récupérer une variable avec plusieurs mots/phrases séparés par un espace
    ...                 pour la transformer en un mot sans espace.
    ...
    ...                 *Arguments :*
    ...                 - ``myStringWithSpaces``    est la chaîne de caractères
    ...                 *Return* : la chaîne de caractères ``myStringAvecEspace`` sans espace.
    ...
    ...                 Exemple :
    ...                 | _myStringWithSpaces_ vaut : \ \ \ \ \ nomFichier1, nomFichier2, nomFichier3
    ...                 | *Return* renvoie : \ \ \ \ \ \ \ \ \ \ \ \ \ \ nomFichier1,nomFichier2,nomFichier3
    [Arguments]   ${myStringWithSpaces}

    ${stringListWithoutSpace}=    String.Strip String    ${myStringWithSpaces}

    [Return]    ${stringListWithoutSpace}

Encoder en base 64
    [Documentation]     Récupérer une variable avec plusieurs mots/phrases séparés par un espace
    ...                 pour la transformer en un mot sans espace.
    ...
    ...                 *Arguments :*
    ...                 - ``myStringWithSpaces``    est la chaîne de caractères
    ...                 *Return* : la chaîne de caractères ``myStringAvecEspace`` sans espace.
    ...
    ...                 Exemple :
    ...                 | _myStringWithSpaces_ vaut : \ \ \ \ \ nomFichier1, nomFichier2, nomFichier3
    ...                 | *Return* renvoie : \ \ \ \ \ \ \ \ \ \ \ \ \ \ nomFichier1,nomFichier2,nomFichier3
    [Arguments]   ${myStringToEncode}

    ${encodedString}=   String.Encode String To Bytes   ${myStringToEncode}  UTF-8
    ${stringBase64}=    base64.standard_b64encode  ${encodedString}

    [Return]    ${stringBase64}


Verifier qu'une chaine de caracteres n'est pas vide
    [Arguments]    ${myString}    ${myError}=La chaîne de caractères est vide

    ${taille}    Get Length    ${myString}
    Run Keyword If    ${taille} == 0    FAIL    ${myError}
