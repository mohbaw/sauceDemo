*** Settings ***
Documentation       Ressource de référence pour Appium et selenium.
# ...
# ...                 Cette libraire propose une forme d'héritage vers Appium et selenium.
# ...                 Les mots-clés conçus sont en communs avec ceux de ``appium.robot``
# ...                 et de ``selenium.robot``.
# ...
# ...                 Avec ``web.robot``, nous sommes en mesure d'effectuer les tests
# ...                 aussi bien sur navigateur d'ordinateur que sur un appareil mobile.
# ...                 Cependant, certains mots-clés n'existent pas dans l'une ou l'autre librairie.
# ...
# ...                 Ci-dessous un tableau indiquant les mots-clés fonctionnels dans un seul environnement.
# ...                 | = Selenium uniquement = | = Appium uniquement = |
# ...                 | Obtenir Mon Url | Obtenir L'Uri Vers Appium Server |
# ...                 | Obtenir Mon Titre | Obtenir La Plateforme Du Device |
# ...                 | Obtenir La Cellule Du Tableau | Ouvrir L'Application Sur Le Device |
# ...                 | Obtenir Mon Url | Deployer Et Ouvrir L'Application Sur Le Device |
# ...                 | Choisir Le Fichier | Taper Sur l'Element |
# ...                 | Choisir Dans La Liste |  |
# ...                 | Choisir Dans La Liste Unique |  |
# ...                 | Choisir Le Bouton Radio |  |
# ...                 | Cocher Un Element |  |
# ...                 | Changer De Navigateur |  |
# ...
# ...                 Pour tous ces mots-clés il est préférable de les appeler directement depuis
# ...                 leur propre librairie plutôt que depuis ``web.robot`` et indiquer qu'un de
# ...                 ces mots-clés fait parti du test tant que les deux librairies n'ont pas les
# ...                 équivalents pour tous. Autrement, le test échouerait car il ne trouverait
# ...                 pas l'équivalence dans l'autre fichier.

Resource            log.robot
Resource            utils.robot
# Resource            appium.robot
Resource            selenium2.robot


*** Keywords ***
# A réactiver une fois la partie appium refondue

# Sur Mon Navigateur
#     [Documentation]     Aiguillage en fonction de la variable Jenkins MY_DEVICE.
#     ...
#     ...                 Navigateur Selenium ou Appium à partir du dictionnaire Jenkins MY_ENVIRONMENT_SETTINGS.
#     ...
#     ...                 Par défaut le DEVICE est PC.
#     ...
#     ...                 *Arguments :*
#     ...                 - ``myKeyword``       est le mot-clé a appeler dans la librairie aiguillée.
#     ...                 - ``myVarargs``       sont les arguments à ajouter lors de l'appel au mot-clé.
#     ...                 *Return* : le résultat de la requete du mot cle.
#     [Arguments]         ${keyword}    @{varargs}
#
#     ${theDevice}=    OperatingSystem.Get Environment Variable    DEVICE    default=PC
#     ${passed}    ${libraryName}=    Run Keyword And Ignore Error    Get From Dictionary    ${CNP_DEVICES}    ${theDevice}libraryName
#     # compatibilite ascendante - Si le dictionnaire n'existe pas,  prendre selenium par defaut
#     ${libraryName}=    Set Variable If  '${passed}'=='PASS'             ${libraryName}    selenium
#     ${returnValue}=    Run Keyword      ${libraryName}.${myKeyword}     @{myVarargs}
#
#     [Return]    ${returnValue}



Ouvrir Navigateur
    [Documentation]    Ouvre le navigateur, conditionné par la variable d'environnement ``%{NAVIGATEUR}``.
    ...                Explicitement les valeurs peuvent être:
    ...                - ``Chrome``
    ...                - ``Chromium``
    ...                - ``Firefox``
    ...                - ``Internet Explorer``
    ...                - ``Edge`` (Bientôt)
    ...
    ...                Si ``url`` est renseignée, positionne le navigateur sur cette dernière.\n\t
    ...                Si ``uri`` est renseignée, ajoute cette portion à ``url``.\n\t
	...                Si ``texte_attendu`` est renseigné, vérifie la présence du texte
    ...                une fois positionné.\n\t
    ...
    ...                Par défaut avec ``url`` non renseignée, l'application CNP concernée sera la cible.
    ...
    ...                Les variables d'environnement ``%{PROJECT_NAME}`` et ``%{ENVIRONNEMENT}`` permettent
    ...                de retrouver l'URL de cette application. Le mécanisme est décrit dans `Naviguer Vers`.
    ...
    ...                *Exemples*
    ...
    ...                On considère la présence du fichier de configuration contenant les URLs des applications CNP
    ...                et les variables d'environnement suivantes avec leurs valeurs :
    ...                - NAVIGATEUR=Chrome
    ...                - PROJECT_NAME=ADELE
    ...                - ENVIRONNEMENT=RE7
    ...
    ...
    ...                | Ouvrir Navigateur |
    ...                | - Ouvre un navigateur Chrome positionné sur l'URL de l'environnement de recette de l'application Adele et
    ...                | maximise la fenêtre du navigateur.
	...
    ...                | Ouvrir Navigateur | uri=la-banque-postale |
    ...                | - Ouvre un navigateur Chrome positionné sur l'URL de l'environnement de recette de l'application Adele avec
    ...                | l'URI fournie (https://wsi-re7.cnp.fr/wsi-web/la-banque-postale).
	...
    ...                | Ouvrir Navigateur | maximiser=False | texte_attendu=@dèle |
    ...                | - Ouvre un navigateur Chrome positionné sur l'URL de l'environnement de recette de l'application Adele,
    ...                | ne maximise pas la fenêtre du navigateur et vérifie la présence du texte @dèle sur la page.
    ...
    ...                | Ouvrir Navigateur | navigateur=Firefox | url=https://www.cnp.fr/particuliers | texte_attendu=CNP Assurances | alias=CNP |
    ...                | - Ouvre un navigateur Firefox repéré par l'alias `CNP`, positionné sur l'URL https://www.cnp.fr/particuliers,
    ...                | maximise la fenêtre du navigateur et vérifie la présence du texte CNP Assurances sur la page.

    [Arguments]        ${navigateur}=%{NAVIGATEUR}
    ...                ${url}=${EMPTY}
    ...                ${uri}=${EMPTY}
    ...                ${maximiser}=${True}
    ...                ${texte_attendu}=${EMPTY}
    ...                ${alias}=${None}

    [Tags]             Socle

    selenium2.Ouvrir Navigateur    navigateur=${navigateur}
    ...                            url=${url}
    ...                            uri=${uri}
    ...                            maximiser=${maximiser}
    ...                            texte_attendu=${texte_attendu}
    ...                            alias=${alias}


# ...               %{IHM_URL_ENV_LIST}
# ...               %{CONF_LIST_URL_ENV}
Naviguer Vers
    [Documentation]    Positionne le navigateur déjà ouvert et actif (voir `Ouvrir Navigateur`)
    ...                sur ``url`` fournie avec ``uri`` si renseignée.
	...
    ...                Si ``texte_attendu`` est renseigné, vérifie alors la présence
    ...                du texte sur la page une fois atteinte.
    ...
    ...                *Mécanisme de choix d'environnements CNP*
    ...
    ...                L'URL cible d'un environnement d'une application CNP, est défini
    ...                par les variables d'environnement ``%{ENVIRONNEMENT}`` et ``%{PROJECT_NAME}``.
    ...
    ...                Pour que cette URL soit trouvée, il faut charger le fichier de configuration
    ...                repéré par la variable d'environnement ``%{CNP_URL_ENV_LIST}``.
    ...
    ...                *Fichier de configuration*
    ...
    ...                Le fichier de configuration comporte un dictionnaire par application CNP,
    ...                nommé ``APP_%{PROJECT_NAME}``. Dans chacun de ces dictionnaires,
    ...                chaque clé correspond à un environnement, et a pour valeur l'URL de ce dernier.
    ...
    ...                Exemple avec l'application Adele :
    ...
    ...                | &{APP_ADELE}
    ...                | ...\t    VFG=https://wsi-mid-vfg00001.angers.cnp.fr:20443/wsi-web/
    ...                | ...\t    RE7=https://wsi-re7.cnp.fr/wsi-web/
    ...                | ...\t    PREPROD=https://wsi-preprod.cnp.fr/wsi-web/
    ...
    ...                Ce fichier est disponible ... EMPLACEMENT A UNIFIER ...
    ...
    ...                *Exemples*
    ...
    ...                On considère la présence du fichier de configuration contenant les URLs des applications CNP
    ...                et les variables d'environnement suivantes avec leurs valeurs :
    ...                - PROJECT_NAME=ADELE
    ...                - ENVIRONNEMENT=RE7
    ...                | Naviguer Vers | url=https://www.cnp.fr/particuliers | texte_attendu=CNP Assurances |
    ...                | Naviguer Vers | uri=la-banque-postale |
    ...                | Naviguer Vers |
    ...
    ...                | - Positionne le navigateur actif sur l'URL https://www.cnp.fr/particuliers, et vérifie la présence
    ...                |  du texte CNP Assurances sur la page.
    ...                |
    ...                | - Positionne le navigateur actif sur l'URL de l'environnement de recette de l'application Adele avec
    ...                | l'URI fournie (https://wsi-re7.cnp.fr/wsi-web/la-banque-postale).
    ...                |
    ...                | - Positionne le navigateur actif sur l'URL de l'environnement de recette de l'application Adele.

    [Arguments]        ${url}=${EMPTY}
    ...                ${uri}=${EMPTY}
    ...                ${texte_attendu}=${EMPTY}

    [Tags]             Socle

    selenium2.Naviguer Vers   url=${url}
    ...                       uri=${uri}
    ...                       texte_attendu=${texte_attendu}


HOTFIX VDI Reboot User Technique
    [Documentation]    Mot clé qui devrait être illégal.
    ...                Il faut résoudre le souci de résolution d'écran.
    ...                Appel du mot-clé ``Wait Until CNP Succeeds``...

    [Arguments]        ${largeur}
    ...                ${hauteur}

    [Tags]             Socle

    SeleniumLibrary.Set Window Size    ${largeur}
    ...                                ${hauteur}


Basculer Vers Navigateur
    [Documentation]    Bascule le focus du navigateur actif vers le navigateur ciblé par ``alias``.\n\t
    ...                ``alias`` peut contenir soit l'alias du navigateur cible soit son index.

    [Arguments]        ${alias}

    [Tags]             Socle

    selenium2.Basculer Vers Navigateur    ${alias}


Basculer Vers Onglet
    [Documentation]    Bascule le focus de l'onglet actif vers l'onglet ciblé par ``locator``.\n\t
    ...                ``locator`` peut contenir :
    ...                - *NEW* : correspondant au dernier onglet ouvert
    ...                - *CURRENT* : retournant juste l'identifiant de l'onglet actuel
    ...                - un titre de page (Nom de l'onglet ou title)
    ...                - un identifiant d'onglet (Window Handle)
    ...                - une URL
    ...                Retourne l'identifiant de l'onglet précédent la bascule.

    [Arguments]        ${locator}

    [Tags]             Socle

    ${onglet_precedent}=    selenium2.Basculer Vers Onglet    ${locator}

    [Return]    ${onglet_precedent}


Fermer Onglet
    [Documentation]    Ferme l'onglet actif.

    [Tags]             Socle

    selenium2.Fermer Onglet


Fermer Navigateur
    [Documentation]    Ferme le navigateur actif.

    [Tags]             Socle

    selenium2.Fermer Navigateur


Fermer Tous Navigateurs
    [Documentation]    Ferme tous les navigateurs ouverts par le robot.

    [Tags]             Socle

    selenium2.Fermer Tous Navigateurs


# Basiques Navigation
Actualiser Page
    [Documentation]    Rafraîchi la page du navigateur actif.

    [Tags]             Socle

    selenium2.Actualiser Page


Faire Precedent
    [Documentation]    Simule un appui sur le bouton `Retour/Précédent` du navigateur actif.

    [Tags]             Socle

    selenium2.Faire Precedent


Faire Defiler Page
    [Documentation]    Fait défiler la page dans la direction souhaitée.\n\t
    ...                ``direction`` peut avoir les valeurs suivantes :
    ...                - un locator pour défiler jusqu'à l'élément ciblé
    ...                - *haut* : défile jusqu'en haut de la page
    ...                - *bas* : défile jusqu'en bas de la page

    [Arguments]        ${direction}

    [Tags]             Socle

    selenium2.Faire Defiler Page     ${direction}


# Récupération Données
Obtenir URL Page
    [Documentation]    Retoune l'URL de la page.

    [Tags]             Socle

    ${url}=    selenium2.Obtenir URL Page

    [Return]    ${url}


Obtenir Titre Page
    [Documentation]    Retourne le titre de la page.

    [Tags]             Socle

    ${titre}=    selenium2.Obtenir Titre Page

    [Return]    ${titre}


Obtenir Texte Element
    [Documentation]    Retourne le texte contenu dans l'élément ciblé par ``locator``.

    [Arguments]        ${locator}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    ${texte}=    selenium2.Obtenir Texte Element    ${locator}
    ...                                            ${timeout}
    ...                                            ${erreur}

    [Return]    ${texte}


Obtenir Valeur Attribut Element
    [Documentation]    Retourne la valeur de l' ``attribut`` contenu dans l'élément ciblé par ``locator``.

    [Arguments]        ${locator}
    ...                ${attribut}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    ${attribut}=    selenium2.Obtenir Valeur Attribut Element    ${locator}
    ...                                                         ${attribut}
    ...                                                         ${timeout}
    ...                                                         ${erreur}

    [Return]    ${attribut}


Obtenir Nombre Correspondances
    [Documentation]    Retourne le nombre de correspondances par rapport au ``locator``.

    [Arguments]        ${locator}

    [Tags]             Socle

    ${nombre}=    selenium2.Obtenir Nombre Correspondances    ${locator}

    [Return]    ${nombre}



# Interactions
Saisir Dans Champ
    [Documentation]     Saisi du texte ou un secret dans un champ.
    ...
    ...                 Il faut renseigner soit l'argument texte qui est le cas général, soit l'argument secret pour ne pas
    ...                 tracer le contenu du secret dans les logs
    ...
    ...                 Par défaut pour les deux modes, il y a une vérification que le champ est visible à l'écran et il est
    ...                 effacé avant saisie. Si on ne souhaite pas que ces actions soient réalisées, il faut renseigner les
    ...                 arguments visible et/ou effacer à False.
    ...
    ...                 Il est possible d'effectuer la saisie avec javascript en renseignant l'argument javascript à True.
    ...
    ...
    ...                 *=> Il est fortement recommandé de surcharger les messages d'erreur par un message d'erreur fonctionnel !*\n\n
    ...                 *!!! Il faudrait probablement indiquer cette partie en préambule plutôt que dans les mots clés !!!*
    ...                 *Idem pour le fonctionnement des timeout*
    ...
    ...                 *Exemples*
    ...
    ...                 | Saisir Dans Champ | locator=//a[@id="id_connexion"] | texte=identifiant | erreur=Impossible de saisir l'id |
    ...                 | Saisir Dans Champ | locator=id:mdp_connexion | secret=mdp |
    ...                 | Saisir Dans Champ | locator=name:montant | effacer=False | texte=0 |
    ...                 | Saisir Dans Champ | locator=//input | visible=False | texte=test | javascript=True |
    ...
    ...                 | - Saisi dans le champ ciblé par le xpath "//a[@id="id_connexion"]" le texte "identifiant" après avoir
    ...                 |  effacé le texte existant, logue le contenu du texte et remplace le message d'erreur par "Impossible de
    ...                 |  saisir l'id" en cas d'échec.
    ...                 |
    ...                 | - Saisi dans le champ ciblé par l'id "mdp_connexion" le secret "mdp" après avoir effacé le texte
    ...                 |  existant et ne logue pas le contenu du secret.
    ...                 |
    ...                 | - Saisi dans le champ ciblé par le name "montant" le texte "0" sans effacer le texte existant et
    ...                 |  logue le contenu du texte.
    ...                 |
    ...                 | - Saisi dans le champ ciblé par le xpath "//input" le texte "test" sans avoir vérifié au préalable
    ...                 |  que le champ est visible à l'écran après avoir effacé le texte existant, en utilisant javascript et
    ...                 |  logue le contenu du texte.

    [Arguments]         ${locator}
    ...                 ${texte}=${EMPTY}
    ...                 ${secret}=${EMPTY}
    ...                 ${effacer}=${True}
    ...                 ${visible}=${True}
    ...                 ${javascript}=${False}
    ...                 ${sortir}=${False}
    ...                 ${timeout}=${None}
    ...                 ${erreur}=${None}

    [Tags]              Socle

    selenium2.Saisir Dans Champ    locator=${locator}
    ...                            texte=${texte}
    ...                            secret=${secret}
    ...                            effacer=${effacer}
    ...                            visible=${visible}
    ...                            javascript=${javascript}
    ...                            sortir=${sortir}
    ...                            timeout=${timeout}
    ...                            erreur=${erreur}


Cliquer Sur Element
    [Documentation]    Clique sur l'élément ciblé par ``locator``.\n\t

    [Arguments]        ${locator}
    ...                ${visible}=${True}
    ...                ${javascript}=${False}
    # ...                 ${touche}
    ...                ${timeout}=${None}      # Same value than SeleniumLibrary
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Cliquer Sur Element    locator=${locator}
    ...                             visible=${visible}
    ...                             javascript=${javascript}
    ...                             timeout=${timeout}
    ...                             erreur=${erreur}


Selectionner Dans Liste
    [Documentation]    Sélectionne une ``option`` dans une liste déroulante
    ...                ciblée par ``locator``.\n\t
    ...                Par défaut, sélectionne un label dans une liste.\n\t
    ...                ``valeur`` permet de plutôt sélectionner une valeur
    ...                si à ``${True}``.\n\t
    ...                ``option`` est utilisé dans ces deux cas.

    [Arguments]        ${locator}
    ...                ${option}
    ...                ${valeur}=${False}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Selectionner Dans Liste    ${locator}
    ...                                 ${option}
    ...                                 ${valeur}
    ...                                 ${timeout}
    ...                                 ${erreur}


Selectionner Bouton Radio
    [Documentation]    Sélectionne la ``valeur`` du bouton radio ciblé par ``name``.\n\t
    ...                ``name`` correspond au name du groupe radio bouton.

    [Arguments]        ${name}
    ...                ${valeur}

    [Tags]             Socle

    selenium2.Selectionner Bouton Radio    ${name}
    ...                                   ${valeur}


Cocher Element
    [Documentation]    Coche la checkbox ciblée par ``locator``.\n\t
    ...                Ne fait rien si la checkbox est déjà cochée.

    [Arguments]        ${locator}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Cocher Element    ${locator}
    ...                        ${timeout}
    ...                        ${erreur}


Decocher Element
    [Documentation]    Décoche la checkbox ciblée par ``locator``.\n\t
    ...                Ne fait rien si la checkbox est déjà décochée.

    [Arguments]        ${locator}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Cocher Element    ${locator}
    ...                        ${timeout}
    ...                        ${erreur}


Deposer Fichier Dans Element
    [Documentation]    Dépose un fichier situé à l'emplacement ``chemin_fichier`` dans l'élément ciblé par ``locator``.\n\t
    ...                De la même façon qu'un utilisateur aurait spécifié un fichier à télécharger, depuis la fenêtre pop-up `Ouvrir` du navigateur.

    [Arguments]        ${locator}
    ...                ${chemin_fichier}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Deposer Fichier Dans Element    ${locator}
    ...                                      ${chemin_fichier}
    ...                                      ${timeout}
    ...                                      ${erreur}


Appuyer Sur Touche
    [Documentation]    TODO

    [Arguments]        ${locator}
    ...                ${ascii_code}


Positionner Curseur Sur Element
    [Documentation]    Positionne le curseur sur l'élément ciblé par ``locator``.

    [Arguments]        ${locator}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Positionner Curseur Sur Element    ${locator}
    ...                                         ${timeout}
    ...                                         ${erreur}


Selectionner Frame
    [Documentation]    Sélectionne une frame ou iframe.

    [Arguments]        ${locator}

    [Tags]             Socle

    selenium2.Selectionner Frame  ${locator}


Deselectionner Frame
    [Documentation]    Désélectionne une frame ou iframe.

    [Tags]             Socle

    selenium2.Deselectionner Frame



# Verif
Capturer Ecran
    [Documentation]    Effectue une capture d'écran de l'onglet actif.

    [Tags]             Socle

    selenium2.Capturer Ecran


URL Page Doit Correspondre
    [Documentation]    Vérifie que l'URL de la page correspond exactement à ``url``.

    [Arguments]        ${url}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.URL Page Doit Correspondre    ${url}
    ...                                    ${timeout}
    ...                                    ${erreur}


URL Page Doit Contenir
    [Documentation]    Vérifie que l'URL de la page contient l' ``uri``.

    [Arguments]        ${uri}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.URL Page Doit Contenir    ${uri}
    ...                                ${timeout}
    ...                                ${erreur}


Titre Page Doit Correspondre
    [Documentation]    Vérifie que le titre de la page correspond exactement à ``titre``.

    [Arguments]        ${titre}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Titre Page Doit Correspondre    ${titre}
    ...                                      ${erreur}


Page Doit Contenir
    [Documentation]    Vérifie que la page contient ``texte``.

    [Arguments]        ${texte}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Page Doit Contenir    ${texte}
    ...                            ${timeout}
    ...                            ${erreur}


Page Ne Doit Pas Contenir
    [Documentation]    Vérifie que la page ne contient pas ``texte``.

    [Arguments]        ${texte}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Page Ne Doit Pas Contenir    ${texte}
    ...                                   ${timeout}
    ...                                   ${erreur}


Page Doit Contenir Element
    [Documentation]    Vérifie que la page contient l'élément ciblé par ``locator``.\n\t
    ...                ``visible`` permet de vérifier également que l'élément est visible à l'écran.

    [Arguments]        ${locator}
    ...                ${visible}=${False}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Page Doit Contenir Element    ${locator}
    ...                                    ${visible}
    ...                                    ${timeout}
    ...                                    ${erreur}


Page Ne Doit Pas Contenir Element
    [Documentation]    Vérifie que la page ne contient pas l'élément ciblé par ``locator``.\n\t
    ...                Si ``visible`` est à ``${True}``, vérifie que la page ne contient
    ...                pas l'élément visible (indisponible à l'écran).

    [Arguments]        ${locator}
    ...                ${visible}=${False}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Page Ne Doit Pas Contenir Element    ${locator}
    ...                                           ${visible}
    ...                                           ${timeout}
    ...                                           ${erreur}



# Elements
Texte Element Doit Correspondre
    [Documentation]    Vérifie que le texte ciblé par ``locator`` correspond à ``texte``.

    [Arguments]        ${locator}
    ...                ${texte}

    [Tags]             Socle

    selenium2.Texte Element Doit Correspondre    ${locator}
    ...                                         ${texte}


Texte Element Doit Correspondre Regex
    [Documentation]    Vérifie que le texte ciblé par ``locator`` correspond à l'expression régulière ``regex``.

    [Arguments]        ${locator}
    ...                ${regex}

    [Tags]             Socle

    selenium2.Texte Element Doit Correspondre Regex    ${locator}
    ...                                               ${regex}


Bouton Radio Doit Etre Selectionne
    [Documentation]    Vérifie que le bouton radio ciblé par ``name`` est bien sélectionné sur ``valeur``.\n\t
    ...                ``name`` correspond au name du groupe radio bouton.

    [Arguments]        ${name}
    ...                ${valeur}

    [Tags]             Socle

    selenium2.Bouton Radio Doit Etre Selectionne    ${name}
    ...                                            ${valeur}


Bouton Radio Ne Doit Pas Etre Selectionne
    [Documentation]    Vérifie que le bouton radio ciblé par ``name`` ne contient aucun choix.\n\t
    ...                ``name`` correspond au name du groupe radio bouton.

    [Arguments]        ${name}

    [Tags]             Socle

    selenium2.Bouton Radio Ne Doit Pas Etre Selectionne    ${name}


Element Doit Etre Inactif
    [Documentation]    Vérifie que l'élément ciblé par ``locator`` est inactif.

    [Arguments]        ${locator}

    [Tags]             Socle

    selenium2.Element Doit Etre Inactif    ${locator}


Attendre Disparition Element
    [Documentation]    Attend la disparition de l'élément ciblé par ``locator``.

    [Arguments]        ${locator}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Attendre Disparition Element    ${locator}
    ...                                      ${timeout}
    ...                                      ${erreur}


Element Doit Etre Coche
    [Documentation]    Vérifie que la checkbox ciblée par ``locator`` est cochée.

    [Arguments]        ${locator}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Element Doit Etre Coche    ${locator}
    ...                                 ${timeout}
    ...                                 ${erreur}


Element Doit Etre Decoche
    [Documentation]    Vérifie que la checkbox ciblée par ``locator`` est décochée.

    [Arguments]        ${locator}
    ...                ${timeout}=${None}
    ...                ${erreur}=${None}

    [Tags]             Socle

    selenium2.Element Doit Etre Decoche    ${locator}
    ...                                   ${timeout}
    ...                                   ${erreur}


Accepter Pop-up
    [Documentation]    Accepte la pop-up du navigateur actif.

    [Arguments]        ${timeout}=${None}

    [Tags]             Socle

    selenium2.Accepter Pop-up    ${timeout}


Verifier Message Pop-up Et Accepter
    [Documentation]    Vérifie la présence du ``texte`` dans la pop-up et l'accepte.

    [Arguments]        ${texte}
    ...                ${timeout}=${None}

    [Tags]             Socle

    selenium2.Verifier Message Pop-up Et Accepter    ${texte}
    ...                                             ${timeout}
