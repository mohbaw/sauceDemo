# -*- coding: utf-8 -*-
*** Settings ***
Documentation     Accès pour Elastic Search via API REST.

Library           ExtendedRequestsLibrary
Library           FakerLibrary    locale=fr_FR
Library           JSONLibrary
Library           OperatingSystem
Library           Collections

Resource          log.robot
Resource          rest.robot


*** Variables ***
&{ES_PFV}        RE7=


*** Keywords ***
Rechercher Dans Elasticsearch
    [Documentation]     Effectuer une recherche dans elasticsearch.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2QueryTemplate``        le chemin vers le modèle de la requête.
    ...                 *Return* : le nombre d'occurences et un fichier JSON des réponses.
    [Arguments]         ${myPath2QueryTemplate}

    # chemin vers le template json de la question
    ${RESTWS.path2postTemplate}=    Set Variable    ${myPath2QueryTemplate}
    # renseigner le end point, l'index en fonction de l'environnement et choisir l'API _search
    ${RESTWS.base_url}=    Set Variable    ${ES_PFV.%{MY_PFV}}
    ${RESTWS.uri}=    Set Variable    /${ES_PFV.%{MY_PFV}index}/_search
    # Authentification simple (via proxy )
    ${RESTAUTH.username}=    Set Variable    ${EMPTY}
    ${jsonResponse}=    rest.Appeler Le Service
    ${totalHits}=    json.Obtenir La Valeur De La Cle Du Json    ${jsonResponse}    hits.total
    Run Keyword If    ${totalHits}==0    log.Warning    Aucun resultat trouve !
    log.Info    ${totalHits} resultats trouves

    [Return]    ${totalHits}    ${jsonResponse}
