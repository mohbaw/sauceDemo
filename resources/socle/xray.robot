*** Settings ***
Documentation       *Interactions RobotFramework <-> Xray*
...
...                 Cette librairie propose les keywords permettant d'interagir avec JIRA Xray.
...                 Elle réalise des appels REST et utilise donc la ressource rest.robot.
...
...                 *Variables d'environnements :*
...                 - ``ROBOT_DEPLOY``                est le chemin vers la racine des sources robotframework (par exemple C:\\Users\\Public\\Documents\\robot\\deploy)
...                 - ``JIRA_USERNAME``               est l'username qui permet l'accès REST publiés par Xray (exemple : _username_)
...                 - ``JIRA_PASSWORD``               est l'uri racine des services REST publiés par Xray (exemple : _password_)

Resource          ../resources/rest.robot

*** Variables ***
${API_REST_JIRA_SERVER}             http://pcld0261:8080
${API_REST_XRAY_URI}                /rest/raven/1.0/api
${API_REST_XRAY_PUBLISH_URI}        /rest/raven/1.0/import/execution/robot
${API_REST_JIRA_URI}                /rest/api/2

*** Keywords ***
récupérer les Test Execution d'un Test Plan
    [Documentation]
    ...                 Récupération des Test Execution d'un Test Plan
    ...
    ...                 *Arguments :*
    ...                       - ``testPlanKey`` : la clé du Test Plan à requêter
    ...
    ...                 *Return :*
    ...                       - ``testExecutionKeys`` : la liste des clés des Test Execution appartenant au Test Plan
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${testExecutionKeys}= | récupérer les Test Execution d'un Test Plan | tokenBasic64 | PACT-34 |
    [Arguments]     ${testPlanKey}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${testPlanExecutionUri}=    Set Variable    ${API_REST_XRAY_URI}/testplan/${testPlanKey}/testexecution
    ${response}=    rest.GET   sessionName=${sessionName}   restUri=${testPlanExecutionUri}

    ${testExecutionKeys}=    Obtenir Les valeurs De La Cle Du JsonPath       myJsonString=${response.text}       myJsonPath=$..key

    [Return]    ${testExecutionKeys}

récupérer les Test d'un Test Plan
    [Documentation]
    ...                 Récupération des Test d'un Test Plan
    ...
    ...                 *Arguments :*
    ...                       - ``testPlanKey`` : la clé du Test Plan à requêter
    ...
    ...                 *Return :*
    ...                       - ``testKeys`` : la liste des clés des Test appartenant au Test Plan
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${testKeys}= | récupérer les Test d'un Test Plan | tokenBasic64 | PUM-218 |
    [Arguments]     ${testPlanKey}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${testPlanExecutionUri}=    Set Variable    ${API_REST_XRAY_URI}/testplan/${testPlanKey}/test
    ${response}=    rest.GET   sessionName=${sessionName}   restUri=${testPlanExecutionUri}

    @{testKeys}=    Obtenir Les valeurs De La Cle Du JsonPath       myJsonString=${response.text}       myJsonPath=$..key

    [Return]    @{testKeys}

récupérer les Test d'une Test Execution
    [Documentation]
    ...                 Récupération des Test d'une Test Execution
    ...
    ...                 *Arguments :*
    ...                       - ``testExecutionKey`` : la clé de la Test Execution à requêter
    ...
    ...                 *Return :*
    ...                       - ``testKeys`` : la liste des clés des Test appartenant à la Test Execution
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${testKeys}= | récupérer les Test d'une Test Execution | tokenBasic64 | PUM-216 |
    [Arguments]     ${testExecutionKey}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${testPlanExecutionUri}=    Set Variable    ${API_REST_XRAY_URI}/testexec/${testExecutionKey}/test
    ${response}=    rest.GET   sessionName=${sessionName}   restUri=${testPlanExecutionUri}

    @{testKeys}=    Obtenir Les valeurs De La Cle Du JsonPath       myJsonString=${response.text}       myJsonPath=$..key

    [Return]    @{testKeys}

construire le dictionnaire d'execution d'un Test Plan
    [Documentation]
    ...                 Insérer le plan de tests dans un dictionnaire robotframework
    ...
    ...                 *Arguments :*
    ...                       - ``testPlanKey`` : la clé du Test Plan à requêter
    ...
    ...                 *Return :*
    ...                       - ``dictTestCases`` : le plan de tests, les clés sont les Test Excecutions, les valeurs associées sont la liste des Tests associés aux Test Executions
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| &{testExecutionKeys}= | contruire le dictionnaire d'execution d'un Test Plan | tokenBasic64 | PACT-34 |
    [Arguments]     ${testPlanKey}

    ${testExecutionKeys}=    récupérer les Test Execution d'un Test Plan        testPlanKey=${testPlanKey}

    &{dictTestCases}=   Create Dictionary
    FOR    ${testExecutionKey}    IN    @{testExecutionKeys}
       @{testKeys}=   récupérer les Test d'une Test Execution      testExecutionKey=${testExecutionKey}
       Set To Dictionary   ${dictTestCases}    ${testExecutionKey}=@{testKeys}
    END

    Log Dictionary  ${dictTestCases}    level=INFO
    [Return]    &{dictTestCases}

obtenir les informations sur une fiche JIRA
    [Documentation]
    ...                 Génération du token Basic pour connexion à JIRA Xray
    ...
    ...                 *Arguments :*
    ...                       - ``issueKey`` : la clé de la fiche JIRA
    ...
    ...                 *Return :*
    ...                       - ``token`` : le token d'authentification
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${basicToken}= | demander le token de connexion au serveur jira xray | username | password |

    [Arguments]     ${issueKey}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${issueUri}=    Set Variable    ${API_REST_JIRA_URI}/issue/${issueKey}
    ${response}=    rest.GET   sessionName=${sessionName}     restUri=${issueUri}

    &{issueInfosDict}=      Create Dictionary
    @{issueType}=    Obtenir Les valeurs De La Cle Du JsonPath       myJsonString=${response.text}       myJsonPath=$..issuetype.name
    ${robotKeywordFile}=    Run Keyword If      '${issueType}[0]'=='Test Execution'     Obtenir La valeur De La Cle Du Json       myJsonString=${response.text}       myKey=customfield_10020
    ${summary}=    Obtenir La valeur De La Cle Du Json       myJsonString=${response.text}       myKey=summary
    Set To Dictionary   ${issueInfosDict}  issueType=${issueType}   robotKeywordFile=${robotKeywordFile}     summary=${summary}

    [Return]  &{issueInfosDict}

publier les resultats robotframework
    [Documentation]
    ...                 Génération du token Basic pour connexion à JIRA Xray
    ...
    ...                 *Arguments :*
    ...                       - ``issueKey`` : la clé de la fiche JIRA
    ...
    ...                 *Return :*
    ...                       - ``token`` : le token d'authentification
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${basicToken}= | demander le token de connexion au serveur jira xray | username | password |

    [Arguments]     ${outpuFilePath}  ${projectKey}=${None}  ${testPlanKey}=${None}  ${testExecutionKey}=${None}    ${testEnvironnement}=${None}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${publishUri}=    Catenate      SEPARATOR=${EMPTY}    ${API_REST_XRAY_PUBLISH_URI}      ?
    ${publishUri}=    Run Keyword If  '${projectKey}'!='${None}'  Catenate      SEPARATOR=${EMPTY}           ${publishUri}     projectKey=${projectKey}  ELSE  Set Variable  ${publishUri}
    ${publishUri}=    Run Keyword If  '${testPlanKey}'!='${None}'  Catenate      SEPARATOR=&           ${publishUri}     testPlanKey=${testPlanKey}  ELSE  Set Variable  ${publishUri}
    ${publishUri}=    Run Keyword If  '${testExecutionKey}'!='${None}'  Catenate      SEPARATOR=&           ${publishUri}     testExecKey=${testExecutionKey}  ELSE  Set Variable  ${publishUri}
    ${publishUri}=    Run Keyword If  '${testEnvironnement}'!='${None}'  Catenate      SEPARATOR=&           ${publishUri}     testEnvironments=${testEnvironnement}  ELSE  Set Variable  ${publishUri}

    ${file_data}=  Get File For Streaming Upload  ${outpuFilePath}
    ${files}=  Create Dictionary  file=${file_data}

    ${response}=    rest.POST   sessionName=${sessionName}   restUri=${publishUri}    files=${files}

    Should Be Equal As Integers   200    ${response.status_code}   La publication des résultats ne s'est pas déroulé correctement

    [Return]     ${response}

créer une Test Execution et y publier les resultats robotframework
    [Documentation]
    ...                 Génération du token Basic pour connexion à JIRA Xray
    ...
    ...                 *Arguments :*
    ...                       - ``issueKey`` : la clé de la fiche JIRA
    ...
    ...                 *Return :*
    ...                       - ``token`` : le token d'authentification
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${basicToken}= | demander le token de connexion au serveur jira xray | username | password |

    [Arguments]     ${outpuFilePath}   ${testPlanKey}=${None}   ${summary}=${None}   ${testEnvironnement}=${None}

    ${sessionName}=   rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${publishUri}=    Catenate      SEPARATOR=${EMPTY}    ${API_REST_XRAY_PUBLISH_URI}/multipart

    ${date}=  Get Current Date
    ${DATETIME} =	Convert Date	${date}  result_format=%Y-%m-%d_%H:%M:%S

    ${projectKey}=  Set Variable    ${Empty}
    IF  '${testPlanKey}'!='${None}'
        ${projectKey}=  Fetch From Left  ${testPlanKey}  -
    ELSE
        IF  '${projectKey}'=='${Empty}'
            ${projectKey}=  Set Variable  PP
        END
    END

    ${summary}=         Set Variable If  '${summary}'=='${None}'        TE_${testPlanKey}_${DATETIME}      ${summary}
    ${testPlanKey}=     Set Variable If  '${testPlanKey}'=='${None}'    ${Empty}                           ${testPlanKey}

    &{testExecInfos}=  Create Dictionary
    ...     $.fields.project.key=${projectKey}
    ...     $.fields.summary=${summary}
    ...     $.fields.customfield_10028[0]=${testPlanKey}
    ...     $.fields.customfield_10026[0]=${testEnvironnement}
    ...     $.fields.labels[0]=rfXrayPublisher

    IF  '${testPlanKey}'!='${None}'
        Set To Dictionary  ${testExecInfos}  $.fields.customfield_10028[0]=${testPlanKey}
    ELSE
        json.
    END

    ${json}=    json.Modifier un fichier Json   %{ROBOT_DEPLOY}/tnr_socle_automatisation/resources/utils/TestExecInfos.template.json   ${testExecInfos}
    ${json}=    Convert Json To String  ${json}
    Create File   ./testExecInfo.json  ${json}  encoding=UTF-8

    ${outpuFile_data}=  Get File For Streaming Upload  ${outpuFilePath}
    ${testExecInfo_data}=  Get File For Streaming Upload  testExecInfo.json
    ${files}=  Create Dictionary  file=${outpuFile_data}  info=${testExecInfo_data}
    #${files}=      Evaluate   [('file', open('${outpuFilePath}', 'rb')), ('info', open('testExecInfo.json', 'rb'))]
    #&{files}=  Create Dictionary  file=${outpuFilePath}  info=.${/}testExecInfo.json

    ${response}=    rest.POST   sessionName=${sessionName}     restUri=${publishUri}       files=${files}

    Should Be Equal As Integers   200    ${response.status_code}   La publication des résultats ne s'est pas déroulé correctment

    [Return]     ${response}
