*** Settings ***
Library           ../utils/BrowserLibrary.py
Library           pywinauto.keyboard

Library     OperatingSystem
Library     BuiltIn
Library     Collections
Resource    %{ROBOT_DEPLOY}/tnr_socle_automatisation/resources/web.robot

*** Keywords ***
Importer un fichier dans le navigateur
    [Documentation]    Importe un fichier dans le navigateur indique par MY_BROWSER
    [Arguments]    ${myPath2file}    ${myNomFenetre}

    Run Keyword And Return If  "%{MY_BROWSER}" == "Chrome"   Importer un fichier dans chrome    ${myPath2file}    ${myNomFenetre}
    Run Keyword And Return If  "%{MY_BROWSER}" == "Firefox"  Importer un fichier dans firefox  ${path2file}
    Run Keyword  FAIL  msg=Le navigateur n'est pas supporté pour l'import de fichier

Annuler le telechargement d'un fichier avec Firefox
    [Documentation]  Annule le telechargement d'un fichier avec Firefox
    Return From Keyword If  "%{MY_BROWSER}" != "Firefox"
    Annuler un telechargement avec firefox

Telecharger le fichier avec Firefox
    [Documentation]  Telecharge un fichier avec Firefox
    Return From Keyword If  "%{MY_BROWSER}" != "Firefox"
    Telecharger un fichier avec firefox

Sauvegarder un fichier depuis un navigateur
    [Documentation]     Récupérer un fichier. (pdf, html, jpeg, etc..)
    ...
    ...                 ('^s') : appuie touche CTRL + S pour enregistrer un fichier
    ...                 (${nomFichier}) : nom du fichier à récupérer
    ...                 ('{ENTER}') : appuie sur la touche Entrée
    [Arguments]         ${nomFichier}

    pywinauto.keyboard.Send Keys  (^s)     pause=2
    pywinauto.keyboard.Send Keys  (${nomFichier}, with_spaces=True)
    pywinauto.keyboard.Send Keys  ({ENTER})    pause=2



Sauvegarder un fichier depuis un navigateur avec copier coller
    [Documentation]     Récupérer un fichier. (pdf, html, jpeg, etc..)
    ...
    ...                 ('^s') : appuie touche CTRL + S pour enregistrer un fichier
    ...                 (${nomFichier}) : nom du fichier à récupérer
    ...                 ('{ENTER}') : appuie sur la touche Entrée
    [Arguments]         ${nomFichier}

    pyperclip.copy  ${nomFichier}
    Sleep  1s
    pywinauto.keyboard.Send Keys  (^s)     pause=2
    pywinauto.keyboard.Send Keys  (^v)     pause=1
    pywinauto.keyboard.Send Keys  ({ENTER})    pause=1


Sauvegarder un fichier depuis un navigateur avec copier coller sans declancher l'enregistrement
    [Documentation]     Récupérer un fichier. (pdf, html, jpeg, etc..)
    ...
    ...                 ('^s') : appuie touche CTRL + S pour enregistrer un fichier
    ...                 (${nomFichier}) : nom du fichier à récupérer
    ...                 ('{ENTER}') : appuie sur la touche Entrée
    [Arguments]         ${nomFichier}

    pyperclip.copy  ${nomFichier}
    Sleep  1s
    pywinauto.keyboard.Send Keys  (^v)     pause=1
    pywinauto.keyboard.Send Keys  ({ENTER})    pause=1


Sauvegarder un fichier depuis un navigateur avec copier coller en s'accrochant a la fenetre
    [Documentation]     Récupérer un fichier. (pdf, html, jpeg, etc..)
    ...
    ...                 ('^s') : appuie touche CTRL + S pour enregistrer un fichier
    ...                 (${nomFichier}) : nom du fichier à récupérer
    ...                 ('{ENTER}') : appuie sur la touche Entrée
    [Arguments]         ${nomFichier}    ${nomFenetre}

    # pyperclip.copy  ${nomFichier}
    # Sleep  1s
    # pywinauto.keyboard.Send Keys  (^s)     pause=2
    # pywinauto.keyboard.Send Keys  (^v)     pause=1
    # pywinauto.keyboard.Send Keys  ({ENTER})    pause=1

    ${statut}    Run Keyword If  "%{MY_BROWSER}" == "Chrome"   Telecharger un fichier dans Chrome    ${nomFichier}    ${nomFenetre}
    # Run Keyword And Return If  "%{MY_BROWSER}" == "Firefox"  Telecharger un fichier dans Firefox    ${nomFichier}    ${nomFenetre}
    # Run Keyword  FAIL  msg=Le navigateur n'est pas supporte pour le telechargement de fichier
    #${myBrowser}    Set Variable    %{MY_BROWSER}
    #${statut}    BrowserLibrary.Telecharger un fichier dans ${myBrowser}    ${nomFichier}    ${nomFenetre}

    [Return]    ${statut}


Fermer la fenetre du navigateur
    [Arguments]    ${myNavigateur}    ${nomFenetre}

    BrowserLibrary.Fermer la fenetre du navigateur    ${myNavigateur}    ${nomFenetre}


# TODO : A RETIRER mots clés déjà présents dans web.robot/selenium.robot ------------------
Ouverture Chrome
    [Documentation]  Ouverture du navigateur internet Google Chrome
	${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
	${disabled} =  Create List    Chrome PDF Viewer
    ${prefs} =     Create Dictionary    download.default_directory=${LOG FILE}    plugins.plugins_disabled=${disabled}
    Call Method    ${options}    add_experimental_option    prefs    ${prefs}
    Call Method    ${options}    add_argument    --start-maximized
    Call Method    ${options}    add_argument    --always-authorize-plugins
    Call Method    ${options}    add_experimental_option  useAutomationExtension  ${False}
    Create WebDriver    Chrome   chrome_options=${options}
    web.Determiner le repertoire des captures d'ecran    ${OUTPUT DIR}/%{PRODUIT}_%{AG}
    #Go To   %{URL}

Ouverture Firefox
    [Documentation]     Ouverture du navigateur internet Mozilla Firefox
    ${firefox_path}=    Evaluate  sys.modules['selenium.webdriver'].firefox.firefox_binary.FirefoxBinary(firefox_path='C:/Program Files/Mozilla Firefox/firefox.exe', log_file=None)  sys
    ${caps}=            Evaluate  sys.modules['selenium.webdriver'].common.desired_capabilities.DesiredCapabilities.FIREFOX    sys
    Set To Dictionary   ${caps}   marionette=${True}
    Create WebDriver    Firefox   firefox_binary=${firefox_path}    capabilities=${caps}
    web.Determiner le repertoire des captures d'ecran    ${OUTPUT DIR}/%{PRODUIT}_%{AG}
    #Go To   %{URL}

Ouverture IE
    [Documentation]  Ouverture du navigateur internet Microsoft Internet Explorer
    ...              La préférence linguistique du poste sur lequel les tests sont éxecutés doit impérativement être réglée en Anglais (Clavier Qwerty)
    ${caps}=         Evaluate  sys.modules['selenium.webdriver'].DesiredCapabilities.INTERNETEXPLORER    sys
    Set To Dictionary   ${caps}   ignoreProtectedModeSettings  ${True}
    Open Browser        %{URL}    ie    desired_capabilitie=${caps}
    web.Determiner le repertoire des captures d'ecran    ${OUTPUT DIR}/%{PRODUIT}_%{AG}

# TODO : A RETIRER ------------------
