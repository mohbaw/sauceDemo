# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource wrapper de AppiumLibrary pour Windows.
...
...                 Cette librairie permet de démarrer une application sur un
...                 appareil Windows.
...
...                 = /!\\ Ne pas utiliser /!\\ =
...                 *[http://localhost:7070/doc/keywords/14/|Appium] effectue*
...                 *l'aiguillage vers cette librairie selon l'appareil utilisé.*

Library             OperatingSystem

Resource            appium.robot
Resource            log.robot


*** Keywords ***
Ouvrir L'Application
    [Documentation]     Ouvrir une application sur un appareil Windows.
    ...
    ...                 *Arguments :*
    ...                 - ``myApp``         est le nom de l'application à ouvrir.
    ...                 - ``myDevice``      est le type de l'appareil utilisé.
    ...                 _Appium Windows._
    [Arguments]         ${myApp}    ${myDevice}

    ${uri2appium}=      appium.Obtenir L'Uri Vers Appium Server
    Open Application    ${uri2appium}
    ...                 platformName=windows
    ...                 platformVersion=${myDevice.platformVersion}
    ...                 deviceName=${myDevice.deviceName}
    ...                 app=${myApp.windowsapp}
