﻿# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource wrapper de DatabaseLibrary.
...
...                 Cette librairie reprend les fonctionnalites presentes dans la librairie
...                 d'origine et propose des mots-cles en francais.
...
...                 Avec cette librairie, il est possible de se connecter à une base de
...                 donnees et d'effectuer des requêtes dessus. De plus, ``sql.robot``
...                 peut mettre les donnees recuperees au format CSV et les extraire
...                 sous cette forme.
...
...                 === Plus d'informations ===
...                 [https://franz-see.github.io/Robotframework-Database-Library/api/0.5/DatabaseLibrary.html|Cliquez ici pour plus d'information sur DatabaseLibrary.]

# Jenkins fourni le fichier de configuration des chaines d'acces Oracle par 'provide configuration file' du job setting
Resource            %{MY_ORADB_CONNECTIONS}
Library             DatabaseLibrary
Library             Collections
Library             OperatingSystem
Resource            Log.robot
Library             String

*** Variables ***
${DEFAULT_NLSLANG}      FRENCH_FRANCE.WE8ISO8859P15
${ORACLE_HOME}          C:\\Oracle\\product\\11.2.0

*** Keywords ***
# ==============================================================================
# Gestion de connection ========================================================
# ==============================================================================
Se Connecter A La Base De Donnees
    [Documentation]     Lancer la connexion à la base de donnees.
    ...
    ...                 *Arguments :*
    ...                 - ``myDatabase``     est la base de donnees à laquelle se connecter.
    [Arguments]         ${myDatabase}

    Connect To Database Using Custom Params    cx_Oracle    ${myDatabase}


Se Deconnecter De La Base De Donnees Courante
    [Documentation]     Se deconnecter de la base de donnees courante.

    Disconnect From Database


Ouvrir La Connexion
    [Documentation]     Preparer la connexion vers la base de donnees.
    ...
    ...                 *Arguments :*
    ...                 - ``myDbChainAccess``       est l'acces à la connexion.
    [Arguments]         ${database}

    # Se connecter avec la chaine d'acces
    #${database}=    Get From Dictionary    ${MDTDBCNX}    ${myDbChaineDAcces}
    ${existNLSSpecial}    ${NLSSpecial}=                Run Keyword And Ignore Error    Get From Dictionary    ${MDTDBNLS}    ${myDbChaineDAcces}
    Run Keyword If    '${existNLSSpecial}'=='PASS'      Set Environment Variable    NLS_LANG    ${NLSSpecial}
    Run Keyword Unless    '${existNLSSpecial}'=='PASS'  Set Environment Variable    NLS_LANG    ${DEFAULT_NLSLANG}
    sql.Se Connecter A La Base De Donnees    ${database}


Ouvrir Une Connexion Avec Le NLSLANG
    [Documentation]     Ouvrir une connexion vers la base de donnees avec le NLSLANG.
    ...
    ...                 *Arguments :*
    ...                 - ``myDbChainAccess``       est l'acces a la connexion.
    ...                 - ``myNlslang``             est le parametre NLSLANG.
    [Arguments]         ${myDbChainAccess}    ${myNlslang}

    # Se connecter avec la chaine d'acces
    ${database}=    Get From Dictionary     ${MDTDBCNX}    ${myDbChainAccess}
    Set Environment Variable    NLS_LANG    ${myNlslang}
    sql.Se Connecter A La Base De Donnees   ${database}


# ==============================================================================
# Effectuer des requêtes =======================================================
# ==============================================================================
Obtenir Le Premier Resultat De Ma Requete Sur La Connexion
    [Documentation]     Lancer une requête SQL et retourner son resultat.
    ...
    ...                 La requête doit renvoyer un unique resultat.
    ...
    ...                 *Arguments :*
    ...                 - ``mySqlQuery``      est la requête à executer.
    ...                 - ``myDbCnx``         est la connexion à la base de donnees.
    ...                 *Return* : le resultat de la requête.
    [Arguments]         ${mySqlQuery}    ${myDbCnx}

    # construire le suffixe des serveurs en fonction de la plateforme cible
    sql.Ouvrir La Connexion    ${myDbCnx}
    # prendre la description du job dans la bdd controlm
    ${queryResult}=    Query    ${mySqlQuery}
    # obtenir le resultat
    log.Debug    Resultat
    # controler avec l'attendusi il est present
    ${length}=    Set Variable    ${EMPTY}
    ${length}=    Get Length    ${queryResult}
    ${myFirstResult}=    Set Variable    ${EMPTY}
    ${myFirstResult}=    Get Variable Value    ${queryResult[0][0]}    null
    log.Debug    Mon Resultat ${myFirstResult}
    sql.Se Deconnecter De La Base De Donnees Courante
    log.Debug    Resultat ${myFirstResult}

    [Return]    ${myFirstResult}


Obtenir Le Resultat De La Requete Dans Un Fichier CSV
    [Documentation]     Executer une requête SQL sur une BDD et exporter le resultat dans un fichier au format CSV.
    ...
    ...                 *Arguments :*
    ...                 - ``mySqlQuery``    est la requête à executer.
    ...                 - ``myDbCnx``       est la connexion à la base de donnees.
    ...                 - ``mySeparator``   est le caractere de separation des donnees recuperees.
    ...                 *Return* : le fichier CSV contenant les resultats de la requête.
    [Arguments]         ${mySqlQuery}    ${myDbCnx}    ${mySeparator}=,

    # lancer la requête sur la bdd
    ${queryResult}    ${nbRows}=    sql.Lancer La Requete    ${mySqlQuery}    ${myDbCnx}
    # Construire l'entete de colonnes
    ${header}    sql.Obtenir Entete    ${mySqlQuery}
    # construire le fichier resultat
    ${myCsvFile}=    Set Variable    %{WORKSPACE}/sqlResultat_%{BUILD_NUMBER}.csv
    sql.Exporter Le Resultat Dans Le Fichier CSV    ${myCsvFile}    ${queryResult}    ${header}

    [Return]    ${myCsvFile}


Lancer La Requete
    [Documentation]     Executer une requête sur une base de donnees.
    ...
    ...                 *Arguments :*
    ...                 - ``mySqlQuery``    est la requête à executer.
    ...                 - ``myDbCnx``       est la connexion à la base de donnees.
    ...                 *Return* : le resultat de la requête et la quantite de lignes obtenues.
    [Arguments]         ${mySqlQuery}    ${myDbCnx}

    sql.Ouvrir La Connexion    ${myDbCnx}
    log.Info    Je lance la requete sur la connexion ${myDbCnx}
    log.Debug    ${mySqlQuery}
    ${mySqlResult}=    Query    ${mySqlQuery}
    # obtenir le resultat
    # Nb de lignes obtenues
    ${myNbRows}=    Get Length    ${mySqlResult}
    Run Keyword If    ${myNbRows} > 0    log.Success    Le resultat contient ${myNbRows} lignes
    ...       ELSE                       log.Warning    Le resultat ne contient aucune ligne
    log.Debug    ${mySqlResult}
    sql.Se Deconnecter De La Base De Donnees Courante

    [Return]    ${mySqlResult}    ${myNbRows}

Exécuter une requête SQL
    [Documentation]     Executer une requête sur une base de donnees.
    ...
    ...                 *Arguments :*
    ...                 - ``mySqlQuery``    est la requête à executer.
    ...                 - ``myDbCnx``       est la connexion à la base de donnees.
    ...                 *Return* : le resultat de la requête et la quantite de lignes obtenues.
    [Arguments]         ${requete}  ${base}  ${user}  ${mdp}  ${resultTrace}=${True}  ${serveur}=${EMPTY}

    log to console  ${EMPTY}

    # Détermination du serveur Oracle
    IF  '${serveur}' == '${EMPTY}'
        ${serveur}=     Set Variable If  '%{ENVIRONMENT}'!='PRODUCTION'  //${SERVEURS_ORACLE_CNP}[%{ENVIRONMENT}]/  ${EMPTY}
    ELSE
        ${serveur}=     Set Variable    //${serveur}/
    END

    sql.Ouvrir La Connexion    "${user}/${mdp}@${serveur}${base}"
    log.Info    Exécution de la requete sur la base ${base}
    log.Debug    ${requete}
    ${resultat}=    Query    ${requete}
    # Nb de lignes obtenues
    ${nbreLignes}=    Get Length    ${resultat}
    Run Keyword If  ${resultTrace}==${True}     log.Debug    ${resultat}
    sql.Se Deconnecter De La Base De Donnees Courante

    [Return]    ${resultat}    ${nbreLignes}


Lancer La Requete sans trace dans la console
    [Documentation]     Executer une requête sur une base de donnees.
    ...
    ...                 *Arguments :*
    ...                 - ``mySqlQuery``    est la requête à executer.
    ...                 - ``myDbCnx``       est la connexion à la base de donnees.
    ...                 *Return* : le resultat de la requête et la quantite de lignes obtenues.
    [Arguments]         ${mySqlQuery}    ${myDbCnx}

    sql.Ouvrir La Connexion    ${myDbCnx}
    log.Info    Je lance la requete sur la connexion ${myDbCnx}
    ${mySqlResult}=    Query    ${mySqlQuery}
    # obtenir le resultat
    # Nb de lignes obtenues
    ${myNbRows}=    Get Length    ${mySqlResult}
    Run Keyword If    ${myNbRows} > 0    log.Success    Le resultat contient ${myNbRows} lignes
    ...       ELSE                       log.Warning    Le resultat ne contient aucune ligne
    sql.Se Deconnecter De La Base De Donnees Courante

    [Return]    ${mySqlResult}    ${myNbRows}


Obtenir Entete
    [Documentation]     Extraire le nom des colonnes du SELECT de la requête et renvoyer au format CSV.
    ...
    ...                 *Arguments :*
    ...                 - ``mySqlQuery``    est la requête à executer.
    ...                 - ``mySeparator``   est le caractere de separation des donnees.
    ...                 *Return* : la liste des entêtes selectionnes dans un fichier CSV.
    [Arguments]         ${mySqlQuery}    ${mySeparator}=,

    # convertir en majuscule
    ${sqlRequestUpperCase}=    Convert To Uppercase    ${mySqlQuery}
    # extraite le nom des colonnes du SELECT    # expression reguliere pour prendre entre SELECT et FROM au plus court - .*? non greeding, http://stackoverflow.com/questions/2074452/regex-to-first-occurence-only
    ${lstCols}=    Get Regexp Matches    ${sqlRequestUpperCase}    SELECT (.*?) FROM    1
    # on prend la premiere occurence trouvee
    ${header}=    Get From List    ${lstCols}    0
    # on supprime l'eventuel hint ou commentaire compris entre /* et */    # double echappement du caractere *
    ${header}=    Remove String Using Regexp    ${header}    /\\*.*\\*/
    # on remplace la virgule par le separateur
    ${myCsvHeader}=    Replace String    ${header.strip()}    ,    ${mySeparator}

    [Return]    ${myCsvHeader}


# ==============================================================================
# Sortir les resultats =========================================================
# ==============================================================================
Extraire Les Donnees Du Sql Dans Le Dictionnaire
    [Documentation]     Extraire les données d'un fichier SQL dans un dictionnaire Python.
    ...
    ...                 *Arguments :*
    ...                 - ``jsonResponse``                              est le fichier SQL.
    ...                 - ``extractSqlForGivenKeyDictionary``           est la carte des clés avec le chemin SQL à extraire.
    ...                 - ``targetedDictionnary``                       est le dictionnaire visé.
    ...                 - ``isWithStrip``                               si le résultat doit être découpé (prendre le premier mot).
    [Arguments]         ${request}   ${extractSqlForGivenKeyDictionary}   ${targetedDictionnary}

    FOR    ${key}    IN    @{extractSqlForGivenKeyDictionary.keys()}
        ${sqlPath}=            Set Variable    ${extractSqlForGivenKeyDictionary.${key}}
        ${sqlValue}=           Run Keyword  Set Variable  ${${sqlPath}}
        Log    ${key} // ${sqlPath} // ${sqlValue}
        Set To Dictionary   ${targetedDictionnary}    ${key}=${sqlValue}
    END


Mettre Au Format CSV
    [Documentation]     Construire une ligne au format CSV à partir de la liste des champs.
    ...
    ...                 *Arguments :*
    ...                 - ``myListFields``      est une liste de champs a remplir.
    ...                 - ``mySeparator``       est le caractere de separation des donnees.
    ...                 *Return* : la ligne creee au format CSV.
    [Arguments]         ${myListFields}    ${mySeparator}=,

    ${agregation}=    Set Variable    ${EMPTY}
    FOR    ${item}    IN    @{myListFields}
        # Construire la ligne CSV avec le separateur    # on distingue le premier champ des suivants
        ${estAgrege}=    Get Length    ${agregation}
        ${agregation}=    Set Variable If    ${estAgrege}==0    ${item}    ${agregation}${mySeparator}${item}
    END
    # ajouter le saut de ligne
    ${retCSVRow}=    Catenate    ${agregation}    \n

    [Return]    ${retCSVRow}


Exporter Le Resultat Dans Le Fichier CSV
    [Documentation]     Exporter les lignes resultats dans un fichier avec l'entête de colonnes au format CSV.
    ...
    ...                 *Arguments :*
    ...                 - ``myFile``            est le fichier dans lequel le resultat sera stocke.
    ...                 - ``myQueryResult``     est le resultat de la requête.
    ...                 - ``myHeader``          est l'entête de la requête.
    ...                 - ``mySeparator``       est le caractere de separation des donnees.
    [Arguments]         ${myFile}    ${myQueryResult}    ${myHeader}    ${mySeparator}=,

    # Nb de colonnes
    ${lstcolumns}=    Split String    ${myHeader}    separator=${mySeparator}
    ${nbCols}=    Get Length    ${lstcolumns}
    # ajouter la ligne entete
    Append To File    ${myfile}    ${myHeader}\n
    # Parcourir chaque ligne
    ${nbRows}=    Get Length    ${myQueryResult}
    FOR    ${currentRow}    IN RANGE    0    ${nbRows}
        # mettre la ligne au format CSV    # La premiere conversion donne un resultat de type ('AAA', 'BBB', 'CCC')
        ${rowCSV}=    sql.Mettre Au Format CSV    ${myQueryResult[${currentRow}]}
        Append To File    ${myFile}    ${rowCSV}
    END


