*** Settings ***
Documentation
...                 Cette ressource met à disposition les mots-clé pour réaliser facilement des appels REST
...
...                 Les modes d'authentification suivants sont disponibles : Basic Authentification et Oauth2 authentification
...
...                 Les verbes suivants sont disponibles : GET, POST, PUT, PATCH, DELETE
...
...
...                 *Exemple avec OAuth2 Authentification : *
...					| ${sessionName}= |   Set Variable  | tnr-api  |
...					| creer session oauth2 | ${sessionName} | https://devcol-api-vfg.cnp.fr/contract-vfg-3 | tnr-api-5a8**** | dc1****** |
...                 | ${response}= | GET | ${sessionName} | /info |
...
...
...                 *Exemple avec Basic Authentification : *
...					| ${sessionName}= |   Set Variable  | maphilip  |
...					| creer session basic | restServer=http://pcld0261:8080 | user=maphilip | password=******** |
...                 | ${response}= | POST | ${sessionName} | restUri=/rest/raven/1.0/import/execution/robot?projectKey=PP&testPlanKey=PP-38&testExecKey=PP-39 | file=./output_xray.xml |
...

Library           RequestsLibrary
Library           FakerLibrary    locale=fr_FR
Resource          Log.robot
Resource          json.robot
Resource          string.robot

*** Keywords ***
obtenir session basic
    [Documentation]
    ...                 Récupère la session si elle existe, sinon crée une session d'authentification REST en mode Basic
    ...                 Le nom de la session prend automatiquement la valeur du "user"
    ...
    ...                 Le token est généré grâce à l'encodage de l'expresion "{user}:{password}" en base 64
    ...                 où :
    ...
    ...                 *Arguments :*
    ...                     - ``restServer`` : url du serveur REST
    ...                     - ``user`` : login d'accès à ce serveur
    ...                     - ``password`` : mot de passe associé au login "user"
    ...                     - ``sessionName`` : (facultatif) nom de la session, prend le nom du user par défaut

    [Arguments]     ${restServer}      ${user}     ${password}    ${sessionName}=${user}

    ${existingSession}=	    Session Exists	${sessionName}
    Return From Keyword If   ${existingSession}

    ${auth}=  Create List  ${user}     ${password}
    Create Session    ${sessionName}    ${restServer}     auth=${auth}  debug=1

obtenir session oauth2
    [Documentation]
    ...                 Récupère la session si elle existe, sinon crée une session d'authentification REST en mode OAuth2
    ...                 où :
    ...                       - sessionName : nom de la session (donner le nom du consumer, exemple : tnr-api)
    ...                       - apiBaseUrl : url de base de l'api
    ...                       - clientId : login d'accès à l'instance de jira
    ...                       - clientSecret : mot de passe associé au login "user"

    [Arguments]     ${sessionName}   ${apiBaseUrl}   ${clientId}   ${clientSecret}

    ${existingSession}=	    Session Exists	${sessionName}
    ${currentDate}=    Get Current Date
    ${expDate}=  Set Variable If  ${existingSession}   ${${sessionName}_expiration}  ${currentDate}
    ${diffDate}=    Subtract Date From Date   ${expDate}  ${currentDate}  exclude_millis=${True}
    ${validToken}=  Evaluate  ${diffDate} > ${0}
    IF  ${existingSession} & ${validToken}
        Return From Keyword
    END

    &{headers}=     Create Dictionary   Content-Type=application/json
    &{params}=  Create Dictionary   client_id=${clientId}   client_secret=${clientSecret}   grant_type=client_credentials

    Create Session       alias=${sessionName}
                 ...     url=${apiBaseUrl}
                 ...     headers=${headers}
                 ...     verify=${True}

    ${resp}=  POST On Session  ${sessionName}  /oauth2/token    params=${params}  headers=${headers}
    ${token_type}=      json.Obtenir la valeur d'un élément via son JsonPath  ${resp.text}  $.token_type
    ${access_token}=    json.Obtenir la valeur d'un élément via son JsonPath  ${resp.text}  $.access_token
    ${expires_in}=    json.Obtenir la valeur d'un élément via son JsonPath  ${resp.text}  $.expires_in

    ${auth}=    Catenate  SEPARATOR=${SPACE}    ${token_type}  ${access_token}

    &{headers}=     Create Dictionary   Authorization=${auth}
    Update Session  ${sessionName}  ${headers}

    ${expirationTime}=  Convert Time  ${expires_in}
    ${expirationDate}=  Add Time To Date  ${currentDate}  ${expirationTime}
    Log   Date acutelle : ${currentDate} et Date d'expiration : ${expirationDate}
    Set Suite Variable  ${${sessionName}_expiration}  ${expirationDate}

obtenir session IDG
    [Documentation]
    ...                 Récupère la session si elle existe, sinon crée une session d'authentification REST via IDG (en oauth2)
    ...                 où :
    ...                       - sessionName : nom de la session (donner le nom du consumer, exemple : tnr-api)
    ...                       - urlIdg : url du serveur d'authentification
    ...                       - apiBaseUrl : url de base de l'api
    ...                       - clientId : login d'accès à l'instance de jira
    ...                       - clientSecret : mot de passe associé au login "user"
    ...                       - scope : les privilèges d'accès

    [Arguments]     ${sessionName}    ${urlIdg}    ${apiBaseUrl}    ${clientId}    ${clientSecret}    ${scope}    ${sslVerif}=${True}

    ${existingSession}=	    Session Exists	${sessionName}
    Return From Keyword If   ${existingSession}

    &{headers}=     Create Dictionary   Content-Type=application/x-www-form-urlencoded
    &{datas}=  Create Dictionary   client_id=${clientId}   client_secret=${clientSecret}   grant_type=client_credentials   scope=${scope}

    Create Session       alias=${sessionName}_idg
                 ...     url=${urlIdg}
                 ...     headers=${headers}
                 ...     verify=${sslVerif}

    ${resp}=  POST On Session  ${sessionName}_idg  /idg/oauth2/realms/root/realms/cnptech/access_token  headers=${headers}  data=${datas}  verify=${True}
    ${token_type}=      json.Obtenir la valeur d'un élément via son JsonPath  ${resp.text}  $.token_type
    ${access_token}=    json.Obtenir la valeur d'un élément via son JsonPath  ${resp.text}  $.access_token

    ${auth}=    Catenate  SEPARATOR=${SPACE}    ${token_type}  ${access_token}

    &{headers}=     Create Dictionary   Authorization=${auth}   url=${apiBaseUrl}   Content-Type=application/json
    
    Create Session       alias=${sessionName}
                 ...     url=${apiBaseUrl}
                 ...     headers=${headers}
                 ...     verify=${sslVerif}


GET
    [Documentation]
    ...                 Réalise un requête GET rest en sécurisé (avec token)
    ...
    ...                 *Arguments :*
    ...                     - ``sessionName`` : le nom de la session préalablement créée
    ...                     - ``restUri`` : l'uri endpoint (sans le baseUrl mentionnée dans le token
    ...                     - ``headers`` : dictionnaire contenant les valeurs à placer dans le header
    ...                     - ``params`` : dictionnaire contenant les paramètres d'appel
    ...
    ...                 *Return :*
    ...                       - ``response`` : la réponse de l'appel réalisé
    ...
    ...                 *Exemples d'utilisation :*
    ...                 | | | | | Facultatif |
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= | =Argument 3= | =Argument 4= |
	...					| ${response}= | GET | tnr-api | /info |


    [Arguments]     ${sessionName}   ${restUri}   ${headers}=${None}   ${params}=${None}  ${activateLog}=${True}    ${expectedStatus}=${None}
    [Timeout]       1min

    ${beforeDate}=    Get Current Date
    ${response}=      GET On Session    ${sessionName}   ${restUri}   headers=${headers}    expected_status=${expectedStatus}
    ${afterDate}=    Get Current Date

    ${responseTime}=  Subtract Date From Date  ${afterDate}  ${beforeDate}  result_format=verbose
    IF  ${activateLog}
        Log.Debug    Appel GET ${response.url}\n\t -> Code retour : ${response.status_code}\n\t -> Body : ${response.text}   darkblue
        Log.Debug    Temps de réponse : ${responseTime}  darkblue
    END

    [Return]    ${response}


POST
    [Documentation]
    ...                 Réalise un requête POST rest en sécurisé (avec token)
    ...
    ...                 *Arguments :*
    ...                  - ``sessionName`` : le nom de la session préalablement créée
    ...                  - ``restUri`` : l'uri endpoint (sans le baseUrl mentionnée dans le token
    ...                  - ``headers`` : dictionnaire contenant les valeurs à placer dans le header
    ...                  - ``datas`` : dictionnaire contenant les données du body
    ...                  - ``files`` : dictionnaire contenant les chemins des différents fichiers à uploader
    ...
    ...                 *Return :*
    ...                       - ``response`` : la réponse de l'appel réalisé
    ...
    ...                 *Exemples d'utilisation :*
    ...                 | | | | | Facultatif |
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= | =Argument 3= | =Argument 4= | =Argument 5= |
	...					| ${response}= | POST | maphilip | /rest/raven/1.0/import/execution/robot?projectKey=PP&testPlanKey=PP-38&testExecKey=PP-39 | file=./output_xray.xml | | |
	...                 | ${response}= | POST | tnr-api | /v2/userProfiles | data=${body} | |


    [Arguments]     ${sessionName}   ${restUri}   ${headers}=${None}    ${datas}=${None}    ${files}=${None}   ${json}=${None}  ${activateLog}=${True}  ${expectedStatus}=${None}
    [Timeout]       5min

    ${response}=      POST On Session    ${sessionName}    ${restUri}   headers=${headers}   data=${datas}   files=${files}   json=${json}  expected_status=anything

    IF  ${activateLog}
        Log.Debug    Appel POST ${response.url} \n -> Code retour : ${response.status_code}\n -> Body : ${response.text}   darkblue
    END

    [Return]    ${response}

PUT
    [Documentation]
    ...                 Réalise un requête PUT rest en sécurisé (avec token)
    ...
    ...                 *Arguments :*
    ...                  - ``sessionName`` : le nom de la session préalablement créée
    ...                  - ``restUri`` : l'uri endpoint (sans le baseUrl mentionnée dans le token
    ...                  - ``headers`` : dictionnaire contenant les valeurs à placer dans le header
    ...                  - ``datas`` : dictionnaire contenant les données du body
    ...
    ...                 *Return :*
    ...                       - ``response`` : la réponse de l'appel réalisé
    ...
    ...                 *Exemples d'utilisation :*
    ...                 | | | | | Facultatif |
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= | =Argument 3= |
	...                 | ${response}= | PUT | tnr-api | /v2/userProfiles | data=${body} |

    [Arguments]     ${sessionName}    ${restUri}    ${headers}=${None}    ${datas}=${None}  ${activateLog}=${True}  ${expectedStatus}=${None}
    [Timeout]       1min

    ${response}=      PUT On Session    ${sessionName}    ${restUri}   headers=${headers}   data=${datas}  expected_status=anything

    IF  ${activateLog}
        Log.Debug    Appel PUT ${response.url} \n -> Code retour : ${response.status_code}\n -> Body : ${response.text}   darkblue
    END

    [Return]    ${response}

PATCH
    [Documentation]
    ...                 Réalise un requête PATCH rest en sécurisé (avec token)
    ...
    ...                 *Arguments :*
    ...                  - ``sessionName`` : le nom de la session préalablement créée
    ...                  - ``restUri`` : l'uri endpoint (sans le baseUrl mentionnée dans le token
    ...                  - ``headers`` : dictionnaire contenant les valeurs à placer dans le header
    ...                  - ``datas`` : dictionnaire contenant les données du body
    ...                  - ``files`` : dictionnaire contenant les chemins des différents fichiers à uploader
    ...
    ...                 *Return :*
    ...                       - ``response`` : la réponse de l'appel réalisé
    ...
    ...                 *Exemples d'utilisation :*
    ...                 | | | | | Facultatif |
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= | =Argument 3= |
	...                 | ${response}= | PATCH | tnr-api | /v2/userProfiles | data=${body} |

    [Arguments]     ${sessionName}   ${restUri}   ${headers}=${None}    ${datas}=${None}    ${files}=${None}  ${activateLog}=${True}
    [Timeout]       1min

    ${response}=      PATCH On Session    ${sessionName}    ${restUri}   headers=${headers}   data=${datas}   files=${files}  expected_status=anything

    IF  ${activateLog}
        Log.Debug    Appel PATCH ${response.url} \n -> Code retour : ${response.status_code}\n -> Body : ${response.text}   darkblue
    END

    [Return]    ${response}

HEAD
    [Documentation]
    ...                 Réalise un requête HEAD rest en sécurisé (avec token)
    ...
    ...                 *Arguments :*
    ...                  - ``sessionName`` : le nom de la session préalablement créée
    ...                  - ``restUri`` : l'uri endpoint (sans le baseUrl mentionnée dans le token
    ...                  - ``headers`` : dictionnaire contenant les valeurs à placer dans le header
    ...
    ...                 *Return :*
    ...                       - ``response`` : la réponse de l'appel réalisé
    ...
    ...                 *Exemples d'utilisation :*
    ...                 | | | | | Facultatif |
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= | =Argument 3= |
	...                 | ${response}= | HEAD | tnr-api | /v2/userProfiles | data=${body} |

    [Arguments]     ${sessionName}   ${restUri}   ${headers}=${None}  ${activateLog}=${True}
    [Timeout]       1min

    ${response}=      HEAD On Session    ${sessionName}    ${restUri}   headers=${headers}  expected_status=anything

    IF  ${activateLog}
        Log.Debug    Appel HEAD ${response.url} \n -> Code retour : ${response.status_code}\n -> Body : ${response.text}   darkblue
    END

    [Return]    ${response}

DELETE
    [Documentation]
    ...                 Réalise un requête DELETE rest en sécurisé (avec token)
    ...
    ...                 *Arguments :*
    ...                  - ``sessionName`` : le nom de la session préalablement créée
    ...                  - ``restUri`` : l'uri endpoint (sans le baseUrl mentionnée dans le token
    ...                  - ``headers`` : dictionnaire contenant les valeurs à placer dans le header
    ...                  - ``datas`` : dictionnaire contenant les données du body
    ...
    ...                 *Return :*
    ...                       - ``response`` : la réponse de l'appel réalisé
    ...
    ...                 *Exemples d'utilisation :*
    ...                 | | | | | Facultatif |
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= | =Argument 3= |
	...                 | ${response}= | DELETE | tnr-api | /v2/userProfiles | data=${body} |

    [Arguments]     ${sessionName}   ${restUri}   ${headers}=${None}    ${datas}=${None}  ${activateLog}=${True}
    [Timeout]       1min

    ${response}=      DELETE On Session    ${sessionName}    ${restUri}   headers=${headers}   data=${datas}  expected_status=anything

    IF  ${activateLog}
        Log.Debug    Appel DELETE ${response.url} \n -> Code retour : ${response.status_code}\n -> Body : ${response.text}   darkblue
    END

    [Return]    ${response}
