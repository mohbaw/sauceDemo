*** Settings ***
Documentation       *Interactions RobotFramework <-> Jira*
...
...                 Cette librairie propose les keywords permettant d'interagir avec l'API JIRA.
...                 Elle réalise des appels REST et utilise donc la ressource rest.robot.
...
...                 *Variables d'environnements :*
...                 - ``ROBOT_DEPLOY``                est le chemin vers la racine des sources robotframework (par exemple C:\\Users\\Public\\Documents\\robot\\deploy)
...                 - ``JIRA_USERNAME``               est l'username qui permet l'accès REST (exemple : _username_)
...                 - ``JIRA_PASSWORD``               est l'uri racine des services REST (exemple : _password_)

Resource          ../resources/rest.robot

*** Variables ***
${API_REST_JIRA_SERVER}             http://pcld0261:8080
${API_REST_JIRA_URI}                /rest/api/2

*** Keywords ***
consulter une demande
    [Documentation]
    ...                 Récupération des Test Execution d'un Test Plan
    ...                 GET /rest/api/2/issue/{issueIdOrKey}
    ...
    ...                 *Arguments :*
    ...                       - ``testPlanKey`` : la clé du Test Plan à requêter
    ...
    ...                 *Return :*
    ...                       - ``testExecutionKeys`` : la liste des clés des Test Execution appartenant au Test Plan
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${testExecutionKeys}= | récupérer les Test Execution d'un Test Plan | tokenBasic64 | PACT-34 |
    [Arguments]     ${issueIdOrKey}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${uri}=    Set Variable    ${API_REST_JIRA_URI}/issue/${issueIdOrKey}

    ${response}=    rest.GET   sessionName=${sessionName}   restUri=${uri}

    [Return]    ${response}

consulter une pièce jointe
    [Documentation]
    ...                 Consulter une pièce jointe
    ...                 GET /rest/api/2/attachment/{id}
    ...
    ...                 *Arguments :*
    ...                       - ``attachmentId`` : l'id de la pièce jointe
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${testExecutionKeys}= | consulter une pièce jointe | ${attachmentId} |
    [Arguments]     ${attachmentId}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}
    ${uri}=    Set Variable    ${API_REST_JIRA_URI}/attachment/${attachmentId}

    ${response}=    rest.GET   sessionName=${sessionName}   restUri=${uri}

ajouter des pièces jointes à une fiche
    [Documentation]
    ...                 Ajouter des pièces jointes à une fiche JIRA
    ...                 POST /rest/api/2/issue/${issueIdOrKey}/attachments
    ...
    ...                 *Arguments :*
    ...                       - ``issueIdOrKey`` : la clé du Test Plan à requêter
    ...                       - ``piecesJointes`` : la liste des pièces jointes
    ...
    ...                 *Return :*
    ...                       - ``attachmentIds`` : la liste des ID des pièces jointes postées
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${testExecutionKeys}= | ajouter des pièces jointes à une fiche | PP-76 | ${piecesJointes} |
    [Arguments]     ${issueIdOrKey}     ${piecesJointes}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}

    ${uri}=    Set Variable    ${API_REST_JIRA_URI}/issue/${issueIdOrKey}/attachments
    &{headers}=     Create Dictionary  X-Atlassian-Token=no-check

    @{attachmentIds}=   Create List
    FOR  ${piecesJointe}  IN  @{piecesJointes}
        ${file_data}=  Get File For Streaming Upload  ${piecesJointe}
        ${files}=  Create Dictionary  file=${file_data}
        ${response}=    rest.POST   sessionName=${sessionName}   restUri=${uri}  headers=${headers}   files=${files}
        ${attachmentId}=    Obtenir la valeur d'un élément via son JsonPath   json=${response.text}   jsonPath=$[0].id
        Append To List  ${attachmentIds}  ${attachmentId}
    END

    [Return]    ${attachmentIds}

supprimer des pièces jointes
    [Documentation]
    ...                 Supprimer des pièces jointes
    ...                 DELETE /rest/api/2/attachment/{id}
    ...
    ...                 *Arguments :*
    ...                       - ``attachmentIds`` : la liste des pièces jointes
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Argument 1=  | =Argument 2= |
	...					| ${testExecutionKeys}= | supprimer des pièces jointes | ${attachmentIds} |
    [Arguments]     ${attachmentIds}

    ${sessionName}=       rest.obtenir session basic   restServer=${API_REST_JIRA_SERVER}   user=%{JIRA_USERNAME}   password=%{JIRA_PASSWORD}

    FOR  ${attachmentId}  IN  @{attachmentIds}
        ${uri}=    Set Variable    ${API_REST_JIRA_URI}/attachment/${attachmentId}
        ${response}=    rest.DELETE   sessionName=${sessionName}   restUri=${uri}
        Should Be Equal As Integers   204    ${response.status_code}   La pièce jointe ${attachmentId} n'a pas été correctement supprimée
    END
