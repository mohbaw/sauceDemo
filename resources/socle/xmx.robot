*** Settings ***
Documentation       Ressource qui permet de réaliser des appels XMX (CNP).
...
...                 Variables d'environnement
...                     - ``ROBOT_APPS`` : Chemin vers les application de l'architecture robot (exemple : C:\robot\app)

Library             Process
Library             String
Resource            log.robot

*** Variables ***
${XMX_JARFILE_NAME}     xmx-commandline-9.1.4-bin.jar

*** Keywords ***
Appel QR
    [Documentation]  Permet l'appel QR en mode XMX-ECI
    ...                 *Arguments :*
    ...                       - ``in_filepath`` : chemin du fichier d'entrée au format XML
    ...                       - ``cics`` : le cics cible
    ...                       - ``user`` : le user RACF
    ...                       - ``qr`` : le code de la QR à appeler
    ...
    ...                 *Return :*
    ...                       - ``resultCode`` : 0 si l'appel s'est bien passé, différent de 0 sinon
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Chemin du flux d'entrée=  | =nom du CICS= | =user RACF= | =Numéro de la QR= |
	...					| ${response}= | xmx.Appel QR | ./000-QR920.xml | CIEAPOR1 | TEWMC201 | QR920 |

    [Arguments]  ${in_filepath}  ${cics}  ${user}  ${qr}  ${out_filepath}=${qr}_OUT.xml

    Log To Console  ${EMPTY}    # Sert uniquement à un affichage propre
    Log.Info  Appel à la ${qr} sur ${cics} avec le user ${user}
    ${cics_securise}=   Replace String Using Regexp  ${cics}  ^C  S
    ${result}=    Run Process
    ...             java  -jar  %{ROBOT_APPS}${/}xmx${/}${XMX_JARFILE_NAME}
    ...                             --cics              ${cics}
    ...                             --cics-securise     ${cics_securise}
    ...                             --qr                ${qr}
    ...                             --input             ${in_filepath}
    ...                             --user              ${user}
    ...                             --output            ${out_filepath}
    ...                             --verbose
    ...             alias=appel_xmx_${qr}
    ...             stdout=xmx_out.log
    ...             stderr=xmx_err.log

    Run Keyword If  '${result.rc}'=='0'   Log.Success  L'appel à la ${qr} sur ${cics} s'est déroulé correctement
    ...       ELSE  Log.Error  Une erreur s'est produite dans l'appel à la ${qr} sur ${cics}

    [Return]    ${result.rc}