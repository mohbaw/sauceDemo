# -*- coding: utf-8 -*-
*** Settings ***
Documentation        Ressource wrapper de AppiumLibrary pour iOS.
...
...                 Cette librairie permet de démarrer une application sur un
...                 appareil iOS.
...
...                 = /!\\ Ne pas utiliser /!\\ =
...                 *[http://localhost:7070/doc/keywords/14/|Appium] effectue*
...                 *l'aiguillage vers cette librairie selon l'appareil utilisé.*

Library             OperatingSystem

Resource            appium.robot
Resource            log.robot


*** Keywords ***
Deployer Et Ouvrir L'Application
    [Documentation]     Déployer et ouvrir une application sur un appareil Apple.
    ...
    ...                 *Arguments :*
    ...                 - ``myApp``         est le nom de l'application à deployer.
    ...                 - ``myDevice``      est le type de l'appareil utilisé.
    ...                 _Appium iOS._
    [Arguments]         ${myApp}    ${myDevice}

    ${uri2appium}=      appium.Obtenir L'Uri Vers Appium Server
    Open Application    ${uri2appium}
    ...                 platformName=iOS
    ...                 platformVersion=${C4_PIC_DEVICES.${myDevice}platformVersion}
    ...                 deviceName=${C4_PIC_DEVICES.${myDevice}deviceName}
    ...                 udid=${C4_PIC_DEVICES.${myDevice}udid}
    ...                	app=${C4_PACKAGES.ios}/${myApp.iOSapp}
    ...                 xcodeConfigFile=${C4_PACKAGES.ios}/${myApp.iOSxcodeConfigFile}
    ...                	bundleId=${myApp.iOSbundleId}
    ...                 autoAcceptAlerts=true
    ...                 locationServicesAuthorized=true
    #...                  updatedWDABundleId=com.carrefour.inhouse.wdar


Ouvrir L'Application
    [Documentation]     Ouvrir une application sur un appareil Apple.
    ...
    ...                 *Arguments :*
    ...                 - ``myApp``         est le nom de l'application à ouvrir.
    ...                 - ``myDevice``      est le type de l'appareil utilisé.
    ...                 _Appium iOS._
    [Arguments]         ${myApp}    ${myDevice}

    ${uri2appium}=      appium.Obtenir L'Uri Vers Appium Server
    Open Application    ${uri2appium}
    ...                 platformName=iOS
    ...                 platformVersion=${C4_PIC_DEVICES.${myDevice}platformVersion}
    ...                 deviceName=${C4_PIC_DEVICES.${myDevice}deviceName}
    ...                 udid=${C4_PIC_DEVICES.${myDevice}udid}
    ...                	xcodeConfigFile=${C4_PACKAGES.ios}/${myApp.iOSxcodeConfigFile}
    ...                	bundleId=${myApp.iOSbundleId}
    #...                  updatedWDABundleId=com.carrefour.inhouse.wdar
