*** Settings ***
Library           ../utils/PandasLibrary.py

*** Keywords ***
Recuperer une valeur dans une colonne d'apres une valeur presente dans une autre colonne
    [Arguments]    ${myFichierExcel}    ${myFeuilleExcel}    ${myColonneCible}    ${myColonneRef}    ${myValeurRef}

    ${resultat}     PandasLibrary.Recuperer une valeur dans une colonne dapres une valeur presente dans une autre colonne    ${myFichierExcel}    ${myFeuilleExcel}    ${myColonneCible}    ${myColonneRef}    ${myValeurRef}

    [Return]    ${resultat}


Recuperer une ligne du fichier Excel correspondant a une valeur d'une colonne
    [Arguments]    ${myFichierExcel}    ${myFeuilleExcel}    ${myColonneCible}    ${myValeurCible}

    ${resultat}    PandasLibrary.Recuperer une ligne du fichier Excel correspondant a une valeur d une colonne    ${myFichierExcel}    ${myFeuilleExcel}    ${myColonneCible}    ${myValeurCible}

    [Return]    ${resultat}
