# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource wrapper de ExcelLibrary.
...
...                 Cette librairie reprend les fonctionnalités présentes dans la librairie
...                 d'origine et propose des mots-clés en français.
...
...                 = Fonctionnalités =
...                 La libraire ``excel.robot`` permet d'ouvrir, de sélectionner et d'enregistrer des fichiers Excel.
...                 Elle peut également écrire des données et en récupérer.
...
...                 == Gestion du fichier ==
...                 Parmi les fonctionnalités de gestion du fichier Excel, cette librairie propose
...                 d'ouvrir un fichier dont on donne le chemin d'accès. Elle est aussi capable de
...                 se positionner sur un fichier Excel déjà ouvert. Enfin, elle peut enregistrer un
...                 fichier à l'endroit indiqué en argument et fermer Excel.
...
...                 Lorsqu'un fichier Excel est en focus du robot, le robot peut compter le nombre
...                 de lignes et de colonnes existantes.
...
...                 == Ajouter et récupérer des données ==
...                 Quel que soit le type d'ajout à effectuer, il faut connaître la
...                 feuille sur laquelle se trouve la case à remplir. Pour écrire dans
...                 une cellule, il faut connaître soit l'identifiant de la cellule,
...                 soit son numéro de ligne et de colonne.
...
...                 Il est aussi possible de rajouter une ligne dans la feuille.
...
...                 Pour récupérer des données, le même principe que pour insérer du texte
...                 s'applique. Il faut connaître l'identifiant de la cellule ou ses coordonnées.
...
...                 === Plus d'informations ===
...                 [http://navinet.github.io/robotframework-excellibrary/ExcelLibrary-KeywordDocumentation.html|Cliquez ici pour plus d'information sur ExcelLibrary.]

Library             XlsxLibrary

*** Keywords ***
# ==============================================================================
# Gérer le fichier Excel =======================================================
# ==============================================================================

Copier La Plage De Cellules Du Classeur Courant Vers L'Onglet
    [Documentation]    copier la plage nommée du classeur courant vers l'onglet du classeur courant
    [Arguments]    ${myOriginRange}    ${myTargetRange}

    XlsxLibrary.copy named range to workbook    ${myOriginRange}    ${myTargetRange}
    log.debug    La plage feuille ${myOriginRange} est copiee dans ${myTargetRange}


Copier La Feuille Du Classeur Vers L'Onglet Du Classeur Courant
    [Documentation]    copier la feuille vers le classeur courant
    [Arguments]    ${path2WbOrigin}    ${wsOriginName}    ${wsTargetName}    ${myOriginRange}=DEBUT    ${myTargetRange}=DEBUT

    XlsxLibrary.copy sheet to workbook    ${path2WbOrigin}    ${wsOriginName}    ${wsTargetName}    ${myOriginRange}    ${myTargetRange}

    log.debug    La feuille ${path2WbOrigin}/${wsOriginName} est copiee dans l'onglet ${wsTargetName}


Ouvrir Excel Avec Le Classeur
    [Documentation]    Ouvrir l'application Excel windows avec le classeur
    [Arguments]    ${path2WbOrigin}    ${estVisible}=${FALSE}

    XlsxLibrary.start excel application with workbook    ${path2WbOrigin}    myVisibility=${estVisible}
    log.debug    Le classeur ${path2WbOrigin} est ouvert avec Excel


Sauvegarder Et Fermer Excel
    [Documentation]    Sauvegarder le classeur courant et fermer l'application Excel windows

    XlsxLibrary.save and close excel application
    log.debug    Excel est clos


Rafraichir Tout Le Classeur
    [Documentation]    mise a jour des TCD

    XlsxLibrary.refresh all workbook
    log.debug    Le classeur courant a ete mis a jour


Ouvrir Le Fichier Excel
    [Documentation]     Ouvrir un fichier Excel.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2ExcelFile``      est le chemin vers le fichier Excel.
    [Arguments]         ${myPath2ExcelFile}

    XlsxLibrary.Open Excel   ${myPath2ExcelFile}


Attraper Un Classeur Ouvert
    [Documentation]     Retourner sur un fichier Excel deja ouvert.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Wb``       ?
    [Arguments]         ${myPath2Wb}

    XlsxLibrary.Attach Excel With Workbook    ${myPath2Wb}
    log.Debug   Le fichier Excel ${myPath2Wb} est attrape


Enregistrer Le Fichier Excel
    [Documentation]     Enregistrer un fichier Excel ouvert précédemment.
    ...
    ...                 *Arguments :*
    ...                 - ``myPathToSave``      est le chemin d'enregistrement du fichier.
    [Arguments]         ${myPathToSave}

    XlsxLibrary.Save Excel  ${myPathToSave}


Enregistrer Et Fermer Excel
    [Documentation]     Enregistrer le classeur courant et fermer Excel.

    XlsxLibrary.Save And Close Excel Application
    log.Debug    Excel est clos


Fermer Excel
    [Documentation]     Fermer l'application Excel Windows.

    XlsxLibrary.Close Excel Application
    log.Debug   Excel est clos


Compter Le Nombre De Lignes De La Feuille Excel
    [Documentation]     Compter les lignes de la feuille Excel.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est la feuille de travail dont on veut le nombre de lignes.
    ...                 *Return* : le nombre de ligne de la feuille.
    [Arguments]         ${mySheet}

    ${rowCount}=    XlsxLibrary.Get Row Count   ${mySheet}

    [Return]        ${rowCount}


Compter Le Nombre De Colonnes De La Feuille Excel
    [Documentation]     Compter les colonnes de la feuille Excel.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est la feuille de travail dont on veut le nombre de lignes.
    ...                 *Return* : le nombre de colonne de la feuille.
    [Arguments]         ${mySheet}

    ${colCount}=    XlsxLibrary.Get Column Count   ${mySheet}

    [Return]        ${colCount}


Recuperer la liste des feuilles Excel
    [Documentation]     Lister les feuilles du fichier Excel.

    ${list}=   XlsxLibrary.get_sheet_names

    [Return]    ${list}


# ==============================================================================
# Ecrire des données ===========================================================
# ==============================================================================
Ecrire Dans Une Cellule
    [Documentation]     Ecrire dans une cellule excel avec ses coordonnees.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est le nom de la feuille de travail.
    ...                 - ``myColumn``      est le numéro de la colonne.
    ...                 - ``myRow``         est le numéro de la ligne.
    ...                 - ``myString``      est la chaîne de caractères à écrire.
    [Arguments]         ${mySheet}    ${myColumn}    ${myRow}    ${myString}

    XlsxLibrary.Set Cell Value By Coordinate    ${mySheet}    ${myRow}    ${myColumn}    ${myString}


Ecrire Dans Une Cellule Par Nom
    [Documentation]     Ecrire dans une cellule Excel avec son nom.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est le nom de la feuille de travail.
    ...                 - ``myCell``        est le nom de la cellule dans laquelle écrire.
    ...                 - ``myString``      est la chaîne de caractères à écrire.
    [Arguments]         ${mySheet}    ${myCell}    ${myString}

    XlsxLibrary.Put String To Cell    ${mySheet}    ${myCell}    ${myString}


Ajouter Une Ligne Dans La Feuille Excel
    [Documentation]     Ajouter une ligne dans la feuille excel et formate le nombre des cellules.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est le nom de la feuille de travail.
    ...                 - ``myRow``         est le numero de la ligne.
    ...                 - ``myRowFormat``   est le format d'ecriture de la ligne.
    [Arguments]         ${mySheet}    ${myRow}    ${myRowFormat}=${EMPTY}

    XlsxLibrary.Append Row To Worksheet    ${mySheet}    ${myRow}    ${myRowFormat}
    log.Debug    Ajout de la ligne ${myRow} dans la feuille ${mySheet} effectue


# ==============================================================================
# Obtenir des données ==========================================================
# ==============================================================================
Obtenir La Valeur D'Une Cellule Par Plage Nommee
    [Documentation]     Récupérer le contenu d'une cellule Excel avec son nom de plage.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``   est le nom de la feuille de travail.
    ...                 - ``myRangeName``   est le nom de la plage nommee
    ...                 *Return* : la valeur de la cellule.
    [Arguments]         ${mySheet}    ${myRangeName}

    ${passed}         ${value}=   Run Keyword And Ignore error    XlsxLibrary.Get Cell Value By Range Name    ${mySheet}    ${myRangeName}
    ${returnValue}=     Set Variable If    '${passed}'=='PASS'    ${value}    ${EMPTY}

    [Return]            ${returnValue}



Obtenir Le Nom De La Colonne A Partir De Son index
    [Documentation]     Récupérer la référence d'une colonne Excel qà partir de son index.
    ...
    ...                 *Arguments :*
    ...                 - ``myIndex``       est l'index de la colonnne.
        ...                 *Return* :      la reference de la colonne.
    [Arguments]         ${myIndex}

    ${letter}=       XlsxLibrary.get_column_letter_from_index   ${myIndex}

    [Return]        ${letter}



Obtenir La Cellule Qui Contient La valeur
    [Documentation]     Récupérer la référence d'une cellule Excel qui contient une valeur spécifique.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est le nom de la feuille de travail.
    ...                 - ``myValue``       est la valeur cherchée.
    ...                 *Return* :      la reference de la cellule.
    [Arguments]         ${mySheet}    ${myValue}

    ${cellRef}=       XlsxLibrary.find_cell_with_specific_value    ${mySheet}    ${myValue}

    [Return]        ${cellRef}



Obtenir La Reference Excel
    [Documentation]     Obtenir la reference a partir du chemin du fichier et de la feuille.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2ExcelFile``  est le chemin vers le fichier Excel.
    ...                 - ``mySheetName``       est le nom de la feuille de travail.
    [Arguments]         ${myPath2ExcelFile}    ${mySheetName}

    # xlRef est de la forme <chemin>\<fichier>.xlsx!feuille
    OperatingSystem.File Should Exist    ${myPath2ExcelFile}    Le fichier Excel ${myPath2ExcelFile} n'existe pas
    ${xlRef}=       Set Variable    ${myPath2ExcelFile}!${mySheetName}

    [Return]        ${xlRef}


Obtenir Le Nom De La Feuille
    [Documentation]     Obtenir le nom de la feuille à partir de la référence.
    ...
    ...                 *Arguments :*
    ...                 - ``myRef``     est la référence à chercher _via_ une expression régulière.
    ...                 *Return* : le chemin vers le fichier et le nom de la feuille.
    [Arguments]         ${myRef}

    # myRef est de la forme <chemin>\<fichier>.xlsx!feuille
    ${wsNames}=     String.Get Regexp Matches    ${myRef}    !(.*)$    1
    ${isFound}=     Get Length    ${wsNames}
    ${wsName}=      Set Variable If    ${isFound}>0    ${wsNames[0]}    ${EMPTY}
    ${path2files}=  String.Get Regexp Matches    ${myRef}    (.*)!    1
    ${isFound}=     Get Length    ${path2files}
    ${path2file}=   Set Variable If    ${isFound}>0    ${path2files[0]}    ${EMPTY}

    [Return]        ${path2file}    ${wsName}


Obtenir Les Noms Des Champs Du Fichier Excel
    [Documentation]     Extraction des noms des champs du fichier (nom des colonnes de la premiere ligne).
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin vers le fichier.
    ...                 - ``mySheet``       est le nom de la feuille de travail.
    ...                 *Return* : les entêtes de la feuille.
    [Arguments]         ${myPath2File}    ${mySheet}

    # On prend a partir de la premiere ligne
    excel.Ouvrir Le Fichier Excel    ${myPath2File}
    ${nbCols}=          excel.Compter Le Nombre De Colonnes De La Feuille Excel    ${mySheet}
    ${allFieldNames}=   Create List

    FOR    ${colIndex}    IN RANGE    ${nbCols}
        # Eliminer les champs de description du cas de test (meta donnees dont le nom commence par '_')
        ${aFieldName}=    excel.Obtenir La Valeur D'Une Cellule    ${mySheet}    ${colIndex}    0
        collections.Append To List    ${allFieldNames}    ${aFieldName}
    END

    [Return]    ${allFieldNames}


Obtenir La Valeur D'Une Cellule
    [Documentation]     Récupérer le contenu d'une cellule Excel avec ses coordonnées.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est le nom de la feuille de travail.
    ...                 - ``myColumn``      est le numéro de la colonne.
    ...                 - ``myRow``         est le numéro de la ligne.
    ...                 *Return* : la valeur de la cellule.
    [Arguments]         ${mySheet}    ${myColumn}   ${myRow}

    ${value}=       XlsxLibrary.Get Cell Value By Coordinate    ${mySheet}    ${myRow}    ${myColumn}

    [Return]        ${value}


Obtenir La Valeur D'Une Cellule Par Nom
    [Documentation]     Récupérer le contenu d'une cellule Excel avec son nom.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``   est le nom de la feuille de travail.
    ...                 - ``myCell``    est le nom de la cellule dans laquelle écrire.
    ...                 *Return* : la valeur de la cellule.
    [Arguments]         ${mySheet}      ${myCell}

    ${passed}         ${value}=   Run Keyword And Ignore error    XlsxLibrary.Read Cell Data By Name    ${mySheet}    ${myCell}
    ${returnValue}=     Set Variable If    '${passed}'=='PASS'    ${value}    ${EMPTY}

    [Return]            ${returnValue}


Obtenir La Valeur De La Colonne Utilisant Expression Reguliere
    [Documentation]     Rechercher une valeur dans une colonne qui correspond à une expression régulière.
    ...
    ...                 *Arguments :*
    ...                 - ``mySheet``       est le nom de la feuille de travail.
    ...                 - ``myColumnId``    est l'id de la colonne recherchée.
    ...                 - ``myRegExp``      est l'expression régulière utilisée pour retrouver la valeur.
    ...                 *Return* : la valeur correspondante a l'expression regulière.
    [Arguments]         ${mySheet}    ${myColumnId}    ${myRegExp}

    ${passed}    ${value}=    Run Keyword And Ignore Error    XlsxLibrary.Get Cell Value In Column Matching Regexp    ${mySheet}    ${myColumnId}    ${myRegExp}
    # En cas d'erreur, le message d'erreur est retourne dans value
    Run Keyword If  '${passed}'=='FAIL'    Fail    Je n'ai pas acces a colonne de la feuille excel ${mySheet}: ${myColumnId}
    Run Keyword If  '${value}' == '${EMPTY}'    log.Warning    Aucune correspondance trouve pour ${myRegExp} dans la colonne ${myColumnId} de la feuille excel ${mySheet}
    ...    ELSE     log.Success    J'ai trouve la valeur ${value} qui correspond a l'expression reguliere ${myRegExp} dans la colonne ${myColumnId} de la feuille excel ${mySheet}

    [return]        ${value}


Lire Le Fichier Excel Dans Un Dictionnaire
    [Documentation]     Charger le fichier excel en tant que liste (les lignes) de dictionnaire (nomChamp=valeur).
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin vers le fichier Excel.
    ...                 - ``mySheet``       est la feuille de travail dont on veut extraire les données.
    ...                 *Return* : le fichier Excel sous forme de dictionnaire Python.
    [Arguments]         ${myPath2File}    ${mySheet}

    ${listRows}=    Create List
    excel.Ouvrir Le Fichier Excel    ${myPath2File}
    ${nbRows}=      excel.Compter Le Nombre De Lignes De La Feuille Excel    ${mySheet}

    # on ne traite pas la ligne en-tete donc on commence a 1
    FOR    ${rowIndex}    IN RANGE    1    ${nbRows}
        ${rowDict}=     excel._Charger La Ligne Excel Dans Un Dictionnaire    ${myPath2File}    ${mySheet}    ${rowIndex}
        # ajouter le dictionnaire a la liste des lignes
        Collections.Append To List    ${listRows}    ${rowDict}
    END

    [Return]    ${listRows}

Lire Le Fichier Excel Dans Un Dictionnaire De Dictionnaires
    [Documentation]     Charger le fichier excel en tant que dictionnaire avec le nom du test comme clé où chaque ligne
    ...                 est aussi un dictionnaire (nom de la colonne =valeur).
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin vers le fichier Excel.
    ...                 - ``mySheet``       est la feuille de travail dont on veut extraire les données.
    ...                 *Return* : le fichier Excel sous forme de dictionnaire Python.
    [Arguments]         ${myPath2File}    ${mySheet}

    ${dictRows}=    Create Dictionary
    excel.Ouvrir Le Fichier Excel    ${myPath2File}
    ${nbRows}=      excel.Compter Le Nombre De Lignes De La Feuille Excel    ${mySheet}

    # on ne traite pas la ligne en-tete donc on commence a 1
    FOR    ${rowIndex}    IN RANGE    1    ${nbRows}
        ${rowDict}=     excel._Charger La Ligne Excel Dans Un Dictionnaire    ${myPath2File}    ${mySheet}    ${rowIndex}
        ${rowKey}=  Get From Dictionary  ${rowDict}  *** Test Cases ***
        # ajouter le dictionnaire de la ligne comme valeur au dictionnaire des lignes
        Set To Dictionary  ${dictRows}  ${rowKey}  ${rowDict}
    END

    [Return]    ${dictRows}

Lire Le Fichier Excel API Dans Un Dictionnaire
    [Documentation]     Charger le fichier excel en tant que liste (les lignes) de dictionnaire (nomChamp=valeur).
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin vers le fichier Excel.
    ...                 - ``mySheet``       est la feuille de travail dont on veut extraire les données.
    ...                 *Return* : le fichier Excel sous forme de dictionnaire Python.
    [Arguments]         ${myPath2File}    ${mySheet}

    &{sheet}=    Create Dictionary
    excel.Ouvrir Le Fichier Excel    ${myPath2File}
    ${nbRows}=      excel.Compter Le Nombre De Lignes De La Feuille Excel    ${mySheet}

    # on ne traite pas la ligne en-tete donc on commence a 1
    FOR    ${rowIndex}    IN RANGE    1    ${nbRows}
        ${testName}=    excel.Obtenir La Valeur D'Une Cellule  ${mySheet}  0   ${rowIndex}
        ${rowDict}=     excel._Charger La Ligne Excel Dans Un Dictionnaire    ${myPath2File}    ${mySheet}    ${rowIndex}
        # ajouter au dictionnaire global
        Set To Dictionary    ${sheet}    ${testName}=${rowDict}
    END

    [Return]    ${sheet}

# ==============================================================================
# Mots-clés privés =============================================================
# ==============================================================================
_Charger La Ligne Excel Dans Un Dictionnaire
    [Documentation]     Construire le dictionnaire associé à la ligne (nomChamp=valeur).
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin vers le fichier Excel.
    ...                 - ``mySheet``       est la feuille de travail dont on veut extraire les données.
    ...                 - ``myIndexRow``    est l'index de la ligne à charger dans le dictionnaire.
    ...                 *Return* : la ligne designée sous forme de dictionnaire Python.
    [Arguments]         ${myPath2File}    ${mySheet}    ${myIndexRow}

    excel.Ouvrir Le Fichier Excel    ${myPath2File}
    ${nbCols}=      excel.Compter Le Nombre De Colonnes De La Feuille Excel    ${mySheet}
    ${rowDict}=     Create Dictionary

    FOR    ${colIndex}    IN RANGE    ${nbCols}
        # obtenir le nom du champ a partir de la ligne entete
        ${fieldName}=       excel.Obtenir La Valeur D'Une Cellule    ${mySheet}    ${colIndex}    0
        # obtenir la value du champ associe a la ligne
        ${fieldValue}=      excel.Obtenir La Valeur D'Une Cellule    ${mySheet}    ${colIndex}    ${myIndexRow}
        # construire le dictionnaire, ne pas tenir compte des colonnes vides
        ${is_none}=	Run Keyword And Return Status	Should Be Equal	${fieldName}	    ${None}
        Run Keyword Unless    ${is_none}    collections.Set To Dictionary    ${rowDict}    ${fieldName}=${fieldValue}
    END

    [Return]    ${rowDict}
