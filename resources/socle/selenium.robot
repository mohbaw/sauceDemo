*** Settings ***
Documentation       Ressource wrapper de la SeleniumLibrary.
...
...                 Cette librairie reprend les fonctionnalités présentes dans la librairie
...                 d'origine et propose des mots-clés en français.
...                 = Cas d'utilisation =
...                 Cette librairie est utilisée pour automatiser des tests sur des
...                 navigateurs web depuis un ordinateur. Pour automatiser des tests
...                 sur des appareils mobiles comme des tablettes, des smartphones ou
...                 autre, veuillez vous référer à la librairie
...                 [http://serhatbolsu.github.io/robotframework-appiumlibrary/AppiumLibrary.html|AppiumLibrary]
...                 , voir ``appium.robot`` pour la version française du socle.
...
...                 Pour utiliser ``selenium.robot``, un WebDriver doit se situer dans les variables
...                 de chemins (_path_) ou doit être déjà démarré sur l'ordinateur l'utilisant.
...                 = Localiser des éléments =
...                 Pour localiser un élément avec Selenium, il existe un type d'argument appelé ``locator``.
...                 La plupart du temps, un ``locator`` est donné sous forme de chaîne de caractères suivant
...                 une syntaxe précise, décrite ci-dessous.
...
...                 La librairie peut localiser de diverses manières un élément, que ce soit par ID de l'élément,
...                 les expressions par Xpath, ou bien les sélecteurs CSS. La choix de la stratégie de reconnaissance
...                 de l'élément depuis le sélecteur donné est effectué automatiquement.
...                 = Temps d'attente =
...                 Tous les éléments qui nécessitent d'attendre pour s'assurer qu'ils sont bien
...                 présents disposent d'un temps d'attente de 5 secondes pour la plupart.
...                 Ce temps reste accessible à tout changement, dans le cas où un site
...                 répondrait plus lentement que la moyenne, il serait alors possible de
...                 redéfinir la valeur du temps d'attente. Pour donner un temps d'attente,
...                 il faut écrire "5s" pour donner 5 secondes et non pas seulement 5.
...                 Exemple :
...                 |   selenium.aller vers le site \ \ \ ``http://www.google.com`` \ \ \ Google \ \ \ 30s
...                 Dans cet exemple, une requête est effectuée pour accéder au site "Google.com".
...                 Lors de l'arrivée sur le site, on s'attend à trouver "Google" d'écrit, ce qui
...                 confirmerait que le bon site a été atteint. Enfin, le troisième argument indique
...                 que l'utilisateur accepte d'attendre jusqu'à 30 secondes avant de considérer que le
...                 site est inaccessible. Par défaut, à la place de 30 secondes, le temps d'attente
...                 est basé à 10 secondes, pour ce cas précis.
...                 = Usages =
...                 Dans cette librairie, plusieurs façons d'effectuer des mêmes actions ont
...                 été conçues. Par exemple, pour cliquer il existe les méthodes suivantes :
...                 | = Nom = | = Fonctionnalité = |
...                 | cliquer sur un element visible et actif | Clique en utilisant l'API Selenium. |
...                 | cliquer avec webdriver | Clique sur un élément depuis le WebDriver. |
...                 | cliquer avec javascript | Exécute un script permettant de cliquer sur la page. |
...                 Il en est de même avec les différents types de saisies possibles. Une saisie peut être
...                 soit tracée, soit invisible (dans le cas de mots de passe), il est aussi possible
...                 de saisir du texte avec l'API, avec le WebDriver et avec JavaScript.

Library             BrowserMobProxyLibrary
Library             OperatingSystem
Library             SeleniumLibrary    run_on_failure=Nothing
Library             Dialogs
# Resource            autowin.robot
Resource            Log.robot
Resource            utils.robot


*** Variables ***
#${WEB_PATH2BMP}           %{ROBOT_HOME}${/}app${/}Portable-Python-Robot-2711.300.1.3${/}App${/}Lib${/}site-packages${/}browsermobproxy
# ${WEB_PATH2WEBDRIVER}    ${CURDIR}${/}..${/}resources${/}Webdriver
${PATH_CHROMIUM}           C:/robot/app/Chromium/chrome.exe
# ${PATH_CHROME}           C:/Program Files (x86)/Google/Chrome/Application/chrome.exe

# Modify Headers and Header Tool extensions
&{WEB_PROXY_AUTH}
                          # Authentification proxi
...                       name=Authorization    value=Basic
                          # extension chrome
...                       crx=modheader.crx     modHeaderId=idgpnmonknjnojddfkpgkljpfnnfcklj
                          # extension firefox
...                       xpi=header_tool-0.6.2-fx.xpi
...                       path2profile=

${SELENIUM_INIT_WAIT}     10
# ${PAUSE_EXECUTION}        False


*** Keywords ***
Determiner Le Repertoire Des Captures D'Ecran
    [Arguments]    ${myPath}

    SeleniumLibrary.Set Screenshot Directory    ${myPath}


# ==============================================================================
# Ouvrir navigateur ============================================================
# ==============================================================================
# TODO REFACTO
Ouvrir Chrome Avec Auth Pour Pdf
    [Documentation]     Ouvrir un fichier Pdf avec un navigateur.
    ...
    ...                 _Version Selenium._

    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    # enable FlashPlayer
    ${prefs}    Create Dictionary    profile.default_content_setting_values.plugins=1    profile.content_settings.plugin_whitelist.adobe-flash-player=1    profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player=1    download.default_directory=%{WORKSPACE}
    # launch webdriver with modHeader extension
    ${options}=    Evaluate      sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${options}    add_extension    ${CURDIR}${/}..${/}resources${/}extensions${/}${WEB_PROXY_AUTH.crx}
    Call Method    ${options}    add_experimental_option    prefs    ${prefs}
    OperatingSystem.Set Environment Variable        PATH    %{PATH};${CURDIR}${/}..${/}resources${/}webdriver
    SeleniumLibrary.Create Webdriver    Chrome      chrome_options=${options}
    # setup headers
    SeleniumLibrary.Go To    chrome-extension://${WEB_PROXY_AUTH.modHeaderId}/icon.png
    SeleniumLibrary.Execute Javascript    localStorage.setItem('profiles', JSON.stringify([{title: 'Selenium', hideComment: true, appendMode: '',headers: [{enabled: true, name: '${WEB_PROXY_AUTH.name}', value: '${WEB_PROXY_AUTH.value}', comment: ''}],respHeaders: [],filters: []}]));
# TODO REFACTO


Ouvrir Chrome
    [Documentation]     Ouvrir Google Chrome sur le device.
    ...
    ...                 _Version Selenium._
    [Arguments]         ${myAlias}=None

    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    ${chrome_options}=                      web.Definir Les Capacites Chrome
    # ${chrome_options.binary_location}=      Set Variable        ${PATH_CHROME}
    # ${chrome_capabilities}=                 Call Method         ${chrome_options}    to_capabilities
    SeleniumLibrary.Create Webdriver        Chrome    alias=${myAlias}    chrome_options=${chrome_options}
    SeleniumLibrary.Maximize Browser Window


Ouvrir Chromium
    [Documentation]     Ouvrir Chromium sur le device.
    ...
    ...                 _Version Selenium._
    [Arguments]         ${myAlias}=None

    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    ${chrome_options}=                      web.Definir Les Capacites Chromium
    ${chrome_options.binary_location}=      Set Variable        ${PATH_CHROMIUM}
    # ${chrome_capabilities}=                 Call Method         ${chrome_options}    to_capabilities
    SeleniumLibrary.Create Webdriver        Chrome    alias=${myAlias}    chrome_options=${chrome_options}
    SeleniumLibrary.Maximize Browser Window


Ouvrir ChromeDistant
    [Documentation]     Ouvrir Google Chrome de manière distante sur le device.
    ...
    ...                 _Version Selenium._

    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    ${chrome_options}=      web.Definir Les Capacites Chrome
    ${chrome_capabilities}=     Call Method     ${chrome_options}    to_capabilities
    Set To Dictionary       ${chrome_capabilities}    enableVNC=${true}
    SeleniumLibrary.Create Webdriver        Remote   command_executor=http://137.74.24.198:10444/wd/hub    desired_capabilities=${chrome_capabilities}
    SeleniumLibrary.Maximize Browser Window


Ouvrir Chrome Avec Authentification Proxy
    [Documentation]     Start chrome browser.
    ...                 |   Based on https://stackoverflow.com/questions/37456794/chrome-modify-headers-in-selenium-java-i-am-able-to-add-extension-crx-through
    ...                 _Version Selenium._

    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    # enable FlashPlayer and disable hardware acceleration (https://src.chromium.org/viewvc/chrome/trunk/src/chrome/common/pref_names.cc?view=markup)
    ${prefs}=       Create Dictionary    profile.default_content_setting_values.plugins=1    profile.content_settings.plugin_whitelist.adobe-flash-player=1    profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player=1    download.default_directory=%{WORKSPACE}    hardware_acceleration_mode.enabled=1
    # launch webdriver with modHeader extension
    ${options}=    Evaluate      sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${options}    add_extension    ${CURDIR}${/}..${/}resources${/}extensions${/}${WEB_PROXY_AUTH.crx}
    Call Method    ${options}    add_experimental_option    prefs    ${prefs}
    OperatingSystem.Set Environment Variable    PATH    %{PATH};${CURDIR}${/}..${/}resources${/}webdriver
    SeleniumLibrary.Create Webdriver        Chrome    chrome_options=${options}
    # setup extension to pass  proxy
    SeleniumLibrary.Go To                   chrome-extension://${WEB_PROXY_AUTH.modHeaderId}/icon.png
    SeleniumLibrary.Execute Javascript      localStorage.setItem('profiles', JSON.stringify([{title: 'Selenium', hideComment: true, appendMode: '',headers: [{enabled: true, name: '${WEB_PROXY_AUTH.name}', value: '${WEB_PROXY_AUTH.value}', comment: ''}],respHeaders: [],filters: []}]));
    SeleniumLibrary.Maximize Browser Window


Ouvrir Chrome Avec Profil
    [Documentation]     DEPRECATED - Ouvrir Chrome sur un profil utilisateur.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Profile``      le chemin vers le profil à charger.
    ...                 _Version Selenium._
    [Arguments]         ${myPath2Profile}

    # on utilise le profile chrome  qui contient l'autentification dans l'extension "Modify Headers"
    ${options}=    Evaluate      sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${options}    add_argument    --user-data-dir\=${myPath2profile}

    # Lancement du navigateur chrome
    log.Debug    Ouverture du navigateur Chrome sur le profil '${myPath2Profile}'
    SeleniumLibrary.Create Webdriver    Chrome    chrome_options=${options}
    SeleniumLibrary.Maximize Browser Window
    SeleniumLibrary.Delete All Cookies


Ouvrir Firefox
    [Arguments]    ${myAlias}=None
    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    ${ff_profile}=    selenium.Definir Le Profil Firefox
    SeleniumLibrary.Create Webdriver    Firefox    alias=${myAlias}    firefox_profile=${ff_profile}
    #SeleniumLibrary.Create Webdriver    Firefox    alias=${myAlias}
    SeleniumLibrary.Maximize Browser Window


Definir Le Profil Firefox
    [Documentation]    Firefox profile settings
    [Arguments]  ${my_download_directory}=%{WORKSPACE}

    # Set extension to handle authentication if needed
    ${ff_profile}=    Evaluate
    ...  sys.modules['selenium.webdriver'].FirefoxProfile()
    ...  sys, selenium.webdriver

    # To turns off download prompt
    Call Method    ${ff_profile}    set_preference    browser.download.manager.showWhenStarting    ${False}
    Call Method    ${ff_profile}    set_preference    browser.cache.memory.enable    ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.dir                ${my_download_directory}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.alertOnEXEOpen      ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.focusWhenStarting   ${False}
    #Call Method    ${profile}    set_preference    browser.download.useDownloadDir              ${True}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.useWindow           ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.closeWhenDone       ${True}
    Call Method    ${ff_profile}    set_preference    services.sync.prefs.sync.browser.download.manager.showWhenStarting   ${False}
    Call Method    ${ff_profile}    set_preference    browser.helperApps.neverAsk.openFile         application/csv
    Call Method    ${ff_profile}    set_preference    browser.helperApps.neverAsk.saveToDisk       application/msword, text/xml, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application${/}download, application/octet-stream, application/xls, text/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    Call Method    ${ff_profile}    set_preference    browser.helperApps.alwaysAsk.force           ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.showAlertOnComplete     ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.closeWhenDone       ${False}
    Call Method    ${ff_profile}    set_preference    pdfjs.enabledCache.state                     ${False}
    Call Method    ${ff_profile}    set_preference    browser.link.open_newwindow                  ${2}
    # Call Method  ${ff_profile}  set_preference
    # ...  browser.download.panel.shown
    # ...  ${FALSE}
    # Call Method  ${ff_profile}  set_preference
    # ...  browser.download.folderList
    # ...  2
    # Call Method  ${ff_profile}  set_preference
    # ...  browser.download.dir
    # ...  ${my_download_directory}
    # Call Method  ${ff_profile}  set_preference
    # ...  browser.helperApps.neverAsk.saveToDisk
    # ...  application/pdf;application/zip;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/octet-stream

    [Return]        ${ff_profile}


Ouvrir Firefox Avec BMP
    [Arguments]    ${myBrowser}

    # init BrowserMob Proxy, Automate HTTP Header Basic Authorization
    ${corporateProxy}=    Create Dictionary    httpProxy=%{MY_CORPPROXY}
    Start Local Server    ${WEB_PATH2BMP}
    ${BrowserMob_Proxy}=    Create Proxy    ${corporateProxy}
    ${auth}=    Create Dictionary    Authorization=Basic Z2VuZXJhbGk6Z2VuZXJhbGkyMDE4
    Set Headers    ${auth}
    ${monProxyUrl}    Get Proxy Url
    Log    ${monProxyUrl}
    # TODO pas de chemin absolu
    ${firefox_path}=    Set Variable    C:\\Users\\h0alt28\\Documents\\robot\\App\\FirefoxPortable\\FirefoxPortable.exe
    ${caps}=            Evaluate        sys.modules['selenium.webdriver'].common.desired_capabilities.DesiredCapabilities.FIREFOX    sys
    Collections.Set To Dictionary       ${caps}    marionette=${False}
    SeleniumLibrary.Create Webdriver    Firefox    firefox_binary=${firefox_path}    capabilities=${caps}    proxy=${BrowserMob_Proxy}
    SeleniumLibrary.Go To               about:config
    SeleniumLibrary.Maximize Browser Window
    SeleniumLibrary.Delete All Cookies
    New Har    %{BUILD_TAG}


Ouvrir Firefox Avec Authentification Proxy
    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    # set a new firefox profile with    header_tool extension
    ${profile}=    Evaluate      sys.modules['selenium.webdriver'].FirefoxProfile()    sys, selenium.webdriver
    Call Method    ${profile}    add_extension    ${CURDIR}${/}..${/}resources${/}extensions${/}${WEB_PROXY_AUTH.xpi}
    # setup extension to pass  proxy
    Call Method    ${profile}    set_preference    extensions.headertool.preferencies.editor    ${WEB_PROXY_AUTH.name}:${WEB_PROXY_AUTH.value}@http://vm659.jn-hebergement.com
	Call Method    ${profile}    set_preference    browser.startup.homepage    about:blank
    Call Method    ${profile}    update_preferences
    # open Firefox with profile
    ${WEB_PROXY_AUTH.path2profile}=    Set Variable    ${profile.path}
  	SeleniumLibrary.Open Browser    about:blank    browser=firefox    alias=conquete    ff_profile_dir=${WEB_PROXY_AUTH.path2profile}
	SeleniumLibrary.Maximize Browser Window
    SeleniumLibrary.Delete All Cookies
    # Activate modify headers with keys    F4 + ALT+a + F4 (https://seleniumhq.github.io/selenium/docs/api/py/webdriver/selenium.webdriver.common.keys.html)
    SeleniumLibrary.Press Key    //body    u'\ue034'
    SeleniumLibrary.Press Key    //body    u'\ue00a'a
    SeleniumLibrary.Press Key    //body    u'\ue034'


Ouvrir IE
    [Arguments]    ${myAlias}=None
    SeleniumLibrary.Set Selenium Implicit Wait    ${SELENIUM_INIT_WAIT}
    SeleniumLibrary.Create Webdriver    Ie    ${myAlias}
    SeleniumLibrary.Maximize Browser Window


Ouvrir Internet Explorer
    [Arguments]    ${myAlias}=None
    Ouvrir IE    ${myAlias}


# ==============================================================================
# Se deplacer ==================================================================
# ==============================================================================
Aller Vers Le Site
    [Documentation]     Charger l'URL.
    ...
    ...                 *Arguments :*
    ...                 - ``myUri``             est l'URI vers laquelle se diriger.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour aller vers le site.
    ...                 _Version Selenium._
    [Arguments]         ${myUri}    ${myExpectedText}    ${myTimeout}=10s

    # Aiguillage vers le site de l'environnement de test choisi dans Jenkins - variable MY_PFV
    ${myUrl}=    web.obtenir url a partir uri    ${myUri}
    log.Debug           Je vais vers le site ${myUrl}
    Log  ${myUrl}
    SeleniumLibrary.Go To               ${myUrl}
    # On verifie la presence du titre de la page attendue en fonction de l'environnement
    SeleniumLibrary.Wait Until Page Contains    ${myExpectedText}       ${myTimeout}
    log.Debug                   La page contient le texte '${myExpectedText}'


Aller Vers Le Site En Utilisant L'URL Complete
    [Documentation]     Charger l'URL sans utiliser la variable MY_PFV.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``             est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour aller vers le site.
    ...                 _Version Selenium._
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myTimeout}=10s

    log.Debug           Je vais vers le site ${myUrl}
    Log  ${myUrl}
    SeleniumLibrary.Go To               ${myUrl}
    # On verifie la presence du titre de la page attendue en fonction de l'environnement
    SeleniumLibrary.Wait Until Page Contains    ${myExpectedText}       ${myTimeout}
    log.Debug                   La page contient le texte '${myExpectedText}'


Naviguer Avec Le Browser Vers Le Site
    [Documentation]     Ouvrir le navigateur myBrowser ou MY_BROWSER par défaut, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``             est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myBrowser``         est le navigateur à utiliser.
    ...                 _Version Selenium._
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myBrowser}=%{MY_BROWSER}    ${myAlias}=None

    ${keywordWithBrowser}=    Set Variable    selenium.Ouvrir ${myBrowser}
    Run Keyword    ${keywordWithBrowser}    ${myAlias}
    selenium.Aller Vers Le Site    ${myUrl}    ${myExpectedText}


Naviguer Avec Le Browser Vers Le Site En Utilisant L'URL Complete
    [Documentation]     Ouvrir le navigateur myBrowser ou MY_BROWSER par défaut, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``             est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myBrowser``         est le navigateur à utiliser.
    ...                 _Version Selenium._
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myBrowser}=%{MY_BROWSER}    ${myAlias}=None

    ${keywordWithBrowser}=    Set Variable    selenium.Ouvrir ${myBrowser}
    Run Keyword    ${keywordWithBrowser}    ${myAlias}
    selenium.Aller Vers Le Site En Utilisant L'URL Complete    ${myUrl}    ${myExpectedText}


Naviguer Vers Le Site
    [Documentation]     Ouvrir le navigateur Chrome avec le profil, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``             est l'URL vers laquelle se diriger.
    ...                 - ``myProfile``         est le profil à utiliser.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 _Version Selenium._
    [Arguments]         ${myUrl}    ${myProfile}    ${myExpectedText}

    # on tente d'aller vers le site avec le navigateur existant
    ${estPass}=    Run Keyword And Return Status    selenium.aller vers le site    ${myUrl}    ${myExpectedText}
    # En cas d'echec, on lance le navigateur et on recommence
    Run Keyword Unless    ${estPass}    selenium.Ouvrir Chrome Avec Authentification Proxy
    Run Keyword Unless    ${estPass}    selenium.aller vers le site    ${myUrl}    ${myExpectedText}


Naviguer Vers Le Site Contenant Un Pdf
    [Documentation]     Ouvrir le navigateur Chrome avec le profil, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``             est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myBrowser``         est le navigateur à utiliser.
    ...                 _Version Selenium._
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myBrowser}=%{MY_BROWSER}

    ${keywordWithBrowser}=    Set Variable    selenium.ouvrir ${myBrowser} avec auth pour pdf
    Run Keyword    ${keywordWithBrowser}
    selenium.Aller Vers Le Site    ${myUrl}    ${myExpectedText}


Se Positionner Sur La Fenetre
    [Documentation]     Se positionner sur l'onglet correspondant.
    ...
    ...                 *Arguments :*
    ...                 - ``myTitle``       est l'onglet sur lequel il faut se positionner.
    ...                 _Version Selenium._
    [Arguments]         ${myTitle}

    ${previousWindow}    SeleniumLibrary.Switch Window   ${myTitle}

    [Return]    ${previousWindow}


Effectuer Un Retour Vers La Page Precedente
    [Documentation]    Simuler l'appui sur le bouton Retour du navigateur

    SeleniumLibrary.Go Back


# ==============================================================================
# Recuperer des donnees ========================================================
# ==============================================================================
Obtenir Mon Url
    [Documentation]     Obtenir l'URL courante.
    ...
    ...                 *Return* : l'adresse de la page actuelle.
    ...                 _Version Selenium Only._

    selenium.Confirmer Que La Page Est Prete
    ${currentLocation}    SeleniumLibrary.Get Location

    [Return]    ${currentLocation}


Obtenir Mon Titre
    [Documentation]     Obtenir le titre de la page courante.
    ...
    ...                 *Return* : le titre de la page actuelle.
    ...                 _Version Selenium Only._

    selenium.Confirmer Que La Page Est Prete
    ${title}    SeleniumLibrary.Get Title

    [Return]    ${title}


Obtenir La Cellule Du Tableau
    [Documentation]     Obtenir le contenu d'une cellule precise.
    ...
    ...                 *Arguments :*
    ...                 - ``myTBodyIdLocator``  est la localisation du tableau.
    ...                 - ``myRow``             est la ligne demandée.
    ...                 - ``myCol``             est la colonne demandée.
    ...                 *Return* : la valeur de la cellule.
    ...                 _Version Selenium Only._
    [Arguments]         ${myTBodyIdLocator}    ${myRow}    ${myCol}

    selenium.Confirmer Que La Page Est Prete
    ${cellLocator}=    Set Variable    //tbody[@id='${myTBodyIdLocator}']/tr[${myRow}]/td[${myCol}]
    ${cellValue}=    SeleniumLibrary.Get Text    ${cellLocator}

    [Return]    ${cellValue}


Obtenir Le Texte De L'Attribut
    [Documentation]     Obtenir le texte de l'attribut.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de l'attribut.
    ...                 *Return* : la valeur de la cellule.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myAttribute}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    ${myTimeout}
    END
    ${attributeValue}=    SeleniumLibrary.Get Element Attribute    ${myLocator}    ${myAttribute}

    [Return]    ${attributeValue}


Obtenir Le Texte De L'Element
    [Documentation]     Obtenir le texte de l'élément qui doit être visible.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de l'élément.
    ...                 *Return* : la valeur de la cellule.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    ${myTimeout}
    END
    ${cellValue}=    SeleniumLibrary.Get Text    ${myLocator}

    [Return]    ${cellValue}


Obtenir L'Element Le Plus Proche De La Liste
    [Documentation]     Obtenir l'élément le plus ressemblant de la liste.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``         est la localisation de la liste.
    ...                 - ``myExpectedOption``  est le choix attendu.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver le choix attendu.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myExpectedOption}    ${myTimeout}=5s

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    SeleniumLibrary.Click Element    ${myLocator}
    ${options}=    Wait Until Keyword Succeeds    3x    200ms    Get List Items    ${myLocator}
    ${maxProximityOption}=    utils.Obtenir L'Element Le Plus Proche    ${myExpectedOption}    ${options}

    [Return]    ${maxProximityOption}

#-----------------------------------------------------------------------------------------------------------------
# TODO: Supprimer les mots clés liés aux tableaux si considérés inutiles
#-----------------------------------------------------------------------------------------------------------------
obtenir le nombre de ligne du tableau
    [Arguments]    ${myTableIdLocator}

    Selenium.Confirmer Que La Page Est Prete
    ${count}=  SeleniumLibrary.get element count  //table[@id='${myTableIdLocator}']/tbody/tr

    [Return]    ${count}


obtenir le nombre de ligne du tableau sans locator
    [Arguments]    ${myTablexpath}

    selenium.Confirmer Que La Page Est Prete
    ${count}=  SeleniumLibrary.get element count  ${myTablexpath}/tbody/tr

    [Return]    ${count}


obtenir la cellule du tableau hors entete
    [Arguments]    ${myTableIdLocator}    ${myRow}    ${myCol}

    Selenium.Confirmer Que La Page Est Prete
    ${cellValue}=    SeleniumLibrary.Get Table Cell    ${myTableIdLocator}    ${myRow}    ${myCol}

    [Return]    ${cellValue}
#-----------------------------------------------------------------------------------------------------------------

Obtenir Le Nombre d elements Correspondants Au Locator
    [Arguments]    ${myLocator}

    selenium.Confirmer Que La Page Est Prete
    ${count}=    SeleniumLibrary.Get Element Count    ${myLocator}

    [Return]    ${count}


Le Texte De L'Element Doit Avoir La Valeur Attendue
    [Documentation]    Compare le texte de l'élément avec la valeur attendue
    [Arguments]    ${myLocator}     ${targetValue}

    ${textValue}=    selenium.Obtenir Le Texte De L'Element    ${myLocator}
    Should Be Equal    ${textValue}    ${targetValue}
    ...    msg=Le texte a la valeur ${textValue} alors que la valeur attendue était ${targetValue}    values=False


Le Texte De L'Element Doit Etre Conforme A L'Expression Reguliere
    [Documentation]    Compare le texte de l'élément avec l'expression régulière
    [Arguments]    ${myLocator}     ${myRegexp}

    ${textValue}=    selenium.Obtenir Le Texte De L'Element    ${myLocator}
    Should Match Regexp    ${textValue}    ${myRegexp}
    ...    msg=Le texte a la valeur ${textValue} alors qu'il devait respecter l'expression régulière ${myRegexp}    values=False


L'Element Doit Etre Desactive
    [Documentation]    Vérifie que l'élément est désactivé
    [Arguments]    ${myLocator}

    SeleniumLibrary.Element Should Be Disabled    ${myLocator}


# ==============================================================================
# Verifier l'etat de la page ===================================================
# ==============================================================================
La Page Doit Etre Prete
    [Documentation]     Verifier que la page est chargée.
    ...
    ...                 _Version Selenium._

    ${isReady}=    SeleniumLibrary.Execute Javascript    return document.readyState;
    Should Be Equal As Strings    ${isReady}    complete


Confirmer Que La Page Est Prete
    [Documentation]     Attendre que la page soit chargée.
    ...
    ...                 _Version Selenium._

    Wait Until Keyword Succeeds    20s    1s    selenium.La Page Doit Etre Prete


La Page Ne Contient Pas L'Element
    [Documentation]     Vérifier que la page ne contient pas un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément qui doit être absent.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myMessage}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myMessage}"""!="""None"""
        SeleniumLibrary.Page Should Not Contain Element    ${myLocator}    ${myMessage}
    ELSE
        SeleniumLibrary.Page Should Not Contain Element    ${myLocator}
    END


La Page Contient
    [Documentation]     Vérifier que la page contient un texte.
    ...
    ...                 *Arguments :*
    ...                 - ``myExpectedText``    est le texte attendu sur la page.
    ...                 - ``myTimeout``         est le temps d'attente maximum autorisé pour trouver le texte.
    ...                 _Version Selenium._
    [Arguments]         ${myExpectedText}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Page Contains    ${myExpectedText}    timeout=${myTimeout}    error=${myError}
    ELSE
        SeleniumLibrary.Wait Until Page Contains    ${myExpectedText}    timeout=${myTimeout}
    END


La Page Contient L'Element
    [Documentation]     Verifier que la page contient un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément qui doit être présent.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour que l'élément apparaisse.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    timeout=${myTimeout}    error=${myError}
    ELSE
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    timeout=${myTimeout}
    END


La Page Contient L'Element Visible
    [Documentation]     Verifier que la page contient un élément visible.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément qui doit être présent.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour que l'élément apparaisse.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    ${myError}    Set Variable    ${SPACE}${myError}${SPACE}
    IF    """${myError}"""!="""${SPACE}None${SPACE}"""
        ${myError}    String.Strip String    ${myError}
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    timeout=${myTimeout}    error=${myError}
        SeleniumLibrary.Wait Until Element Is Visible  ${myLocator}    timeout=${myTimeout}    error=${myError}
    ELSE
        SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}    timeout=${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible  ${myLocator}    timeout=${myTimeout}
    END

#-----------------------------------------------------------------------------------------------------------------
# Supprimer le mot clé inutile
#-----------------------------------------------------------------------------------------------------------------
Attendre La Disparition Element
    [Documentation]     Attendre qu'un élément disparaisse de la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément qui doit disparaître.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour que l'élément disparaisse.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=180s

    # attendre une demi-seconde le temps que s'affiche la roue de generaiton
    Sleep    500ms
    SeleniumLibrary.Wait Until Element Is Not Visible     ${myLocator}    ${myTimeout}


L'Element N'Est Plus Visible Sur La Page
    [Documentation]    Vérifie que l'élément n'est plus visible sur la page avant le timeout
    [Arguments]    ${myLocator}    ${timeout}=5s    ${myError}=None

    SeleniumLibrary.Wait Until Element Is Not Visible    ${myLocator}    timeout=${timeout}    error=${myError}
#-----------------------------------------------------------------------------------------------------------------

Verifier Le Titre De La Page
    [Documentation]    Vérifie que le titre de la page correspond bien a la valeur attendue
    [Arguments]        ${myTitle}    ${myMessage}=None

    IF    """${myMessage}"""!="""None"""
        SeleniumLibrary.Title Should Be    ${myTitle}    ${myMessage}
    ELSE
        SeleniumLibrary.Title Should Be    ${myTitle}
    END


L'URL De La Page Doit Avoir La Valeur Attendue
    [Documentation]    Compare l'URL de la page active avec la valeur attendue
    [Arguments]    ${urlAttendu}

    ${urlObtenu}    Selenium.obtenir mon url
    SeleniumLibrary.Location Should Be    ${urlAttendu}    message=L'URL de la page ${urlObtenu} est differente de l'URL attendue ${urlAttendu}


L'URL De La Page Contient
    [Documentation]    Attend l'apparition de l'URI dans l'URL de la page
    [Arguments]        ${myURI}    ${myTimeout}=5s    ${myMessage}=None

    ${myMessage}    Set Variable    ${SPACE}${myMessage}${SPACE}
    IF    """${myMessage}"""!="""${SPACE}None${SPACE}"""
        ${myMessage}    String.Strip String    ${myMessage}
        SeleniumLibrary.Wait Until Location Contains    expected=${myURI}    timeout=${myTimeout}    message=${myMessage}
    ELSE
        SeleniumLibrary.Wait Until Location Contains    expected=${myURI}    timeout=${myTimeout}
    END


# ==============================================================================
# Choisir un element ===========================================================
# ==============================================================================
Deposer Un Fichier Dans Element Actif
    [Documentation]     Déposer un fichier dans un élément (input) en indiquant son chemin d'accès.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``    est la localisation de l'élement (input).
    ...                 - ``myPath2file``       est le chemin vers le fichier.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver l'élément.
    ...                 - ``myError``           est le message d'erreur personnalisable en cas d'échec pour trouver l'élément.
    ...                 _Version Selenium Only._
    [Arguments]         ${myLocator}    ${myPath2file}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    END
    SeleniumLibrary.Choose File    ${myLocator}    ${myPath2file}


Choisir Dans La Liste
    [Documentation]     Faire un choix dans une liste déroulante.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de la liste.
    ...                 - ``myOption``      est l'élément à selectionner dans la liste déroulante.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Version Selenium Only._
    [Arguments]         ${myLocator}    ${myOption}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    log.Debug    Choisir ${myOption}
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    END
    SeleniumLibrary.Select From List By Label       ${myLocator}    ${myOption}


Choisir Dans La Liste Par Valeur
    [Documentation]     Faire un choix dans une liste déroulante.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de la liste.
    ...                 - ``myOption``      est l'élément à selectionner dans la liste déroulante.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Version Selenium Only._
    [Arguments]         ${myLocator}    ${myOption}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    log.Debug    Choisir ${myOption}
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    END
    SeleniumLibrary.Select From List By Value       ${myLocator}    ${myOption}


Choisir Bouton Radio
    [Documentation]     Sélectionner un bouton radio.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'ensemble des boutons radios existants.
    ...                 - ``myChoice``      est le bouton radio à sélectionner.
    ...                 _Version Selenium Only._
    [Arguments]         ${myLocator}    ${myChoice}

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}
    SeleniumLibrary.Select Radio Button    ${myLocator}    ${mychoice}


Le Bouton Radio Doit Etre Selectionne
    [Documentation]     S'assurer que le bouton radio est sélectionné'.
    ...
    ...                 *Arguments :*
    ...                 - ``myName``     est le nom du groupe de choix possible (name).
    ...                 - ``myValue``      est le bouton radio qui doit être sélectionné.
    ...                 _Version Selenium Only._
    [Arguments]         ${myName}    ${myValue}

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${myName}
    SeleniumLibrary.Radio Button Should Be Set To    ${myName}    ${myValue}


Le Bouton Radio Ne Doit Pas Etre Selectionne
    [Documentation]     S'assurer que le bouton radio n'est pas sélectionné'.
    ...
    ...                 *Arguments :*
    ...                 - ``myName``     est le nom du groupe de choix possible (name).
    ...                 _Version Selenium Only._
    [Arguments]         ${myName}

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${myName}
    SeleniumLibrary.Radio Button Should Not Be Selected    ${myName}


Cocher Un Element
    [Documentation]     Cocher un élément sur une page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la checkbox à cocher.
    ...                 _Version Selenium Only._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    END
    SeleniumLibrary.Select Checkbox    ${myLocator}


Decocher Un Element
    [Documentation]     Décocher un élément sur une page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la checkbox à décocher.
    ...                 _Version Selenium Only._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    END
    SeleniumLibrary.Unselect Checkbox    ${myLocator}


L'Element Doit Etre Coche
    [Documentation]     S'assurer que l'élément (checkbox) est coché.
    [Arguments]         ${myLocator}

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}
    SeleniumLibrary.Checkbox Should Be Selected    ${myLocator}


L'Element Doit Etre Decoche
    [Documentation]     S'assurer que l'élément (checkbox) est décoché.
    [Arguments]         ${myLocator}

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${myLocator}
    SeleniumLibrary.Checkbox Should Not Be Selected    ${myLocator}


# ==============================================================================
# Saisir du texte ==============================================================
# ==============================================================================
Saisir Un Secret Dans Element Actif
    [Documentation]     Saisir du texte non enregistre dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myText}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    selenium.Saisir Un Secret Avec Webdriver    ${myLocator}      ${myText}


Saisir Dans Element Actif Et Sortir Du Champ
    [Documentation]     Saisir du texte dans un élément puis quitter le champ.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}     ${myText}

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Element Is Enabled   ${myLocator}
    log.Debug       Saisir le texte '${myText}'
    selenium.Saisir Avec Webdriver                  ${myLocator}    ${myText}
    # Tabulation pour sortir du champ
    selenium.Appuyer Sur Une Touche      ${myLocator}    u'\ue004'


Appuyer Sur Une Touche
    [Documentation]     Appuyer sur une touche du clavier.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel la touche sera appuyée.
    ...                 - ``myAsciiCode``   est le code ASCII de la touche sur laquelle on veut appuyer.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myAsciiCode}

    SeleniumLibrary.Press Keys    ${myLocator}    ${myAsciiCode}


Saisir Dans Element Actif
    [Documentation]     Saisir du texte dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myText}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    log.Debug    Saisir le texte '${myText}'
    selenium.Saisir Avec Webdriver    ${myLocator}   ${myText}


Saisir Dans Element Actif Avec Javascript
    [Documentation]     Saisir du texte dans un élément via Javascript.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myText}    ${myAttribute}=value    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    log.Debug    Saisir le texte '${myText}'
    selenium.Saisir Avec Javascript    ${myLocator}    ${myText}    ${myAttribute}


Saisir Dans Element Actif Sans Effacer L'Existant
    [Documentation]     Saisir du texte dans un élément à la suite de ce qui est déjà présent.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myText}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    log.Debug    Saisir le texte '${myText}'
    selenium.Saisir Avec Webdriver    ${myLocator}   ${myText}    False


Saisir Dans Element Avec Verification De La Saisie
    [Documentation]     Saisir du texte dans un élément et s'assurer que la saise est effective.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}

    web.Saisir Dans Element Actif    ${myLocator}    ${myText}
    ${texteObtenu}=    web.Obtenir Le Texte De L'Attribut    ${myLocator}    value
    Should Be Equal    ${myText}    ${texteObtenu}    msg=None    values=True    ignore_case=True


Saisir Avec Webdriver
    [Documentation]     Saisir du texte dans un élément directement depuis le webdriver.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myText}    ${myClear}=True

    SeleniumLibrary.Input Text    ${myLocator}    ${myText}    ${myClear}


Saisir Un Secret Avec Webdriver
    [Documentation]     Saisir du texte non enregistré dans un élément directement depuis le webdriver.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myText}

    SeleniumLibrary.Input Password    ${myLocator}    ${myText}


Saisir Avec Javascript
    [Documentation]     Saisir du texte dans un élément en utilisant un script.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myText}    ${myAttribute}=value

    Execute Javascript    document.evaluate("${myLocator}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.setAttribute('${myAttribute}', '${myText}');


Effacer Dans Element Actif
    [Documentation]     Effacer du texte dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte effacé.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    SeleniumLibrary.Clear Element Text    ${myLocator}


# ==============================================================================
# Cliquer ======================================================================
# ==============================================================================
Cliquer Sur Element Visible
    [Documentation]     Cliquer sur un élément visible sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel cliquer.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}     ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    selenium.Cliquer Sur Element Avec Webdriver    ${myLocator}


Cliquer Sur Element Actif
    [Documentation]     Cliquer sur un élément actif sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel cliquer.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    END
    selenium.Cliquer Sur Element Avec Webdriver    ${myLocator}


Cliquer Sur Element Visible Et Actif
    [Documentation]     Cliquer sur un élément visible et actif sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``    est l'élément sur lequel on veut cliquer.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver l'élément.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=5s   ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    selenium.Cliquer Sur Element Avec Webdriver    ${myLocator}


Cliquer Sur Element Visible Et Actif Avec Javascript
    [Documentation]     Cliquer sur un élément visible et actif sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``    est l'élément sur lequel on veut cliquer.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver l'élément.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}    ${myTimeout}=5s   ${myError}=None

    selenium.Confirmer Que La Page Est Prete
    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}    ${myError}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    selenium.Cliquer Sur Element Avec Javascript    ${myLocator}


Cliquer Sur Element Visible Et Actif Apres La Disparition D'Un Autre Element Qui Le Cache
    [Documentation]    Clique sur l'élément après la disparition d'un autre qui le cache
    [Arguments]    ${myLocatorClick}    ${myLocatorElementQuiCache}    ${myTimeout}=5s

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Element Is Not Visible      ${myLocatorElementQuiCache}
    SeleniumLibrary.Wait Until Element Is Enabled    ${myLocatorClick}    ${myTimeout}
    SeleniumLibrary.Wait Until Element Is Visible    ${myLocatorClick}    ${myTimeout}
    SeleniumLibrary.Click Element    ${myLocatorClick}


Cliquer Sur Element Visible Et Actif Le Plus A Gauche Possible
    [Documentation]    Clique sur la zone la plus à gauche possible de l'élément
    [Arguments]    ${myLocator}    ${myTimeout}=5s

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    ${width}	${height}=	SeleniumLibrary.Get Element Size	${myLocator}
    ${var}=		Evaluate	-(${width}/2)+1
    SeleniumLibrary.Click Element At Coordinates    ${myLocator}    ${var}    0


Cliquer Sur Element Visible Et Actif En Maintenant Des Touches
    [Documentation]    Clique sur l'élément en maintenant une ou plusieurs touches du clavier
    [Arguments]    ${myLocator}    ${myKeys}   ${myTimeout}=5s

    selenium.Confirmer Que La Page Est Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    selenium.Cliquer Sur Element Avec Webdriver    ${myLocator}    ${myKeys}


Cliquer Sur Element Avec Webdriver
    [Documentation]     Cliquer sur un élément avec le webdriver sans vérification préalable.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel on veut cliquer.
    ...                 Version Selenium.
    [Arguments]         ${myLocator}    ${myModifier}=False

    SeleniumLibrary.Click Element    ${myLocator}    ${myModifier}


Cliquer Sur Element Avec Javascript
    [Documentation]     Cliquer sur un élément en utilisant javascript.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel on veut cliquer.
    ...                 _Version Selenium._
    [Arguments]         ${myLocator}

    SeleniumLibrary.Execute Javascript    document.evaluate("${myLocator}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();


Accepter La Popup
    [Documentation]    Accepte la Pop-up qui apparaît à l'écran et retourne le message qu'elle affiche
    [Arguments]    ${myTimeout}=5s

    ${myMessage}=    SeleniumLibrary.Handle Alert    ACCEPT    ${myTimeout}

    [Return]    ${myMessage}


Verifier Le Message De La Popup Et L'Accepter
    [Documentation]    Vérifie le message de la Pop-up qui apparaît à l'écran et l'accepter
    ...
    ...                 *Arguments :*
    ...                 - ``myMessage``     est le message que l'on veut vérifier.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Version Selenium._
    [Arguments]         ${myMessage}    ${myTimeout}=5s

    SeleniumLibrary.Alert Should Be Present    ${myMessage}    ACCEPT    ${myTimeout}


# ==============================================================================
# Generalites ==================================================================
# ==============================================================================
Capturer L'Ecran
    [Documentation]     Faire une capture d'écran de la page actuelle.
    ...
    ...                 _Version Selenium._

    SeleniumLibrary.Capture Page Screenshot    selenium-screenshot-{index:03}.png


Capture L'Ecran Avec Timestamp Pour Nom De Fichier
    [Documentation]   Réalise une capture d'écran avec un timestamp pour nom de fichier
    ${date}=    Get Current Date    result_format=%d-%m-%Y %Hh%Mm%Ss   exclude_millis=True
    ${title}=  get title
    ${page_title}=  remove string  ${title}  ,  ?  .  ;  /  :  §  !  %  *  $  \
    SeleniumLibrary.Capture Page Screenshot   ${date}_${page_title}.png


Capture L'Ecran Avec Timestamp Pour Page Identifier De Fichier
    [Documentation]   Réalise une capture d'écran avec un timestamp pour nom de fichier
    ${date}=    Get Current Date    result_format=%d-%m-%Y %Hh%Mm%Ss   exclude_millis=True
    ${page_title}=  get title
    ${page_identifier}=  get text  //*[@id="pageIdentifier"]/samp
    SeleniumLibrary.Capture Page Screenshot   ${date}_${page_identifier}_${page_title}.png


Fermer Le Navigateur
    [Documentation]     Fermer le navigateur actif.
    ...
    ...                 _Version Selenium._

    SeleniumLibrary.Close Browser


Fermer Tous Les Navigateurs
    [Documentation]     Fermer tous les navigateurs ouverts.
    ...
    ...                 _Version Selenium._

    SeleniumLibrary.Close All Browsers


Actualiser La Page
    [Documentation]    Actualise la page active du navigateur

    SeleniumLibrary.Reload Page


Fermer L'Onglet Du Navigateur
    [Documentation]    Ferme l'onglet actif du navigateur

    SeleniumLibrary.Close Window


Changer De Navigateur
    [Documentation]     Changer de navigateur.
    ...
    ...                 *Arguments :*
    ...                 - ``myIndex``   est le nouveau navigateur à ouvrir.
    ...                 _Version Selenium Only._
    [Arguments]         ${myIndex}

    SeleniumLibrary.Switch Browser    ${myIndex}


Positionner Le Curseur Sur Un Element
    [Documentation]    Positionne le curseur de la souris au dessus d'un element
    [Arguments]        ${myLocator}    ${myTimeout}=5s   ${myError}=None

    IF    """${myError}"""!="""None"""
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    ELSE
        SeleniumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}
    END
    SeleniumLibrary.Mouse Over    ${myLocator}


Scroller Jusqu'A L'Apparition De L'Element
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit affiché à l'écran
    [Arguments]        ${myLocator}

    SeleniumLibrary.Scroll Element Into View    ${myLocator}


Scroller Jusqu'A L'Apparition De L'Element Entier
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit entièrement affiché à l'écran
    [Arguments]        ${myLocator}

    SeleniumLibrary.Scroll Element Into View    ${myLocator}
    ${largeur}    ${hauteur}    SeleniumLibrary.Get Element Size    ${myLocator}
    Execute Javascript    window.scrollBy(0, ${hauteur});


Scroller Jusqu'A L'Apparition De L'Element En Dessous Du Header
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit affiché à l'écran et scroll à nouveau de la hauteur du header
    ...                Pertinant lorsque le header est situé au premier plan et cache potentiellement l'élément à cliquer
    [Arguments]        ${myLocator}    ${myHeaderLocator}

    # Scroll jusqu'à l'élément
    SeleniumLibrary.Scroll Element Into View    ${myLocator}
    # Scroll de la taille de l'élément pour qu'il soit complètement visible
    ${largeur}    ${hauteur_element}    SeleniumLibrary.Get Element Size    ${myLocator}
    # Scroll de la taille du header
    ${largeur}    ${hauteur_header}    SeleniumLibrary.Get Element Size    ${myHeaderLocator}
    ${hauteur}    Evaluate    ${hauteur_element}+${hauteur_header}
    Execute Javascript    window.scrollBy(0, -${hauteur});


Scroller Jusqu'A L'Apparition De L'Element Au Dessus Du Footer
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit affiché à l'écran et scroll à nouveau de la hauteur du footer
    ...                Pertinant lorsque le footer est situé au premier plan et cache potentiellement l'élément à cliquer
    ...                Il est possible d'ajouter plusieurs locators si il y a plusieurs éléments qui composent le footer
    [Arguments]        ${myLocator}    @{myFooterLocators}

    # Scroll jusqu'à l'élément
    SeleniumLibrary.Scroll Element Into View    ${myLocator}

    # Détermination de la taille de l'élément pour qu'il soit complètement visible
    ${largeur}    ${hauteur_element}    SeleniumLibrary.Get Element Size    ${myLocator}

    # Détermination de la taille du footer complet
    ${hauteur_totale}=    Set Variable    ${hauteur_element}
    FOR    ${element}    IN    @{myFooterLocators}
        ${largeur}    ${hauteur_element}    SeleniumLibrary.Get Element Size    ${element}
        ${hauteur_totale}=    Evaluate    ${hauteur_totale}+${hauteur_element}
    END

    # Scroll de la taille du footer + l'élément
    Execute Javascript    window.scrollBy(0, ${hauteur_totale});


Scroller Jusqu'Au Bas De La Page
    [Documentation]    Scroll sur la page jusqu'au bas de la page

    Execute Javascript    window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' });


Selectionner La Frame
    [Documentation]    Selectionne la frame ou l'iframe
    [Arguments]    ${myLocator}

    SeleniumLibrary.Select Frame    ${myLocator}


Deselectionner La Frame
    [Documentation]    Deselectionne la frame ou l'iframe

    SeleniumLibrary.Unselect Frame


Selectionner La Nouvelle Fenetre
    [Documentation]    Selectionne la derniere fenetre ouverte

    SeleniumLibrary.Select Window    New


Mettre le robot en pause
    [Documentation]  Met le robot en pause
    [Arguments]     ${message}
    Execute Manual Step  ${message}