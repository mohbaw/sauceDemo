*** Settings ***
Documentation
...                 Cette ressource met à disposition les mots-clé pour réaliser facilement des appels SOAP

Library    Zoomba.SOAPLibrary
Library    SudsLibrary

Library     Collections
Library     OperatingSystem
Library     String

*** Variables ***
${ENDPOINT_URL} =       https://cwse:4005/PO/PDT/ConsultDematClient/V1/S
${WSDL_URL} =           https://cwse:4005/PO/PDT/ConsultDematClient/V1?wsdl
${WSDL_HOST} =          https://cwse:4005
${ENDPOINT} =           /PO/PDT/ConsultDematClient/V1/
${FILE} =               C:\\robotconfig\\contentSOAP.txt
${EXEMPLE} =            C:\\robotconfig\\response_exemple.txt

*** Keywords ***

Appeler une méthode SOAP avec une liste
    Call Soap Method With List Object  ${var_action}  ${var_soap_object}

Appeler une méthode SOAP avec un objet
    Call Soap Method With Object

Convertir la réponse SOAP en JSON
    Convert Soap Response To Json

Créer une session SOAP
    Create Soap Session

Créer une session SOAP et fixer le WSDL
    Create Soap Session And Fix Wsdl

Créer une session SOAP et déterminer l'emplacement
    Create Soap Session And Set Location

Créer des objets WSDL
    Create Wsdl Objects

*** Test Cases ***

SOAP
    Create Soap Client  ${WSDL_URL}
    Attach My Plugin
    Set GetStats Type    specialType
    ${dbl array}=    Create Wsdl Object    ArrayOfDouble
    Append To List    ${dbl array.double}    2.0
    Append To List    ${dbl array.double}    3.0
    ${result}=    Call Soap Method    GetStatistics    ${dbl array}
    Should Be Equal As Numbers    ${result.Average}    2.5

Check App Auth Ticket
    Create Soap Client    ${WSDL_URL}
    ${WSDLobj}    Create Wsdl Object    AuthUser
    Set Wsdl Object Attribute    ${WSDLobj}    userName    MyAdmin
    Set Wsdl Object Attribute    ${WSDLobj}    password    Password123
    ${result}    Call Soap Method    AuthUser    ${WSDLobj}
    log    Result: ${result}
    # Now get the specific element from the response
    ${AuthVal}    Get Wsdl Object Attribute    ${result}    AuthTicket
    log    Auth Ticket: ${AuthVal}

Test
    Create Soap Client    ${WSDL_URL}
    ${double array}=    Create Wsdl Object    ArrayOfDouble
    Append To List    ${double array.double}    ${5.2}
    Append To List    ${double array.double}    ${1.6}
    ${resp}=    Call Soap Method Using Kwargs    GetStatistics    X=${double array}