﻿*** Settings ***
Library           Collections
Library           FakerLibrary    locale=fr_FR
Resource          sql.robot
Resource          csv.robot
Resource          Log.robot

*** Variables ***

*** Keywords ***
Obtenir le statut de la requete
    [Documentation]
    [Arguments]    ${myRequestId}     ${myExtractSqlForGivenKeyDictionary}     ${myTargetDictionnary}     ${myEnv}=%{MY_PFV}

    # Construire la requete SQL pour obtenir le statut de la request
    ${mySqlWith}=      Set Variable    ${empty}
    ${mySqlSelect}=    Catenate    SEPARATOR=\n    SELECT    request.STATUS, \ \ request.REQUEST_TYPE, \ \ request.SUBMISSION_CHANNEL
    ${mySqlFrom}=      Catenate    SEPARATOR=\n    FROM      INS_REQ_BUSINESS_REQUEST request
    ${mySqlWhere}=     Catenate    SEPARATOR=\n    WHERE     request.REQUEST_ID = '${myRequestId}'
    ${mySqlFinal}=     Set Variable    ${empty}
    ${mySqlQuery}=     Catenate    SEPARATOR=\n    ${mySqlWith}    ${mySqlSelect}    ${mySqlFrom}    ${mySqlWhere}    ${mySqlFinal}
    ${request}    ${nbRow}=    sql.Lancer La Requete    ${mySqlQuery}    dbINS_${myEnv}

    sql.Extraire Les Donnees Du Sql Dans Le Dictionnaire   ${request}   ${myExtractSqlForGivenKeyDictionary}   ${myTargetDictionnary}

    # Renseigner le dictionnaire JDD
    #Collections.Set To Dictionary    ${myTargetDictionnary}    requestStatus=${request[0][0]}    requestType=${request[0][1]}    requestSubmissionChannel=${request[0][2]}