*** Settings ***
Documentation       Ressource wrapper de AppiumLibrary.
...
...                 Cette librairie reprend les fonctionnalités présentes dans la librairie
...                 d'origine et propose des mots-clés en français.
...
...                 Elle est employée depuis une redirection de ``web.robot``.
...
...                 = Cas d'utilisation =
...                 Cette librairie est utilisée pour automatiser des tests sur des
...                 navigateurs web sur des tablettes, des smartphones ou autre.
...                 Pour automatiser des tests sur des navigateurs web sur ordinateur,
...                 veuillez vous référer à la librairie [http://robotframework.org/SeleniumLibrary/SeleniumLibrary.html|SeleniumLibrary],
...                 voir ``selenium.robot`` pour la version française du socle.
...
...                 = Localiser ou spécifier des éléments =
...                 Il existe deux types d'arguments permettant la localisation d'éléments
...                 dans la librairie Appium. Ces arguments peuvent être un ``locator`` ou un
...                 ``element``. Un ``locator`` est une chaîne de caractères qui décrit
...                 de quelle manière un élément peut être localisé grâce à différentes
...                 stratégies de localisation. Un ``element`` est une variable qui se
...                 trouve dans un élément Web, il s'agit d'une représentation de cet élément.
...
...                 == Utiliser des localisateurs ==
...                 Par défaut, quand un localisateur est employé, il est mis en relation
...                 avec les attributs de la clé du type de l'élément particulier.
...
...                 Exemple, dans le cas d'une recherche par Xpath :
...                 | appium.Cliquer Sur Element Actif \ \ \ \ \ //*[@type="android.widget.EditText"]
...                 Pour indiquer que l'on veut exécuter une requête Xpath, il faut précéder la
...                 requête de "//".
...
...                 == Utiliser des éléments ==
...                 Il est possible de sélectionner des éléments directement avec
...                 la fonctionnalité ``AppiumLibrary.Get Webelement`` de la AppiumLibrary.
...
...                 Par exemple :
...                 |   @{elements} \ \ \ \ AppiumLibrary.Get Webelement \ \ \ \ \ \ \ class=UIAButton
...                 |   appium.Cliquer Sur Element Actif \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ @{elements}[2]
...                 Explications : on récupère tous les éléments dont la classe est
...                 ``UIAButton`` avec la méthode ``AppiumLibrary.Get Webelement``. Puis, on clique sur
...                 le troisième élément de la liste d'éléments obtenus.
...                 Il s'agit du troisième élément car le comptage commence avec 0.

Library             AppiumLibrary
Library             OperatingSystem

Resource            appium.iOS.robot
Resource            appium.android.robot
Resource            log.robot
Resource            utils.robot


*** Variables ***
#${WEB_PATH2BMP}          %{ROBOT_HOME}${/}app${/}Portable-Python-Robot-2711.300.1.3${/}App${/}Lib${/}site-packages${/}browsermobproxy
# ${WEB_PATH2WEBDRIVER}    ${CURDIR}${/}..${/}resources${/}WebDriver
# ${WEB_PATH2CHROME}       %{ROBOT_DEPLOY}${/}tnr_socle_automatisation${/}utils${/}GoogleChromePortable${/}Chrome${/}Application${/}chrome.exe

${APPIUM_ENDPOINT}       europe=https://eu1.appium.testobject.com/wd/hub
...                      us=https://us1.appium.testobject.com/wd/hub
...                      apikey=1c71e04c-e57c-4f99-b477-8fdcbdc030b5


*** Keywords ***
Sur Mon Device
    [Documentation]     Aiguillage en fonction de la variable Jenkins MY_DEVICE.
    ...
    ...                 Certains mots-clés doivent être différents en fonction de Android ou iOS.
    ...
    ...                 *Arguments :*
    ...                 - ``myKeyword``     est le mot clé à appeler dans la librairie aiguillée.
    ...                 - ``myVarargs``     sont les arguments à ajouter lors de l'appel au mot-clé.
    ...                 _Appium Only._
    [Arguments]         ${myKeyword}    @{myVarargs}

    ${plateforme}=    appium.Obtenir La Plateforme Du Device
    Run Keyword    appium.${plateforme}.${myKeyword}    @{myVarargs}


Obtenir L'Uri Vers Appium Server
    [Documentation]     Obtenir une URI vers le serveur Appium.
    ...
    ...                 *Return* : l'URI vers le serveur Appium.
    ...
    ...                 _Appium Only._

    ${uri2appium}=      Get Environment Variable    URI2APPIUM    default=${APPIUM_ENDPOINT}

    [Return]            ${uri2appium}


Obtenir La Plateforme Du Device
    [Documentation]     Obtenir la plateforme de l'appareil utilisé.
    ...
    ...                 *Return* : la plateforme de l'appareil.
    ...
    ...                 _Appium Only._

    [Return]            ${CNP_DEVICES.%{MY_DEVICE}platformName}


Ouvrir L'Application Sur Le Device
    [Documentation]     Ouvrir une application sur l'appareil.
    ...
    ...                 *Arguments :*
    ...                 - ``myApp``         est l'application à ouvrir.
    ...                 - ``myDevice``      est l'appareil utilisé.
    ...
    ...                 _Appium Only._
    [Arguments]         ${myApp}

    appium.Sur Mon Device    Ouvrir L'Application    ${myApp}


Deployer Et Ouvrir L'Application Sur Le Device
    [Documentation]     Deployer puis ouvrir une application sur l'appareil.
    ...
    ...                 *Arguments :*
    ...                 - ``myApp``         est l'application à deployer.
    ...                 - ``myDevice``      est l'appareil utilisé.
    ...
    ...                 _Appium Only._
    [Arguments]         ${myApp}    ${myDevice}

    appium.Sur Mon Device      Deployer Et Ouvrir L'Application    ${myApp}    ${myDevice}


Taper Sur L'Element
    [Documentation]     Taper sur un element de l'application.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel taper.
    ...
    ...                 _Appium Only._
    [Arguments]         ${myLocator}

    AppiumLibrary.Wait Until Element Is Visible    ${myLocator}
    ATap    ${myLocator}


# ==============================================================================
# Ouvrir navigateur ============================================================
# ==============================================================================
Ouvrir Chrome
    [Documentation]     Ouvrir Google Chrome sur le device.
    ...
    ...                 _Version Selenium._

    appium.Sur Mon Device    Ouvrir Le Navigateur    Chrome


# ==============================================================================
# Se deplacer ==================================================================
# ==============================================================================
Aller Vers Le Site
    [Documentation]     Charger l'URL.
    ...
    ...                 *Arguments :*
    ...                 - ``myUri``             est l'URI vers laquelle se diriger.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour aller vers le site.
    ...                 _Version Selenium._
    [Arguments]         ${myUri}    ${myExpectedText}    ${myTimeout}=10s

    # Aiguillage vers le site de l'environnement de test choisi dans Jenkins - variable MY_PFV
    ${myUrl}=    web.obtenir url a partir uri    ${myUri}
    log.Debug    Je vais vers le site ${myUrl}
    AppiumLibrary.Go To Url    ${myUrl}
    # On verifie la presence du titre de la page attendue en fonction de l'environnement
    AppiumLibrary.Wait Until Page Contains    ${myExpectedText}    ${myTimeout}
    log.Debug    La page contient le texte '${myExpectedText}'


Naviguer Avec Le Browser Vers Le Site
    [Documentation]     Ouvrir le navigateur myBrowser ou MY_BROWSER par défaut, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``             est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``    est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myBrowser``         est le navigateur à utiliser.
    ...                 _Version Selenium._
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myBrowser}=%{MY_BROWSER}

    ${keywordWithBrowser}=    Set Variable    appium.Ouvrir ${myBrowser}
    Run Keyword    ${keywordWithBrowser}
    appium.Aller Vers Le Site    ${myUrl}    ${myExpectedText}


# ==============================================================================
# Recuperer des donnees ========================================================
# ==============================================================================
Obtenir Mon Url
    [Documentation]     _Appium Version No Operation._

    No Operation


Obtenir Mon Titre
    [Documentation]     _Appium Version No Operation._

    No Operation


Obtenir La Cellule Du Tableau
    [Documentation]     _Appium Version No Operation._

    No Operation


Obtenir Le Texte De L'Attribut
    [Documentation]     Obtenir le texte de l'attribut.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de l'attribut.
    ...                 *Return* : la valeur de la cellule.
    ...
    ...                 _Appium Version._
    [Arguments]         ${myLocator}

    appium.Confirmer Que La Page Est Prete
    ${cellValue}=       AppiumLibrary.Get Element Attribute    ${myLocator}
    [Return]            ${cellValue}


Obtenir Le Texte Complet De L'Element
    [Documentation]     _Appium Version._
    [Arguments]         ${myLocator}

    appium.Confirmer Que La Page Est Prete
    appium.La Page Contient L'Element    ${myLocator}
    ${cellValue}=    AppiumLibrary.Execute Script    return document.evaluate("${myLocator}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerText;
  	${text}=         Set Variable    ${cellValue.replace("\n", " ")}
	${stripText}=    utils.Eliminer Les Espaces En Double    ${text}
    [Return]         ${stripText}


Obtenir Le Texte De L'Element
    [Documentation]     Obtenir le texte de l'élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de l'élément.
    ...                 *Return* : la valeur de la cellule.
    ...
    ...                 _Appium Version._
    [Arguments]         ${myLocator}

    appium.Confirmer Que La Page Est Prete
    appium.La Page Contient L'Element    ${myLocator}
    ${cellValue}=   AppiumLibrary.Get Text    ${myLocator}
    [Return]        ${cellValue}


Obtenir L'Element Le Plus Proche De La Liste
    [Documentation]     Obtenir l'élément le plus ressemblant de la liste.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``         est la localisation de la liste.
    ...                 - ``myExpectedOption``  est le choix attendu.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver le choix attendu.
    ...                 *Return* : l'élément ressemblant le plus.
    ...
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myExpectedOption}    ${myTimeout}=5s

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Element Is Enabled     ${myLocator}    ${myTimeout}
    AppiumLibrary.Click Element                     ${myLocator}
    ${options}=                 Wait Until Keyword Succeeds    3x    200ms    Get List Items    ${myLocator}
    ${maxProximityOption}=      utils.Obtenir L'Element Le Plus Proche    ${myExpectedOption}    ${options}

    [Return]    ${maxProximityOption}


# ==============================================================================
# Verifier l'etat de la page ===================================================
# ==============================================================================
La Page Doit Etre Prete
    [Documentation]     Vérifier que la page est chargée.
    ...
    ...                 _Appium Version._
    ${isReady}=    AppiumLibrary.Execute Script    return document.readyState;
    Should Be Equal As Strings    ${isReady}    complete


Confirmer Que La Page Est Prete
    [Documentation]     Attendre que la page soit chargée.
    ...
    ...                 _Appium Version._

    # Debug Purpose: appium.capturer l'ecran
    Wait Until Keyword Succeeds    20s    1s    appium.la page doit etre prete


La Page Ne Contient Pas L'Element
    [Documentation]     Vérifier que la page ne contient pas un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément qui doit être absent.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Page Should Not Contain Element    ${myLocator}


La Page Contient
    [Documentation]     Vérifier que la page contient un texte.
    ...
    ...                 *Arguments :*
    ...                 - ``myExpectedText``    est le texte attendu sur la page.
    ...                 - ``myTimeout``         est le temps d'attente maximum autorisé pour trouver le texte.
    ...                 _Appium Version._
    [Arguments]         ${myExpectedText}    ${myTimeout}=5s

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Page Contains    ${myExpectedText}    timeout=${myTimeout}


La Page Contient L'Element
    [Documentation]     Vérifier que la page contient un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément qui doit être présent.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour que l'élément apparaisse.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myTimeout}=5s    ${myError}=None

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Page Contains Element    ${myLocator}    timeout=${myTimeout}    error=${myError}


Attendre La Disparition Element
    [Documentation]     Attendre qu'un element disparaisse de la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'element qui doit disparaitre.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour que l'element disparaisse.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myTimeout}=180s

    # attendre une demi-seconde le temps que s'affiche la roue de generation
    Sleep    500ms
    AppiumLibrary.Wait Until Element Is Not Visible     ${myLocator}    ${myTimeout}


# ==============================================================================
# Choisir un element ===========================================================
# ==============================================================================
# TODO
Choisir Le Fichier
    [Documentation]     Choisir un fichier en indiquant son chemin d'accès.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``    ?
    ...                 - ``myPath2file``       est le chemin vers le fichier.
    ...                 _Appium Version No Operation._
    [Arguments]         ${myXpathLocator}    ${myPath2file}    ${myTimeout}=5s

    No Operation

# TODO
Choisir Dans La Liste
    [Documentation]     Faire un choix dans une liste déroulante.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de la liste.
    ...                 - ``myOption``      est l'élément à selectionner dans la liste déroulante.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Appium Version No Operation._
    [Arguments]         ${myLocator}    ${myOption}    ${myTimeout}=5s

    No Operation

# TODO
Choisir Dans La Liste Unique
    [Documentation]     Faire un choix unique dans une liste déroulante.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la localisation de la liste.
    ...                 _Appium Version No Operation._
    [Arguments]         ${myLocator}

    No Operation

# TODO
Choisir Bouton Radio
    [Documentation]     Sélectionner un bouton radio.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'ensemble des boutons radios existants.
    ...                 - ``myChoice``      est le bouton radio à sélectionner.
    ...                 _Appium Version No Operation._
    [Arguments]         ${myLocator}    ${myChoice}

    No Operation

# TODO
Cocher Un Element
    [Documentation]     Cocher un élément sur une page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est la checkbox à cocher.
    ...                 _Appium Version No Operation._
    [Arguments]         ${myLocator}

    No Operation


# ==============================================================================
# Saisir du texte ==============================================================
# ==============================================================================
Saisir Un Secret Dans Element Actif
    [Documentation]     Saisir du texte non enregistré dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myText}

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Element Is Enabled     ${myLocator}
    AppiumLibrary.Wait Until Element Is Visible     ${myLocator}
    log.Debug       Saisir le texte '${myText}'
    appium.Saisir Un Secret Avec Webdriver          ${myLocator}      ${myText}


Saisir Dans Element Actif Et Sortir Du Champ
    [Documentation]     Saisir du texte dans un élément puis quitter le champ.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}     ${myText}

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Element Is Enabled   ${myLocator}
    log.Debug       Saisir le texte '${myText}'
    appium.Saisir Avec Webdriver                  ${myLocator}    ${myText}
    # Tabulation pour sortir du champ
    appium.Appuyer Sur Une Touche      61

# TODO un dictionnaire pour convertir code ascii en keycode appium
Appuyer Sur Une Touche
    [Documentation]     Appuyer sur une touche du clavier.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel la touche sera appuyée.
    ...                 - ``myKeycode``     est le code Android de la touche sur laquelle on veut appuyer.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myKeycode}

    AppiumLibrary.Press Keycode         ${myKeycode}


Saisir Dans Element Actif
    [Documentation]     Saisir du texte dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myText}

    log.Debug    Saisir le texte '${myText}'
    AppiumLibrary.Wait Until Element Is Visible     ${myLocator}
    appium.Saisir Avec Webdriver    ${myLocator}    ${myText}


Saisir Avec Webdriver
    [Documentation]     Saisir du texte dans un élément directement depuis le webdriver.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myText}

    AppiumLibrary.Input Text    ${myLocator}    ${myText}


Saisir Un Secret Avec Webdriver
    [Documentation]     Saisir du texte non enregistré dans un élément directement depuis le webdriver.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myText}

    AppiumLibrary.Input Password    ${myLocator}    ${myText}


Saisir Avec Script
    [Documentation]     Saisir du texte dans un élément en utilisant un script.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``        est le texte à saisir.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myText}

    AppiumLibrary.Execute Script    document.evaluate("${myLocator}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.setAttribute('value', '${myText}');


# ==============================================================================
# Cliquer ======================================================================
# ==============================================================================
Cliquer Sur Element Visible
    [Documentation]     Cliquer sur un élément visible sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel cliquer.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}     ${myTimeout}=5s

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Element Is Visible    ${myLocator}   ${myTimeout}
    appium.Confirmer Que La Page Est Prete
    appium.Cliquer Sur Element Avec Webdriver    ${myLocator}


Cliquer Sur Element actif
    [Documentation]     Cliquer sur un élément actif sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel cliquer.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myTimeout}=5s

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Element Is Enabled    ${myLocator}    ${myTimeout}
    appium.Cliquer Sur Element Avec Webdriver    ${myLocator}


Cliquer Sur Element Visible Et Actif
    [Documentation]     Cliquer sur un élément visible et actif sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``    est l'élément sur lequel on veut cliquer.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver l'élément.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}    ${myTimeout}=5s   ${myError}=None

    appium.Confirmer Que La Page Est Prete
    AppiumLibrary.Wait Until Element Is Visible    ${myLocator}    ${myTimeout}    ${myError}
    appium.Cliquer Sur Element Avec Javascript    ${myLocator}


Cliquer Sur Element Avec Webdriver
    [Documentation]     Cliquer sur un élément avec le webdriver sans vérification préalable.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel on veut cliquer.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}

    AppiumLibrary.Click Element           ${myLocator}


Cliquer Sur Element Avec Javascript
    [Documentation]     Cliquer sur un élément en utilisant javascript.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``     est l'élément sur lequel on veut cliquer.
    ...                 _Appium Version._
    [Arguments]         ${myLocator}

    AppiumLibrary.Execute Script      document.evaluate("${myLocator}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();


# ==============================================================================
# Generalites ==================================================================
# ==============================================================================
Capturer L'Ecran
    [Documentation]     Faire une capture d'écran de la page actuelle.
    ...
    ...                 _Appium Version._

    AppiumLibrary.Capture Page Screenshot


Fermer Tous Les Navigateurs
    [Documentation]     Fermer tous les navigateurs ouverts.
    ...
    ...                 _Appium Version._

    AppiumLibrary.Close All Applications
    # supprimer le profil firefox si necessaire
  	${passed}    ${length}=      Run Keyword And Ignore Error    Get Length    ${WEB_PROXY_AUTH.path2profile}
  	${isSet}=                    Set Variable If    '${passed}'=='FAIL'    0    ${length}
    Run Keyword If    ${isSet} > 0    Remove Directory    ${WEB_PROXY_AUTH.path2profile}    recursive=${TRUE}

# TODO
Changer De Navigateur
    [Documentation]     Changer de navigateur.
    ...
    ...                 *Arguments :*
    ...                 - ``myIndex``   est le nouveau navigateur à ouvrir.
    ...                 _Appium Version No Operation._
    [Arguments]         ${myIndex}

    No Operation
