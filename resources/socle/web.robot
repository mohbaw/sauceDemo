*** Settings ***
Documentation       Ressource de référence pour Appium et Selenium.
...
...                 Cette libraire propose une forme d'héritage vers Appium et Selenium.
...                 Les mots-clés conçus sont en communs avec ceux de ``appium.robot``
...                 et de ``selenium.robot``.
...
...                 Avec ``web.robot``, nous sommes en mesure d'effectuer les tests
...                 aussi bien sur navigateur d'ordinateur que sur un appareil mobile.
...                 Cependant, certains mots-clés n'existent pas dans l'une ou l'autre librairie.
...
...                 Ci-dessous un tableau indiquant les mots-clés fonctionnels dans un seul environnement.
...                 | = Selenium uniquement = | = Appium uniquement = |
...                 | Obtenir Mon Url | Obtenir L'Uri Vers Appium Server |
...                 | Obtenir Mon Titre | Obtenir La Plateforme Du Device |
...                 | Obtenir La Cellule Du Tableau | Ouvrir L'Application Sur Le Device |
...                 | Obtenir Mon Url | Deployer Et Ouvrir L'Application Sur Le Device |
...                 | Choisir Le Fichier | Taper Sur l'Element |
...                 | Choisir Dans La Liste |  |
...                 | Choisir Dans La Liste Unique |  |
...                 | Choisir Le Bouton Radio |  |
...                 | Cocher Un Element |  |
...                 | Changer De Navigateur |  |
...
...                 Pour tous ces mots-clés il est préférable de les appeler directement depuis
...                 leur propre librairie plutôt que depuis ``web.robot`` et indiquer qu'un de
...                 ces mots-clés fait parti du test tant que les deux librairies n'ont pas les
...                 équivalents pour tous. Autrement, le test échouerait car il ne trouverait
...                 pas l'équivalence dans l'autre fichier.

Resource            utils.robot
Resource            selenium.robot
Resource            appium.robot
Resource            %{CNPDevicesSettings}
Resource            %{CNPBaseUrlSettings}


*** Variables ***
# Timeout global
${GLOBAL_TIMEOUT}    5s

# Modify Headers and Header Tool extensions
&{WEB_PROXY_AUTH}
                          # Authentification proxi
...                       name=Authorization    value=Basic ABC
                          # extension chrome
...                       crx=modheader.crx     modHeaderId=idgpnmonknjnojddfkpgkljpfnnfcklj
                          # extension firefox
...                       xpi=header_tool-0.6.2-fx.xpi


*** Keywords ***
Modifier Le Timeout Global
    [Documentation]  Modification du timeout appliqué à tous les mots clés web
    [Arguments]  ${myTimeout}

    Set Global Variable    ${GLOBAL_TIMEOUT}    ${myTimeout}


# Mettre En Pause En Cas De Plantage
#     selenium.Mettre En Pause En Cas De Plantage


Determiner Le Repertoire Des Captures D'Ecran
    [Arguments]    ${myPath}

    selenium.Determiner Le Repertoire Des Captures D'Ecran    ${myPath}


# ==============================================================================
# Parametrer navigateur ========================================================
# ==============================================================================
Sur Mon Navigateur
    [Documentation]     Aiguillage en fonction de la variable Jenkins MY_DEVICE.
    ...
    ...                 Navigateur Selenium ou Appium à partir du dictionnaire Jenkins MY_ENVIRONMENT_SETTINGS.
    ...
    ...                 Par défaut le DEVICE est PC.
    ...
    ...                 *Arguments :*
    ...                 - ``myKeyword``       est le mot-clé a appeler dans la librairie aiguillée.
    ...                 - ``myVarargs``       sont les arguments à ajouter lors de l'appel au mot-clé.
    ...                 *Return* : le résultat de la requete du mot cle.
    [Arguments]         ${myKeyword}    @{myVarargs}

    ${theDevice}=    OperatingSystem.Get Environment Variable    MY_DEVICE    default=PC
    ${passed}    ${libraryName}=    Run Keyword And Ignore Error    Get From Dictionary    ${CNP_DEVICES}    ${theDevice}libraryName
    # compatibilite ascendante - Si le dictionnaire n'existe pas,  prendre selenium par defaut
    ${libraryName}=    Set Variable If  '${passed}'=='PASS'             ${libraryName}    selenium
    ${returnValue}=    Run Keyword      ${libraryName}.${myKeyword}     @{myVarargs}

    [Return]    ${returnValue}


Fixer L'Url De Base
    [Documentation]     Positionner l'url de base en fonction de l'environnement.
    ...
    ...                 *Arguments :*
    ...                 - ``myAppEnvSettings``        ?
    [Arguments]         ${myAppEnvSettings}

    ${WEB_BASE_URL}=    Get From Dictionary    ${myAppEnvSettings}    %{MY_PFV}
    Set Suite Variable    ${WEB_BASE_URL}


Definir Les Capacites Chrome
    [Documentation]     Définir le paramétrage de fonctionnement de chrome_options.
    ...
    ...                 *Return* : la liste des options définies.

    ${options}=         Evaluate      sys.modules['selenium.webdriver'].ChromeOptions()      sys, selenium.webdriver
    ${prefs}=           Create Dictionary
    ...    download.default_directory=%{WORKSPACE}
    # To Turns off multiple download warning, https://stackoverflow.com/questions/15817328/disable-chrome-download-multiple-files-confirmation
    # ...                             profile.content_settings.exceptions.automatic_downloads."https://minos.altran.com,*".setting=1
    # To turns off download prompt
    ...                             download.prompt_for_download=${FALSE}
    Call Method     ${options}      add_argument                disable-extensions
    Call Method     ${options}      add_argument                --no-sandbox
    # Impression silencieuse qui désactive l'IHM d'impression et lance l'impression sur l'imprimante par défaut (lors d'un CTRL+P par exemple)
    Call Method     ${options}      add_argument                --kiosk-printing
    #Call Method    ${options}      add_argument                single-process
    #Call Method    ${options}      add_argument                headless
    Call Method     ${options}      add_argument                disable-gpu
    Call Method     ${options}      add_experimental_option     useAutomationExtension   ${FALSE}
    Call Method     ${options}      add_experimental_option     prefs   ${prefs}
    Call Method     ${options}      add_experimental_option     detach    ${TRUE}

    [Return]        ${options}


Definir Les Capacites Chromium
    [Documentation]     Définir le paramétrage de fonctionnement de chrome_options.
    ...
    ...                 *Return* : la liste des options définies.

    ${options}=         Evaluate      sys.modules['selenium.webdriver'].ChromeOptions()      sys, selenium.webdriver
    ${prefs}=           Create Dictionary
    ...    download.default_directory=%{WORKSPACE}
    ...    plugins.plugins_disabled=Chrome PDF Viewer
    ...    plugins.always_open_pdf_externally=${TRUE}
    # To Turns off multiple download warning, https://stackoverflow.com/questions/15817328/disable-chrome-download-multiple-files-confirmation
    # ...                             profile.content_settings.exceptions.automatic_downloads."https://minos.altran.com,*".setting=1
    # To turns off download prompt
    ...                             download.prompt_for_download=${FALSE}
    Call Method     ${options}      add_argument                disable-extensions
    Call Method     ${options}      add_argument                --no-sandbox
    # Impression silencieuse qui désactive l'IHM d'impression et lance l'impression sur l'imprimante par défaut (lors d'un CTRL+P par exemple)
    Call Method     ${options}      add_argument                --kiosk-printing
    #Call Method    ${options}      add_argument                single-process
    #Call Method    ${options}      add_argument                headless
    Call Method     ${options}      add_argument                disable-gpu
    Call Method     ${options}      add_experimental_option     useAutomationExtension   ${FALSE}
    Call Method     ${options}      add_experimental_option     prefs   ${prefs}
    Call Method     ${options}      add_experimental_option     detach    ${TRUE}

    [Return]        ${options}


# ==============================================================================
# Ouvrir navigateur ============================================================
# ==============================================================================
Ouvrir Chrome Avec Auth Pour Pdf
    [Documentation]     Ouvrir un fichier Pdf avec un navigateur.

    web.Sur Mon Navigateur      Ouvrir Chrome Avec Auth Pour Pdf


Ouvrir Chrome
    [Documentation]     Ouvrir Google Chrome sur le device.

    web.Sur Mon Navigateur      Ouvrir Chrome


Ouvrir ChromeDistant
    [Documentation]     Ouvrir Google Chrome de maniere distante sur le device.

    web.Sur Mon Navigateur      Ouvrir ChromeDistant


Ouvrir Chrome Avec Authentification Proxy
    [Documentation]     Start chrome browser.
    ...                 | Based on https://stackoverflow.com/questions/37456794/chrome-modify-headers-in-selenium-java-i-am-able-to-add-extension-crx-through

    web.Sur Mon Navigateur      Ouvrir Chrome Avec Authentification Proxy


Ouvrir Chrome Avec Profil
    [Documentation]     DEPRECATED - Ouvrir Chrome sur un profil utilisateur.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Profile``      le chemin vers le profil a charger.
    [Arguments]    ${myPath2Profile}

    web.Sur Mon Navigateur      Ouvrir Chrome Avec Profil    ${myPath2Profile}


Ouvrir Firefox
    [Documentation]     Ouvrir Firefox sur le device.

    web.Sur Mon Navigateur      Ouvrir Firefox


Ouvrir Firefox Avec BMP
    web.Sur Mon Navigateur      Ouvrir Firefox Avec BMP


Ouvrir Firefox Avec Authentification Proxy
    web.Sur Mon Navigateur      Ouvrir Firefox Avec Authentification Proxy


# ==============================================================================
# Se deplacer ==================================================================
# ==============================================================================
Aller Vers Le Site
    [Documentation]     Charger l'URL.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``           est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``  est le texte attendu pour confirmer que la bonne page a été atteinte.
    [Arguments]         ${myUri}    ${myExpectedText}

    ${myUrl}=    web.obtenir url a partir uri    ${myUri}
    web.Sur Mon Navigateur      Aller Vers Le Site    ${myUrl}    ${myExpectedText}

Naviguer Avec Le Browser Vers Le Site
    [Documentation]     Ouvrir le navigateur myBrowser ou MY_BROWSER par défaut, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``           est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``  est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myBrowser``       est le navigateur à utiliser.
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myBrowser}=%{MY_BROWSER}    ${myAlias}=None

    web.Sur Mon Navigateur      Naviguer Avec Le Browser Vers Le Site    ${myUrl}    ${myExpectedText}    ${myBrowser}    ${myAlias}


Naviguer Avec Le Browser Vers Le Site En Utilisant L'URL Complete
    [Documentation]     Ouvrir le navigateur myBrowser ou MY_BROWSER par défaut, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``           est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``  est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myBrowser``       est le navigateur à utiliser.
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myBrowser}=%{MY_BROWSER}    ${myAlias}=None

    web.Sur Mon Navigateur      Naviguer Avec Le Browser Vers Le Site En Utilisant L'URL Complete    ${myUrl}    ${myExpectedText}    ${myBrowser}    ${myAlias}


Naviguer Vers Le Site
    [Documentation]     Ouvrir le navigateur Chrome avec le profil, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``           est l'URL vers laquelle se diriger.
    ...                 - ``myProfile``       est le profil à utiliser.
    ...                 - ``myExpectedText``  est le texte attendu pour confirmer que la bonne page a été atteinte.
    [Arguments]         ${myUrl}    ${myProfile}    ${myExpectedText}

    web.Sur Mon Navigateur      Naviguer Vers Le Site    ${myUrl}    ${myProfile}    ${myExpectedText}


Naviguer Vers Le Site Contenant Un Pdf
    [Documentation]     Ouvrir le navigateur Chrome avec le profil, se positionner sur l'URL et vérifier la présence du texte sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myUrl``           est l'URL vers laquelle se diriger.
    ...                 - ``myExpectedText``  est le texte attendu pour confirmer que la bonne page a été atteinte.
    ...                 - ``myBrowser``       est le navigateur à utiliser.
    [Arguments]         ${myUrl}    ${myExpectedText}    ${myBrowser}=%{MY_BROWSER}

    web.Sur Mon Navigateur      Naviguer Vers Le Site Contenant Un Pdf    ${myUrl}    ${myExpectedText}    ${myBrowser}


Se Positionner Sur La Fenetre
    [Documentation]     Se positionner sur l'onglet correspondant.
    ...
    ...                 *Arguments :*
    ...                 - ``myTitle``     est l'onglet sur lequel il faut se positionner.
    [Arguments]         ${myTitle}

    ${previousWindow}    web.Sur Mon Navigateur    Se Positionner Sur La Fenetre    ${myTitle}

    [Return]    ${previousWindow}


Effectuer Un Retour Vers La Page Precedente
    [Documentation]    Simuler l'appui sur le bouton Retour du navigateur

    web.Sur Mon Navigateur    Effectuer Un Retour Vers La Page Precedente


# ==============================================================================
# Recuperer des donnees ========================================================
# ==============================================================================
Obtenir Mon Url
    [Documentation]     Obtenir l'URL courante.
    ...
    ...                 *Return* : l'adresse de la page actuelle.

    ${currentLocation}=      web.Sur Mon Navigateur      Obtenir Mon Url

    [Return]                ${currentLocation}


Obtenir Mon Titre
    [Documentation]     Obtenir le titre de la page courante.
    ...
    ...                 *Return* : le titre de la page actuelle.

    ${title}=      web.Sur Mon Navigateur      Obtenir Mon Titre

    [Return]    ${title}


Obtenir La Cellule Du Tableau
    [Documentation]     Obtenir le contenu d'une cellule précise.
    ...
    ...                 *Arguments :*
    ...                 - ``myTBodyIdLocator``    est la localisation du tableau.
    ...                 - ``myRow``               est la ligne demandée.
    ...                 - ``myCol``               est la colonne demandée.
    ...                 *Return* : la valeur de la cellule.
    [Arguments]         ${myTBodyIdLocator}    ${myRow}    ${myCol}

    ${cellValue}=      web.Sur Mon Navigateur      Obtenir La Cellule Du Tableau    ${myTBodyIdLocator}    ${myRow}    ${myCol}

    [Return]            ${cellValue}


Obtenir Le Texte De L'Attribut
    [Arguments]     ${myLocator}    ${myAttribute}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    ${myText}=    web.Sur Mon Navigateur    Obtenir Le Texte De L'Attribut    ${myLocator}    ${myAttribute}    ${myTimeout}    ${myError}

    [Return]    ${myText}


Obtenir Le Texte De L'Element
    [Arguments]    ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    ${cellValue}=    web.Sur Mon Navigateur    Obtenir Le Texte De L'Element    ${myLocator}    ${myTimeout}    ${myError}

    [Return]            ${cellValue}


Obtenir L'Element Le Plus Proche De La Liste
    [Documentation]     Obtenir l'élément le plus ressemblant de la liste.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est la localisation de la liste.
    ...                 - ``myExpectedText``  est le choix attendu.
    ...                 - ``myTimeout``       est le temps d'attente maximum pour trouver le choix attendu.
    [Arguments]         ${myLocator}    ${myExpectedOption}    ${myTimeout}=${GLOBAL_TIMEOUT}

    web.Sur Mon Navigateur      Obtenir L'Element Le Plus Proche De La Liste    ${myLocator}    ${myExpectedOption}    ${myTimeout}


Obtenir URL A Partir Uri
    [Documentation]     Construction de l'url a partir de l'uri et de l'url de base de l'environnement.
    ...
    ...                 *Arguments :*
    ...                 - ``myUri``       est l'URI a convertir en URL.
    ...                 *Return* : l'URL construite depuis l'URI.
    [Arguments]    ${myUri}

    return from keyword if    "${myUri}"==""    ${WEB_BASE_URL}
    ${url}=    Set Variable    ${WEB_BASE_URL}${myUri}
    [Return]    ${url}


obtenir le nombre de ligne du tableau
    [Arguments]    ${myTableIdLocator}

    ${count}=    web.Sur Mon Navigateur    obtenir le nombre de ligne du tableau    ${myTableIdLocator}
    [Return]    ${count}


obtenir le nombre de ligne du tableau sans locator
    [Arguments]    ${myTablexpath}

    ${count}=    web.Sur Mon Navigateur    obtenir le nombre de ligne du tableau sans locator    ${myTablexpath}
    [Return]    ${count}

obtenir le nombre d elements correspondants au locator
    [Arguments]    ${myXpath}

    ${count}=    web.Sur Mon Navigateur    obtenir le nombre d elements correspondants au locator    ${myXpath}
    [Return]    ${count}

obtenir la cellule du tableau hors entete
    [Arguments]    ${myTableIdLocator}    ${myRow}    ${myCol}

    ${cellValue}=    web.Sur Mon Navigateur    obtenir la cellule du tableau hors entete    ${myTableIdLocator}    ${myRow}    ${myCol}
    [Return]    ${cellValue}


Le Texte De L'Element Doit Avoir La Valeur Attendue
    [Documentation]    Compare le texte de l'élément avec la valeur attendue
    [Arguments]    ${myLocator}    ${targetValue}

    web.Sur Mon Navigateur    Le Texte De L'Element Doit Avoir La Valeur Attendue    ${myLocator}    ${targetValue}


Le Texte De L'Element Doit Etre Conforme A L'Expression Reguliere
    [Documentation]    Compare le texte de l'élément avec l'expression régulière
    [Arguments]    ${myLocator}     ${myRegexp}

    web.Sur Mon Navigateur    Le Texte De L'Element Doit Etre Conforme A L'Expression Reguliere    ${myLocator}     ${myRegexp}


L'Element Doit Etre Desactive
    [Documentation]    Vérifie que l'élément est désactivé
    [Arguments]    ${myLocator}

    web.Sur Mon Navigateur    L'Element Doit Etre Desactive    ${myLocator}


# ==============================================================================
# vérifier l'etat de la page ===================================================
# ==============================================================================
La Page Doit Etre Prete
    [Documentation]     Vérifier que la page est chargée.

    web.Sur Mon Navigateur      La Page Doit Etre Prete


Confirmer Que La Page Est Prete
    [Documentation]     Attendre que la page soit chargée.

    web.Sur Mon Navigateur      Confirmer Que La Page Est Prete


La Page Ne Contient Pas L'Element
    [Documentation]     Vérifier que la page ne contient pas un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément qui doit être absent.
    [Arguments]         ${myLocator}    ${myMessage}=None

    web.Sur Mon Navigateur      La Page Ne Contient Pas L'Element    ${myLocator}    ${myMessage}


La Page Contient
    [Documentation]     Vérifier que la page contient un texte.
    ...
    ...                 *Arguments :*
    ...                 - ``myExpectedText``      est le texte attendu sur la page.
    ...                 - ``myTimeout``           est le temps d'attente maximum autorise pour trouver le texte.
    [Arguments]         ${myExpectedText}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur      La Page Contient    ${myExpectedText}    ${myTimeout}    ${myError}


La Page Contient L'Element
    [Documentation]     Vérifier que la page contient un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément qui doit être present.
    ...                 - ``myTimeout``       est le temps d'attente maximum pour que l'élément apparaisse.
    [Arguments]         ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur      La Page Contient L'Element    ${myLocator}    ${myTimeout}    ${myError}


La Page Contient L'Element Visible
    [Documentation]     Vérifier que la page contient un élément visible.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément qui doit être present.
    ...                 - ``myTimeout``       est le temps d'attente maximum pour que l'élément apparaisse.
    [Arguments]         ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur      La Page Contient L'Element Visible    ${myLocator}    ${myTimeout}    ${myError}


Attendre La Disparition Element
    [Documentation]     Attendre qu'un élément disparaisse de la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément qui doit disparaître.
    ...                 - ``myTimeout``       est le temps d'attente maximum pour que l'élément disparaisse.
    [Arguments]         ${myLocator}    ${myTimeout}=180s

    web.Sur Mon Navigateur      Attendre La Disparition Element    ${myLocator}   ${myTimeout}


L'Element N'Est Plus Visible Sur La Page
    [Documentation]    Vérifie que l'élément n'est plus visible sur la page avant le timeout
    [Arguments]        ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    L'Element N'Est Plus Visible Sur La Page    ${myLocator}    ${myTimeout}    ${myError}


Verifier Le Titre De La Page
    [Documentation]    Vérifie que le titre de la page correspond bien à la valeur attendue
    [Arguments]        ${title}    ${myMessage}=None

    web.Sur Mon Navigateur    Verifier le titre de la page     ${title}    ${myMessage}


L'URL De La Page Doit Avoir La Valeur Attendue
    [Documentation]    Compare l'URL de la page active avec la valeur attendue
    [Arguments]        ${targetValue}

    web.Sur Mon Navigateur    L'URL De La Page Doit Avoir La Valeur Attendue    ${targetValue}


L'URL De La Page Contient
    [Documentation]    Attend l'apparition de l'URI dans l'URL de la page
    [Arguments]        ${myURI}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myMessage}=None

    web.Sur Mon Navigateur    L'URL De La Page Contient    ${myURI}    ${myTimeout}    ${myMessage}


# ==============================================================================
# Choisir un élément ===========================================================
# ==============================================================================
Deposer Un Fichier Dans Element Actif
    [Documentation]     Déposer un fichier dans un élément (input) en indiquant son chemin d'accès.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``    est la localisation de l'élement (input).
    ...                 - ``myPath2file``       est le chemin vers le fichier.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver l'élément.
    ...                 - ``myError``           est le message d'erreur personnalisable en cas d'échec pour trouver l'élément.
    [Arguments]         ${myXpathLocator}    ${myPath2file}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Deposer Un Fichier Dans Element Actif    ${myXpathLocator}    ${myPath2file}    ${myTimeout}    ${myError}


Choisir Dans La Liste
    [Documentation]     Faire un choix dans une liste déroulante.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est la localisation de la liste.
    ...                 - ``myOption``        est l'élément à sélectionner dans la liste déroulante.
    ...                 - ``myTimeout``       est le temps d'attente maximum pour trouver l'élément.
    [Arguments]         ${myLocator}    ${myOption}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Choisir Dans La Liste    ${myLocator}    ${myOption}    ${myTimeout}    ${myError}


Choisir Dans La Liste Par Valeur
    [Documentation]     Faire un choix dans une liste déroulante par attribut value.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est la localisation de la liste.
    ...                 - ``myOption``        est l'élément à sélectionner dans la liste déroulante.
    ...                 - ``myTimeout``       est le temps d'attente maximum pour trouver l'élément.
    [Arguments]         ${myLocator}    ${myOption}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Choisir Dans La Liste Par Valeur    ${myLocator}    ${myOption}    ${myTimeout}    ${myError}


Choisir Bouton Radio
    [Documentation]     Sélectionner un bouton radio.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'ensemble des boutons radios existants.
    ...                 - ``myChoice``        est le bouton radio à sélectionner.
    [Arguments]         ${myLocator}    ${myChoice}

    web.Sur Mon Navigateur    Choisir Bouton Radio    ${myLocator}    ${myChoice}


Le Bouton Radio Doit Etre Selectionne
    [Documentation]     S'assurer que le bouton radio est sélectionné'.
    ...
    ...                 *Arguments :*
    ...                 - ``myName``     est le nom du groupe de choix possible (name).
    ...                 - ``myValue``      est le bouton radio qui doit être sélectionné.
    ...                 _Version Selenium Only._
    [Arguments]         ${myName}    ${myValue}

    web.Sur Mon Navigateur    Le Bouton Radio Doit Etre Selectionne    ${myName}    ${myValue}


Le Bouton Radio Ne Doit Pas Etre Selectionne
    [Documentation]     S'assurer que le bouton radio n'est pas sélectionné'.
    ...
    ...                 *Arguments :*
    ...                 - ``myName``     est le nom du groupe de choix possible (name).
    ...                 _Version Selenium Only._
    [Arguments]         ${myName}

    web.Sur Mon Navigateur    Le Bouton Radio Ne Doit Pas Etre Selectionne    ${myName}


Cocher Un Element
    [Documentation]     Cocher un élément sur une page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est la checkbox à cocher.
    [Arguments]         ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}   ${myError}=None

    web.Sur Mon Navigateur    Cocher Un Element    ${myLocator}    ${myTimeout}   ${myError}


Decocher Un Element
    [Documentation]     Décocher un élément sur une page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est la checkbox à décocher.
    [Arguments]         ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}   ${myError}=None

    web.Sur Mon Navigateur    Decocher Un Element    ${myLocator}    ${myTimeout}   ${myError}


L'Element Doit Etre Coche
    [Documentation]     S'assurer que l'élément (checkbox) est coché.
    [Arguments]         ${myLocator}

    web.Sur Mon Navigateur    L'Element Doit Etre Coche    ${myLocator}


L'Element Doit Etre Decoche
    [Documentation]     S'assurer que l'élément (checkbox) est décoché.
    [Arguments]         ${myLocator}

    web.Sur Mon Navigateur    L'Element Doit Etre Decoche    ${myLocator}


# ==============================================================================
# Saisir du texte ==============================================================
# ==============================================================================
Saisir Un Secret Dans Element Actif
    [Documentation]     Saisir du texte non enregistré dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Saisir Un Secret Dans Element Actif    ${myLocator}    ${myText}    ${myTimeout}    ${myError}


Saisir Dans Element Actif Et Sortir Du Champ
    [Documentation]     Saisir du texte dans un élément puis quitter le champ.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}     ${myText}

    web.Sur Mon Navigateur    Saisir Dans Element Actif Et Sortir Du Champ    ${myLocator}    ${myText}


Appuyer Sur Une Touche
    [Documentation]     Appuyer sur une touche du clavier.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel la touche sera appuyée.
    ...                 - ``myAsciiCode``     est le code ASCII de la touche sur laquelle on veut appuyer.
    [Arguments]         ${myLocator}    ${myAsciiCode}

    web.Sur Mon Navigateur    Appuyer Sur Une Touche    ${myLocator}    ${myAsciiCode}


Saisir Dans Element Actif
    [Documentation]     Saisir du texte dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Saisir Dans Element Actif    ${myLocator}    ${myText}    ${myTimeout}    ${myError}


Saisir Dans Element Actif Avec Javascript
    [Documentation]     Saisir du texte dans un élément avec Javascript.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}    ${myAttribute}=value    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Saisir Dans Element Actif Avec Javascript    ${myLocator}    ${myText}    ${myAttribute}    ${myTimeout}    ${myError}


Saisir Dans Element Actif Sans Effacer L'Existant
    [Documentation]     Saisir du texte dans un élément à la suite de ce qui est déjà présent.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Saisir Dans Element Actif Sans Effacer L'Existant    ${myLocator}    ${myText}    ${myTimeout}    ${myError}



Saisir Dans Element Avec Verification De La Saisie
    [Documentation]     Saisir du texte dans un élément et s'assurer que la saise est effective.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}

    web.Sur Mon Navigateur    Saisir Dans Element Avec Verification De La Saisie    ${myLocator}    ${myText}


Saisir Avec Webdriver
    [Documentation]     Saisir du texte dans un élément directement depuis le webdriver.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}

    web.Sur Mon Navigateur    Saisir Avec Webdriver    ${myLocator}    ${myText}


Saisir Un Secret Avec Webdriver
    [Documentation]     Saisir du texte non enregistré dans un élément directement depuis le webdriver.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}

    web.Sur Mon Navigateur    Saisir Un Secret Avec Webdriver       ${myLocator}    ${myText}


Saisir Avec Javascript
    [Documentation]     Saisir du texte dans un élément en utilisant un script.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte sera saisi.
    ...                 - ``myText``          est le texte à saisir.
    [Arguments]         ${myLocator}    ${myText}    ${myAttribute}=value

    web.Sur Mon Navigateur    Saisir Avec Javascript        ${myLocator}    ${myText}    ${myAttribute}


Effacer Dans Element Actif
    [Documentation]     Effacer du texte dans un élément.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément dans lequel le texte effacé.
    [Arguments]         ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur    Effacer Dans Element Actif    ${myLocator}    ${myTimeout}    ${myError}


# ==============================================================================
# Cliquer ======================================================================
# ==============================================================================
Cliquer Sur Element Visible
    [Documentation]     Cliquer sur un élément visible sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``         est l'élément sur lequel cliquer.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver l'élément.
    [Arguments]         ${myLocator}     ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur      Cliquer Sur Element Visible     ${myLocator}     ${myTimeout}    ${myError}


Cliquer Sur Element Actif
    [Documentation]     Cliquer sur un élément actif sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément sur lequel cliquer.
    ...                 - ``myTimeout``       est le temps d'attente maximum pour trouver l'élément.
    [Arguments]         ${myLocator}     ${myTimeout}=${GLOBAL_TIMEOUT}    ${myError}=None

    web.Sur Mon Navigateur      Cliquer Sur Element Actif     ${myLocator}     ${myTimeout}    ${myError}


Cliquer Sur Element Visible Et Actif
    [Documentation]     Cliquer sur un élément visible et actif sur la page.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``      est l'élément sur lequel on veut cliquer.
    ...                 - ``myTimeout``           est le temps d'attente maximum pour trouver l'élément.
    [Arguments]         ${myXpathLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}   ${myError}=None

    web.Sur Mon Navigateur      Cliquer Sur Element Visible Et Actif    ${myXpathLocator}    ${myTimeout}    ${myError}


Cliquer Sur Element Visible Et Actif Avec Javascript
    [Documentation]     Cliquer sur un élément visible et actif sur la page avec Javascript.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpathLocator``      est l'élément sur lequel on veut cliquer.
    ...                 - ``myTimeout``           est le temps d'attente maximum pour trouver l'élément.
    [Arguments]         ${myXpathLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}   ${myError}=None

    web.Sur Mon Navigateur      Cliquer Sur Element Visible Et Actif Avec Javascript    ${myXpathLocator}    ${myTimeout}    ${myError}


Cliquer Sur Element Visible Et Actif Apres La Disparition D'Un Autre Element Qui Le Cache
    [Documentation]    Clique sur l'élément après la disparition d'un autre qui le cache
    [Arguments]    ${myLocatorClick}    ${myLocatorElementQuiCache}    ${myTimeout}=${GLOBAL_TIMEOUT}

    web.Sur Mon Navigateur    Cliquer Sur Element Visible Et Actif Apres La Disparition D'Un Autre Element Qui Le Cache    ${myLocatorClick}    ${myLocatorElementQuiCache}    ${myTimeout}


Cliquer Sur Element Visible Et Actif Le Plus A Gauche Possible
    [Documentation]    Clique sur la zone la plus à gauche possible de l'élément
    [Arguments]    ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}

    web.Sur Mon Navigateur    Cliquer Sur Element Visible Et Actif Le Plus A Gauche Possible    ${myLocator}    ${myTimeout}


Cliquer Sur Element Visible Et Actif En Maintenant Des Touches
    [Documentation]    Clique sur l'élément en maintenant une ou plusieurs touches du clavier
    [Arguments]    ${myLocator}    ${myKeys}    ${myTimeout}=${GLOBAL_TIMEOUT}

    web.Sur Mon Navigateur    Cliquer Sur Element Visible Et Actif En Maintenant Des Touches    ${myLocator}    ${myKeys}    ${myTimeout}


Cliquer Sur Element Avec Webdriver
    [Documentation]     Cliquer sur un élément avec le webdriver sans vérification préalable.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément sur lequel on veut cliquer.
    [Arguments]         ${myLocator}

    web.Sur Mon Navigateur      Cliquer Sur Element Avec Webdriver      ${myLocator}


Cliquer Sur Element Avec Javascript
    [Documentation]     Cliquer sur un élément en utilisant javascript.
    ...
    ...                 *Arguments :*
    ...                 - ``myLocator``       est l'élément sur lequel on veut cliquer.
    [Arguments]         ${myLocator}

    web.Sur Mon Navigateur      Cliquer Sur Element Avec Javascript      ${myLocator}


Accepter La Popup
    [Documentation]    Accepte la Pop-up qui apparaît à l'écran et retourne le message qu'elle affiche
    [Arguments]    ${myTimeout}=${GLOBAL_TIMEOUT}

    ${myMessage}=    web.Sur Mon Navigateur    Accepter La Popup    ${myTimeout}

    [Return]    ${myMessage}


Verifier Le Message De La Popup Et L'Accepter
    [Documentation]    Vérifie le message de la Pop-up qui apparaît à l'écran et l'accepter
    ...
    ...                 *Arguments :*
    ...                 - ``myMessage``     est le message que l'on veut vérifier.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver l'élément.
    [Arguments]         ${myMessage}    ${myTimeout}=${GLOBAL_TIMEOUT}

    web.Sur Mon Navigateur    Verifier Le Message De La Popup Et L'Accepter    ${myMessage}    ${myTimeout}


# ==============================================================================
# Generalites ==================================================================
# ==============================================================================
Capturer L'Ecran
    [Documentation]    Faire une capture d'écran de la page actuelle.

    web.Sur Mon Navigateur    Capturer L'Ecran


Capture L'Ecran Avec Timestamp Pour Nom De Fichier
    [Documentation]    Réalise une capture d'écran avec un timestamp pour nom de fichier

    web.Sur Mon Navigateur    Capture L'Ecran Avec Timestamp Pour Nom De Fichier


Capture L'Ecran Avec Timestamp Pour Page Identifier De Fichier
    [Documentation]    Réalise une capture d'écran avec un timestamp pour nom de fichier

    web.Sur Mon Navigateur    Capture L'Ecran Avec Timestamp Pour Page Identifier De Fichier


Fermer Le Navigateur
    [Documentation]     Fermer le navigateur actif.

    web.Sur Mon Navigateur    Fermer Le Navigateur


Fermer Tous Les Navigateurs
    [Documentation]    Fermer tous les navigateurs ouverts.

    web.Sur Mon Navigateur    Fermer Tous Les Navigateurs


Actualiser La Page
    [Documentation]    Actualise la page active du navigateur

    web.Sur Mon Navigateur    Actualiser La Page


Fermer L'Onglet Du Navigateur
    [Documentation]    Ferme l'onglet actif du navigateur

    web.Sur Mon Navigateur    Fermer L'Onglet Du Navigateur


Changer De Navigateur
    [Documentation]     Changer de navigateur.
    ...
    ...                 *Arguments :*
    ...                 - ``myIndex``       est le nouveau navigateur à ouvrir.
    [Arguments]         ${myIndex}

    web.Sur Mon Navigateur    Changer De Navigateur    ${myIndex}


Positionner Le Curseur Sur Un Element
    [Documentation]    Place le curseur de la souris au dessus d'un élément
    [Arguments]    ${myLocator}    ${myTimeout}=${GLOBAL_TIMEOUT}   ${myError}=None

    web.Sur Mon Navigateur    Positionner Le Curseur Sur Un Element    ${myLocator}    ${myTimeout}   ${myError}


Scroller Jusqu'A L'Apparition De L'Element
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit affiché à l'écran
    [Arguments]    ${myLocator}

    web.Sur Mon Navigateur    Scroller Jusqu'A L'Apparition De L'Element    ${myLocator}


Scroller Jusqu'A L'Apparition De L'Element Entier
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit entièrement affiché à l'écran
    [Arguments]    ${myLocator}

    web.Sur Mon Navigateur    Scroller Jusqu'A L'Apparition De L'Element Entier    ${myLocator}


Scroller Jusqu'A L'Apparition De L'Element En Dessous Du Header
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit affiché à l'écran et scroll à nouveau de la hauteur du header
    ...                Pertinant lorsque le header est situé au premier plan et cache potentiellement l'élément à cliquer
    [Arguments]        ${myLocator}    ${myHeaderLocator}

    web.Sur Mon Navigateur    Scroller Jusqu'A L'Apparition De L'Element En Dessous Du Header    ${myLocator}    ${myHeaderLocator}


Scroller Jusqu'A L'Apparition De L'Element Au Dessus Du Footer
    [Documentation]    Scroll sur la page jusqu'à ce que l'élément soit affiché à l'écran et scroll à nouveau de la hauteur du footer
    ...                Pertinant lorsque le footer est situé au premier plan et cache potentiellement l'élément à cliquer
    [Arguments]    ${myLocator}    @{myFooterLocators}

    web.Sur Mon Navigateur    Scroller Jusqu'A L'Apparition De L'Element Au Dessus Du Footer    ${myLocator}    @{myFooterLocators}


Scroller Jusqu'Au Bas De La Page
    [Documentation]    Scroll sur la page jusqu'au bas de la page

    web.Sur Mon Navigateur    Scroller Jusqu'Au Bas De La Page


Selectionner La Frame
    [Documentation]    Selectionne la frame ou l'iframe
    [Arguments]    ${myLocator}

    web.Sur Mon Navigateur    Selectionner La Frame    ${myLocator}


Deselectionner La Frame
    [Documentation]    Deselectionne la frame ou l'iframe

    web.Sur Mon Navigateur    Deselectionner La Frame


# ==============================================================================
# Telechargement ===============================================================
# ==============================================================================
Le Telechargement Doit Etre En Cours
    [Documentation]     Attendre le démarrage du téléchargement.

    Sleep    1s


Le Dernier Fichier Telecharge Doit Etre Complet
    [Documentation]     Vérifier que le fichier a été completement téléchargé.
    ...
    ...                 *Return* : le chemin vers le fichier téléchargé.

    ${path2DownloadedFile}=    utils.Obtenir Le Chemin Vers Le Dernier Fichier Du Repertoire    %{WORKSPACE}
    Should Not End With    ${path2DownloadedFile}    .part
    Should Not End With    ${path2DownloadedFile}    .tmp
    Should Not End With    ${path2DownloadedFile}    .crdownload

    [Return]    ${path2DownloadedFile}


Obtenir Le Dernier Fichier Telecharge
    [Documentation]     Obtenir le dernier fichier téléchargé.
    ...
    ...                 *Arguments :*
    ...                 - ``myRetryInterval``     est le temps d'attente entre deux tentatives de récuperation du fichier.
    ...                 *Return* : le chemin vers le fichier téléchargé.
    [Arguments]         ${myRetryInterval}=5s

    web.Le Telechargement Doit Etre En Cours
    ${path2DownloadedFile}=    Wait Until Keyword Succeeds    20x     ${myRetryInterval}    le dernier fichier telecharge doit etre complet

    [Return]    ${path2DownloadedFile}