*** Settings ***
Resource        ../resources/json.robot
Resource        ../utils/opencv2/test/Log.robot
Resource        %{EDA_PARAMETERS}
Library         ../utils/AMQ.py

*** Keywords ***
Connexion à Artemis MQ et consommer les messages de la file
    Log.INFO    Consommation de la file ${eda_queue_parameters_%{ENVIRONMENT}.topic}
    Log List    ${eda_clusters_%{ENVIRONMENT}}

    ${clusters}     Set Variable  ${Empty}
    FOR  ${cluster}  IN  @{eda_clusters_%{ENVIRONMENT}}
        ${clusterString}    Set Variable    ('${cluster.host}',${cluster.port})
        ${clusters}         Catenate    SEPARATOR=${Empty}     ${clusters}     ${clusterString},
    END
    ${clusters}     Fetch From Right    ${clusters}     ,

    Consommer File AMQ   ${eda_queue_parameters_%{ENVIRONMENT}.topic}  ${eda_queue_parameters_%{ENVIRONMENT}.user}  ${eda_queue_parameters_%{ENVIRONMENT}.password}
    Log.INFO    Les messages ont été consommés