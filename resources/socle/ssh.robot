*** Settings ***
Documentation    Tests unitaires de la librairie SSH

Library	         SSHLibrary

*** Keywords ***
Connexion SSH
    [Documentation]     Connexion ssh à une machine
    ...                 /!\ Ne pas oublier de fermer la connexion après utilisation /!\
    [Arguments]  ${host}    ${username}   ${password}   ${port}=22   ${timeout}=30 min

    Open Connection  host=${host}    port=${port}  encoding=UTF-8   timeout=${timeout}
    ${login}  Login   username=${username}    password=${password}

    Log    ${login}
    Log    Connexion OK à la machine ${host} avec le compte ${username}

Exécuter une commande
    [Arguments]  ${commande}
    ${retour}   Execute Command  ${commande}
    [Return]    ${retour}

Vérifier qu'un fichier n'existe pas
    [Arguments]  ${chemin}
    ${status}  ${var}=  Run Keyword And Ignore Error   SSHLibrary.File Should Not Exist  path=${chemin}
    ${exist}=   Set Variable If  '${status}'=='PASS'  ${True}   ${False}
    [Return]  ${exist}

Vérifier qu'un fichier existe
    [Arguments]  ${chemin}
    ${status}  ${var}=  Run Keyword And Ignore Error   SSHLibrary.File Should Exist  path=${chemin}
    ${exist}=   Set Variable If  '${status}'=='PASS'  ${True}   ${False}
    [Return]  ${exist}

Télécharger un fichier
    [Arguments]  ${chemin}
    Get File  source=${chemin}  destination=.

Déposer un fichier
    [Arguments]  ${source}  ${destination}  ${newline}=LF
    Put File  source=${source}  destination=${destination}  newline=${newline}