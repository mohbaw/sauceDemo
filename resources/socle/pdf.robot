# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource de gestion de fichier PDF.
...
...                 Cette librairie sert pour trois occasions :
...                 - Vérifier que deux fichiers PDF contiennent un même texte.
...                 - Extraire le contenu d'un fichier PDF et son nombre de pages.
...                 - Enregistrer le fichier PDF dans l'espace de travail du robot.

Library             PdfLibrary
Library             OperatingSystem
Library             FakerLibrary    locale=fr_FR
Library             ../utils/PDFCustomLibrary.py


*** Keywords ***
Verifier Que Le Texte Des Pdf Est Identique
    [Documentation]     Vérifier que le texte des PDF est identique.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2PdfOrigin``      est le chemin vers le PDF d'origine.
    ...                 - ``myPath2PdfTarget``      est le chemin vers le PDF de sortie.
    [Arguments]         ${myPath2PdfOrigin}    ${myPath2PdfTarget}

    ${pageNumberOrigin}    ${textOrigin}=    pdf.Extraire Le Texte Et Le Nombre De Pages Du Pdf    ${myPath2PdfOrigin}
    log.Debug    Le nombre de pages du fichier ${myPath2PdfOrigin} est: ${pageNumberOrigin}
    log.Debug    Le texte extrait est: ${textOrigin}
    ${pageNumberTarget}    ${textTarget}=    pdf.Extraire Le Texte Et Le Nombre De Pages Du Pdf    ${myPath2PdfTarget}
    log.Debug    Le texte extrait est: ${textTarget}
    log.Debug    Le nombre de pages du fichier ${myPath2PdfTarget} est: ${pageNumberTarget}
    Should Be Equal As Numbers          ${pageNumberOrigin}     ${pageNumberTarget}    Le nombre de page est different
    Should Be Equal    ${textOrigin}    ${textTarget}           Les textes sont differents


Extraire Le Texte Et Le Nombre De Pages Du Pdf
    [Documentation]     Obtenir le texte et la quantité de pages présentes dans le PDF.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Pdf``        est le chemin vers le PDF à consulter.
    ...                 *Return* : le nombre de page du PDF et son texte.
    [Arguments]         ${myPath2Pdf}

    ${pageNumber}   ${pdfText}=     PdfLibrary.Pdf To Txt    ${myPath2Pdf}

    [Return]        ${pageNumber}   ${pdfText}


Enregistrer Le Pdf Dans Le Workspace
    [Documentation]     Enregistrer le Pdf.
    ...
    ...                 *Arguments :*
    ...                 - ``myPdfStream``       est le flux de données du nouveau PDF.
    ...                 - ``myPdfName``         est le nom du fichier PDF.
    ...                 *Return* : le chemin vers le nouveau Pdf.
    [Arguments]         ${myPdfStream}    ${myPdfName}=${EMPTY}

    # Construire un nom fictif
    ${pdfName}=     Run Keyword If   '${myPdfName}'=='${EMPTY}'    FakerLibrary.Password    special_chars=False    digits=False    upper_case=True    lower_case=False
    ${path2Pdf}=    Set Variable    %{WORKSPACE}${/}${myPdfName}.pdf
    Create File     ${path2Pdf}    ${myPdfStream}

    [Return]        ${path2Pdf}


Creation PDF
    [Arguments]    ${myListeImages}    ${myTestName}=${TEST NAME}   ${myOutputDir}=%{WORKSPACE}

    PDFCustomLibrary.Creation PDF    ${myListeImages}    ${myTestName}   ${myOutputDir}
