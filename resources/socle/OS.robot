# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource pour lancer des scripts sur serveur distant.
...
...                 Le fichier OS.robot regroupe les scripts qui ont un impact sur le serveur distant.
...                 - la manipulation des fichiers/dossiers.
...                 - création de commande en fonction de l'OS.

Resource    string.robot
Resource    ssh.robot


*** Variables ***
#La valeur SYSTEM est une variable global créée dans system - OS Doit Etre Verifier
&{OS}   AIXFOLDERPATROL= /patrol
...     AIXCOMMANDEPROCESSPATROL= ps -ef | grep "PatrolAgent -p 2001"
...     AIXCOMMANDEPROCESSNETWORKER= ps -ef | grep nsrexe
...     LinuxFOLDERPATROL= /logiciels/patrol
...     LinuxCOMMANDEPROCESSPATROL= ps -ef | grep "PatrolAgent -p 2001"
...     LinuxCOMMANDEPROCESSNETWORKER= ps -ef | grep nsrexe


*** Keywords ***
# ==============================================================================
# Fonctionnalités pour fichiers ================================================
# ==============================================================================
Verifier L'Existence D'Un Fichier
    [Documentation]     Vérifier si un fichier existe.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin du fichier à trouver.
    ...                 *Return* : si le fichier existe par un code retour ``RC``.
    ...
    ...                 Exemple:
    ...                  | Verifier L'Existence D'Un Fichier | /application/test/apache.ksh |
    ...                  | Should Be Equal As Integers  | $RC | 0 |
    ...                  | Verifier L'Existence D'Un Fichier | /nop/ca/nexite/pas |
    ...                  | Should Be Equal As Integers  | $RC | 1 |
    [Arguments]    ${myPath2File}    ${myMessage}=None

    OperatingSystem.File Should Exist    ${myPath2File}    ${myMessage}


Rechercher Un Fichier Avec Un Mot
    [Documentation]     Chercher un nom de fichier ou de dossier
    ...                 avec une chaîne de caractères spécifique dans son nom.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Dir``    est le chemin où trouver les fichiers/dossiers.
    ...                 - ``myWord``        est le mot, la lettre ou le chiffre à trouver dans le dossier ``myPath2Dir``
    ...                 *Return* : une liste de fichier et/ou de dossiers avec ``myWord`` dans le nom.

    [Arguments]   ${myPath2Dir}   ${myWord}

    ${word}=    ssh.Executer La Commande    grep "${myWord}" "${myPath2Dir}"
    ${listWords}=   OS.Separer Un Argument Qui Est Une Liste De Mot Qui Passe A La Ligne    ${word}

    [Return]    @{listWords}


Rechercher Dans Un Fichier Un Mot
    [Documentation]     Chercher dans le fichier un mot / une phrase
    ...                 avec une chaîne de caractères spécifique.
    ...
    ...                 *Arguments :*
    ...                 - ``myFile``   est le nom de fichier.
    ...                 - ``myWord``   est le mot, la lettre ou le chiffre à trouver dans ``myFile``.
    ...                 *Return* : la phrase ou le mot ``myWord``.
    ...
    ...                 Exemple:
    ...                  | $resultCommand  | Rechercher Dans Un Fichier Un Mot | apacheV7.56 | version |
    ...                  | Should Be Equal As string  | $resultCommand | version:7.56  |
    ...                  | $resultCommand  | Rechercher Dans Un Fichier Un Mot | apacheV7.56 | Hello World |
    ...                  | Should Be Equal As string  | $resultCommand | ${EMPTY} |
    [Arguments]   ${myFile}  ${myWord}

    ${resultCommand}=    ssh.Executer La Commande    egrep "${myWord}" "${myFile}"

    [Return]    ${resultCommand}


Compter Le Nombre De Fichier En Utilisant Un Filtre
    [Documentation]     Compter tous les fichiers/dossiers présents
    ...                 avec le filtre (sed - cut - awk).
    ...
    ...                 *Arguments :*
    ...                 - ``myPath``        est le nom ou chemin du fichier/dossier.
    ...                 - ``myFilter``      est la commande shell comme un sed, awk ou cut.
    ...                 *Return* : le nombre de fichiers/dossiers trouvés avec ``myFilter``.
    [Arguments]   ${myPath}   ${myFilter}

    ${count}=    ssh.Executer La Commande    ls -a "${myPath}" | ${myFilter} | wc -l

    [Return]    ${count}


#TODO à tester
Supprimer Les Lignes Vide Dans Un Fichier
    [Documentation]     Supprimer les lignes vides dans un fichier.
    ...
    ...                 *Arguments :*
    ...                 - ``myFile``   est le nom du fichier.
    ...                 *Return* : le fichier sans ligne vide.
    [Arguments]     ${myFile}

    ${contents}=  Get File   ${myFile}
    Create File   ${myFile}
    ${lines}=  string.Supprimer Les Lignes    ${contents}
    FOR  ${line}   IN  @{lines}
        ${length}=  GetLength  ${line}
        Run Keyword if  ${length} != ${0}    Append To File  ${myFile}  ${line}\n
    END

    ${result}=  Run    cat ${myFile}
    [Return]    ${result}


# ==============================================================================
# Fonctionnalités pour dossiers ================================================
# ==============================================================================
Lister Le Contenu D'Un Dossier
    [Documentation]     Liste les fichiers contenus dans un dossier spécifique.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Dir``   est le chemin du dossier.
    ...                 - ``myPattern``   est l'expression régulière pour filtrer les résultats attendus.
    ...                 - ``myAbsolute``   détermine si on souhaite le chemin absolu de chaque fichier.
    ...                 *Return* : si le dossier contient des fichiers par un code retour.
    ...
    ...                 Exemple:
    ...                 | $resultCommand  | Rechercher Les Fichiers Dans Le Dossier | /application/test |
    ...                 | $resultCommand | une liste de fichier |
    ...                 | $resultCommand  | Rechercher Les Fichiers Dans Le Dossier | /nop/ca/nexiste/pas |
    ...                 | $resultCommand | Empty |
    [Arguments]    ${myPath2Dir}    ${myPattern}=None    ${myAbsolute}=False

    ${resultCommand}=    OperatingSystem.List Directory    ${myPath2Dir}    ${myPattern}    ${myAbsolute}

    [Return]    ${resultCommand}


Verifier L'Existence D'Un Dossier
    [Documentation]     Vérifier qu'un dossier existe.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Dir``   est le chemin théorique du dossier.
    ...                 *Return* : si le dossier existe par un code retour ``RC``.
    ...
    ...                 Exemple:
    ...                 | Existence D'Un Dossier | /application/test |
    ...                 | Should Be Equal As Integers  | $RC | 0 |
    ...                 | Existence  D Un Dossier | /nop/ca/nexiste/pas |
    ...                 | Should Be Equal As Integers  | $RC | 1 |
    [Arguments]   ${myPath2Dir}

    ssh.Executer La Commande    test -d "${myPath2Dir}"


Rechercher Un Dossier Avec Un Nom Sans La Liste De Mot
    [Documentation]     TODO: revoir doc. Chercher un dossier à l'aide d'un ou plusieurs filtres.
    ...
    ...                 La liste de mots est séparée par un | pour l'expression ``egrep``.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2Dir``    est le chemin du dossier.
    ...                 - ``myProd``        est le nom du produit.
    ...                 - ``myListWords``   est la liste de mots separés par un |.
    ...                 *Return* : si le dossier existe par un code retour.
    ...
    ...                 Exemple:
    ...                 | $resultCommand  | Rechercher Un Dossier Avec Un Nom Sans La Liste De Mot | /application/test | apache | 7|patch|fix |
    ...                 | Should Be Equal As Integers  | $resultCommand | 0 |
    ...                 | $resultCommand  | Rechercher Un Dossier Avec Un Nom Sans La Liste De Mot | rien/ici | apache | 7|patch |
    ...                 | Should Be Equal As Integers  | $resultCommand | 1 |
    [Arguments]  ${myPath2Dir}  ${myProd}   ${myListWords}
    ${resultCommand}=    ssh.Executer La Commande    find ${myPath2Dir} -type d -name "${myProd}*" 2> /dev/null | egrep -v "${myListWords}"
    [Return]    ${resultCommand}
