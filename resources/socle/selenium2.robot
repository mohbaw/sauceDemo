*** Settings ***
Documentation       Ressource wrapper de la ``SeleniumLibrary``,
...                 une librairie externe propre au projet Robot Framework.
...
...                 Cette dernière permet d'automatiser des actions
...                 sur un navigateur web depuis un ordinateur.
...                 Ce fichier implémente donc les mots-clés
...                 situés dans `web.robot` pour la partie IHM web.
...
...                 ! Ni ``SeleniumLibrary`` ni ``selenium.robot``
...                 ne doivent être inclus dans les robots !
...
...                 Les mêmes recommandations s'appliquent pour
...                 l'architecture utilisant ``AppiumLibrary``.
...                 Voir la documentation dans le fichier resource
...                 ``appium.robot`` pour plus de détails.
...
...                 En somme, il faut donc utiliser *uniquement* ``web.robot``
...                 implémentant un mécanisme appelé "aiguillage" qui permet
...                 une abstraction de la plateforme visée
...                 appelant les bons mot-clés pour chacune d'elle.
...
...                 Selenium s'appuie sur un WebDriver, un programme permettant
...                 de piloter le navigateur web propre à ce dernier.
...                 Il existe un WebDriver pour chaque navigateur web différent
...                 (Chrome, Firefox, Edge, ...).
...                 Ce(s) dernier(s), doi(ven)t être accessible(s) dans
...                 la variable de chemin (_path_), ou être déjà démarré(s)
...                 sur la machine qui en aura besoin...


Library             OperatingSystem
Library             SeleniumLibrary

Resource            Log.robot
Resource            utils.robot



*** Keywords ***
# -----------------------------------------------------------------------------
# Navigateur(s)
# -----------------------------------------------------------------------------

# Appels du socle (web.robot)
Ouvrir Navigateur
    [Arguments]    ${navigateur}
    ...            ${url}
    ...            ${uri}
    ...            ${maximiser}
    ...            ${texte_attendu}
    ...            ${alias}

    # We also screenshot on failure, so we don't want SeleniumLibrary to do the same.
    SeleniumLibrary.Register Keyword To Run On Failure    NOTHING

    # https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html#Timeouts,%20waits,%20and%20delays
    SeleniumLibrary.Set Selenium Implicit Wait    10s

    # Global Timeout - Defaulting to 10 except if specified in env
    ${timeout}=    OperatingSystem.Get Environment Variable
    ...            TIMEOUT    default=10
    SeleniumLibrary.Set Selenium Timeout    ${timeout}

    Run Keyword    selenium2.Ouvrir ${navigateur}    alias=${alias}
    IF    ${maximiser}
        SeleniumLibrary.Maximize Browser Window
    END
    web2.Naviguer Vers    url=${url}    uri=${uri}    texte_attendu=${texte_attendu}


Naviguer Vers
    [Arguments]    ${url}
    ...            ${uri}
    ...            ${texte_attendu}

    # Si url n'est pas spécifiée on veut charger une URL projet
    # Sinon on ne passe pas dans ce IF
    IF    not "${url}"
        ${r}=    Run Keyword And Ignore Error    Import Resource    %{CNP_URL_ENV_LIST}
        IF    "${r}[0]" == "FAIL"
            log.Error  "${r}[1]"
            Fail       Le fichier de configuration contenant les URLs des applications IHM n'a pas pu être chargé
        ELSE
            # L'url de l'app est associé au dictionnaire des environnements...
            ${app}=    Evaluate        "%{PROJECT_NAME}".upper()
            ${url}=    Set Variable    ${CNP_${app}}[%{ENVIRONNEMENT}]
        END
    END

    log.Debug    Naviguer vers ${url}${uri}
    SeleniumLibrary.Go To      ${url}${uri}  # Optionnal URI append to url

    IF    "${texte_attendu}"
        log.Debug    La page doit contenir: ${texte_attendu}
        SeleniumLibrary.Wait Until Page Contains    text=${texte_attendu}
    ELSE
        selenium2.Confirmer Page Prete
    END


Basculer Vers Navigateur
    [Arguments]    ${alias}

    SeleniumLibrary.Switch Browser    ${alias}


Basculer Vers Onglet
    [Arguments]    ${locator}

    ${previous_window}=    SeleniumLibrary.Switch Window    ${locator}

    [Return]    ${previous_window}


Fermer Onglet
    SeleniumLibrary.Close Window


Fermer Navigateur
    SeleniumLibrary.Close Browser


Fermer Tous Navigateurs
    SeleniumLibrary.Close All Browsers


# Mot-clés techniques (ne doivent pas être utilisés en dehors de ce fichier)
Ouvrir Chrome
    [Arguments]    ${alias}

    ${chrome_options}=                    selenium2.Definir Options Chrome
    SeleniumLibrary.Create Webdriver      Chrome
    ...                                   alias=${alias}
    ...                                   chrome_options=${chrome_options}
    ...                                   kwargs={"options": "--log-level=3"}


Ouvrir Chromium
    [Arguments]    ${alias}

    ${chrome_options}=                    selenium2.Definir Options Chrome
                                                              # TODO: SPEC workspace
    ${chrome_options.binary_location}=    Set Variable        C:/robot/app/Chromium/chrome.exe
    SeleniumLibrary.Create Webdriver      Chrome
    ...                                   alias=${alias}
    ...                                   chrome_options=${chrome_options}
    ...                                   kwargs={"options": "--log-level=3"}  #TOFIX


Definir Options Chrome
    ${options}=    Evaluate      sys.modules['selenium.webdriver'].ChromeOptions()      sys, selenium.webdriver
    ${prefs}=      Create Dictionary
    ...              download.default_directory=%{WORKSPACE}
    ...              download.prompt_for_download=${FALSE}
    # Hotfix pour télécharger les PDFs au lieu de les lire dans le navigateur
    # (Applicable Chromium seulement)
    ...              plugins.plugins_disabled=Chrome PDF Viewer
    ...              plugins.always_open_pdf_externally=${TRUE}

    Call Method     ${options}      add_argument                disable-extensions
    Call Method     ${options}      add_argument                disable-gpu
    # Impression silencieuse qui désactive l'IHM d'impression et lance l'impression
    # sur l'imprimante par défaut (lors d'un CTRL+P par exemple)
    Call Method     ${options}      add_argument                --kiosk-printing
    # Call Method    ${options}      add_argument                headless  #TMTC

    Call Method     ${options}      add_experimental_option     prefs   ${prefs}
    # Risque de sécurité béant, est-ce vraiment nécéssaire ? Supposé non
    #Call Method     ${options}      add_argument                --no-sandbox

    [Return]        ${options}


Ouvrir Firefox
    [Arguments]    ${alias}

    ${ff_profile}=    selenium2.Definir Profil Firefox
    SeleniumLibrary.Create Webdriver    Firefox    alias=${alias}    firefox_profile=${ff_profile}


Definir Profil Firefox
    [Arguments]    ${download_directoy}=%{WORKSPACE}

    # Set extension to handle authentication if needed
    ${ff_profile}=    Evaluate
    ...  sys.modules['selenium.webdriver'].FirefoxProfile()
    ...  sys, selenium.webdriver

    # To turn off download prompt
    Call Method    ${ff_profile}    set_preference    browser.download.manager.showWhenStarting    ${False}
    Call Method    ${ff_profile}    set_preference    browser.cache.memory.enable    ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.dir                ${download_directoy}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.alertOnEXEOpen      ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.focusWhenStarting   ${False}
    #Call Method    ${profile}    set_preference    browser.download.useDownloadDir              ${True}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.useWindow           ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.closeWhenDone       ${True}
    Call Method    ${ff_profile}    set_preference    services.sync.prefs.sync.browser.download.manager.showWhenStarting   ${False}
    Call Method    ${ff_profile}    set_preference    browser.helperApps.neverAsk.openFile         application/csv
    Call Method    ${ff_profile}    set_preference    browser.helperApps.neverAsk.saveToDisk       application/msword, text/xml, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application${/}download, application/octet-stream, application/xls, text/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    Call Method    ${ff_profile}    set_preference    browser.helperApps.alwaysAsk.force           ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.showAlertOnComplete     ${False}
    Call Method    ${ff_profile}    set_preference    browser.download.manager.closeWhenDone       ${False}
    Call Method    ${ff_profile}    set_preference    pdfjs.enabledCache.state                     ${False}
    Call Method    ${ff_profile}    set_preference    browser.link.open_newwindow                  ${2}

    [Return]        ${ff_profile}


Ouvrir Internet Explorer
    [Arguments]    ${alias}

    SeleniumLibrary.Create Webdriver    Ie    ${alias}



# -----------------------------------------------------------------------------
# Basiques Navigation
# -----------------------------------------------------------------------------

# Appels du socle (web.robot)
Actualiser Page
    SeleniumLibrary.Reload Page


Faire Precedent
    SeleniumLibrary.Go Back


Faire Defiler Page
    [Arguments]    ${direction}

    IF    """${direction}""".upper() == "HAUT"
        SeleniumLibrary.Execute Javascript    window.scrollTo(0, 0);
    ELSE IF    """${direction}""".upper() == "BAS"
        SeleniumLibrary.Execute Javascript    window.scrollTo(0, document.body.scrollHeight);
    ELSE
        # Scroll jusqu'a l'élément et le met au centre de l'écran
        ${element}=    SeleniumLibrary.Get WebElement    ${direction}
		SeleniumLibrary.Execute Javascript    arguments[0].scrollIntoView();    ARGUMENTS    ${element}
    END


# Mot-clés techniques (ne doivent pas être utilisés en dehors de ce fichier)
Page Prete Ou Pas
    [Documentation]     Cette instruction JS permet de savoir que la page est
    ...                 ''complète'' (DOM retourné) avant de Wait explicitement
    ...                 par la suite.

    ${isReady}=    SeleniumLibrary.Execute Javascript
    ...             return document.readyState;
    Should Be Equal As Strings    ${isReady}    complete


Confirmer Page Prete
    Wait Until Keyword Succeeds    20s    1s    selenium2.Page Prete Ou Pas



# ============= Uniquement mots-clés web.robot à partir d'ici =================
# -----------------------------------------------------------------------------
# Récupération Données
# -----------------------------------------------------------------------------
Obtenir URL Page
    selenium2.Confirmer Page Prete
    ${url}=    SeleniumLibrary.Get Location

    [Return]    ${url}


Obtenir Titre Page
    selenium2.Confirmer Page Prete
    ${title}=    SeleniumLibrary.Get Title

    [Return]     ${title}


Obtenir Texte Element
    [Arguments]    ${locator}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    ${timeout}    ${error}
    ${extract}=    SeleniumLibrary.Get Text             ${locator}

    [Return]    ${extract}


Obtenir Valeur Attribut Element
    [Arguments]    ${locator}
    ...            ${attribute}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    ${timeout}    ${error}
    ${attribute}=    SeleniumLibrary.Get Element Attribute    ${locator}    ${attribute}

    [Return]    ${attribute}


Obtenir Nombre Correspondances
    [Arguments]    ${locator}

    selenium2.Confirmer Page Prete
    ${count}=    SeleniumLibrary.Get Element Count    ${locator}

    [Return]    ${count}



# -----------------------------------------------------------------------------
# Interactions
# -----------------------------------------------------------------------------

# -- Elements
Saisir Dans Champ
    [Arguments]    ${locator}
    ...            ${texte}
    ...            ${secret}
    ...            ${effacer}
    ...            ${visible}
    ...            ${javascript}
    ...            ${sortir}        #TODO: Non implémenté
    ...            ${timeout}
    ...            ${erreur}

    IF    ("${texte}" and "${secret}")
        Fail    Un seul argument `texte` OU `secret` doit être utilisé, sinon aucun pour juste effacer.
    END

    # Enabled allows to be certain that the element is 'loaded' fully (think interactive) # TOVERIFY 85% sure
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}    ${timeout}    ${erreur}
    # Visible will crash if the element is not visible from human eyes - TOVERIFY
    Run Keyword If    ${visible}
    ...  SeleniumLibrary.Wait Until Element Is Visible    ${locator}    ${timeout}    ${erreur}

    Run Keyword If    ${effacer}    SeleniumLibrary.Clear Element Text    ${locator}
    IF    "${texte}"
        log.Debug    Saisir le texte '${texte}'
        IF    ${javascript}
            ${element}=    SeleniumLibrary.Get WebElement    ${locator}
            SeleniumLibrary.Execute Javascript    arguments[0].value = "${texte}";    ARGUMENTS    ${element}
        ELSE
            SeleniumLibrary.Input Text    ${locator}    ${texte}
        END
    ELSE IF    "${secret}"
        log.Debug    Saisir le secret
        IF    ${javascript}
            ${element}=    SeleniumLibrary.Get WebElement    ${locator}
            SeleniumLibrary.Execute Javascript    arguments[0].value = "${secret}";    ARGUMENTS    ${element}
        ELSE
            SeleniumLibrary.Input Password    ${locator}    ${secret}
        END
    END


Cliquer Sur Element
    [Arguments]    ${locator}
    ...            ${visible}
    ...            ${javascript}
    ...            ${timeout}
    ...            ${erreur}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}    ${timeout}    ${erreur}
    Run Keyword If    ${visible}
    ...  SeleniumLibrary.Wait Until Element Is Visible    ${locator}    ${timeout}    ${erreur}

    IF    ${javascript}
        ${element}=    SeleniumLibrary.Get WebElement    ${locator}
		SeleniumLibrary.Execute Javascript    arguments[0].click();    ARGUMENTS    ${element}
    ELSE
        SeleniumLibrary.Click Element    ${locator}
    END


Selectionner Dans Liste
    [Arguments]    ${locator}
    ...            ${option}
    ...            ${valeur}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    log.Debug    Sélectionner ${option}
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}    ${timeout}    ${error}

    IF  ${valeur}
        SeleniumLibrary.Select From List By Value    ${locator}    ${option}
    ELSE
        SeleniumLibrary.Select From List By Label    ${locator}    ${option}
    END


Selectionner Bouton Radio
    [Arguments]    ${locator}
    ...            ${choice}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}
    SeleniumLibrary.Select Radio Button              ${locator}    ${choice}
    selenium2.Bouton Radio Doit Etre Selectionne      ${locator}    ${choice}


Cocher Element
    [Arguments]    ${locator}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}    ${timeout}    ${error}
    SeleniumLibrary.Select Checkbox    ${locator}


Decocher Element
    [Arguments]    ${locator}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}    ${timeout}    ${error}
    SeleniumLibrary.Unselect Checkbox    ${locator}



# -- Divers
Deposer Fichier Dans Element
    [Arguments]    ${locator}
    ...            ${file_path}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}    ${timeout}    ${error}
    SeleniumLibrary.Choose File    ${locator}    ${file_path}


#TODO: SPEC - Think...
# TODO: implement tab dans `Saisir Dans Champ`
# Saisir Dans Element Actif Et Sortir Du Champ
    # Tabulation pour sortir du champ
    # selenium2.Appuyer Sur Une Touche      ${locator}    u'\ue004'
Appuyer Sur Touche
    [Arguments]    ${locator}
    ...            ${ascii_code}

    SeleniumLibrary.Press Keys    ${locator}    ${ascii_code}


Positionner Curseur Sur Element
    [Arguments]    ${locator}
    ...            ${timeout}
    ...            ${error}

    SeleniumLibrary.Wait Until Element Is Visible    ${locator}    ${timeout}    ${error}
    SeleniumLibrary.Mouse Over    ${locator}


Selectionner Frame
    [Arguments]    ${locator}

    SeleniumLibrary.Select Frame    ${locator}


Deselectionner Frame
    SeleniumLibrary.Unselect Frame



# -----------------------------------------------------------------------------
# Vérifications
# -----------------------------------------------------------------------------

Capturer Ecran
    SeleniumLibrary.Capture Page Screenshot    selenium-screenshot-{index:03}.png


# -- Page
URL Page Doit Correspondre
    [Arguments]    ${url}
    ...            ${timeout}
    ...            ${error}

    # ${page}=    SeleniumLibrary.Get Location
    SeleniumLibrary.Wait Until Location Is    ${url}    timeout=${timeout}    message=${error}
    ...    message=L'URL de la page est differente de l'URL attendue ${url}


URL Page Doit Contenir
    [Arguments]    ${uri}
    ...            ${timeout}
    ...            ${error}

    SeleniumLibrary.Wait Until Location Contains    expected=${uri}    timeout=${timeout}    message=${error}


Titre Page Doit Correspondre
    [Arguments]    ${title}
    ...            ${error}

    SeleniumLibrary.Title Should Be    ${title}    ${error}


Page Doit Contenir
    [Arguments]    ${text}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Contains    ${text}    timeout=${timeout}    error=${error}


Page Ne Doit Pas Contenir
    [Arguments]    ${text}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Does Not Contain    ${text}    timeout=${timeout}    error=${error}


Page Doit Contenir Element
    [Arguments]    ${locator}
    ...            ${visible}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    IF  ${visible}
        SeleniumLibrary.Wait Until Element Is Visible   ${locator}    timeout=${timeout}    error=${error}
    END
    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    timeout=${timeout}    error=${error}


Page Ne Doit Pas Contenir Element
    [Arguments]    ${locator}
    ...            ${visible}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    IF  ${visible}
        SeleniumLibrary.Wait Until Element Is Visible   ${locator}    timeout=${timeout}    error=${error}
    END
    SeleniumLibrary.Wait Until Page Does Not Contain Element    ${locator}    timeout=${timeout}    error=${error}


# -- Elements
Texte Element Doit Correspondre
    [Arguments]    ${locator}
    ...            ${text_expected}

    ${text_value}=    selenium2.Obtenir Texte Element    ${locator}    timeout=${None}    error=${None}
    Should Be Equal    ${text_value}    ${text_expected}
    ...    msg=Le texte a la valeur ${text_value} alors que la valeur attendue était ${text_expected}.    values=False


Texte Element Doit Correspondre Regex
    [Arguments]    ${locator}
    ...            ${regex}

    ${text_value}=    selenium2.Obtenir Texte Element    ${locator}    timeout=${None}    error=${None}
    Should Match Regexp    ${text_value}    ${regex}
    ...    msg=Le texte a la valeur ${text_value} alors qu'il devait respecter l'expression régulière ${regex}.    values=False


Bouton Radio Doit Etre Selectionne
    [Arguments]    ${name}
    ...            ${value}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${name}
    SeleniumLibrary.Radio Button Should Be Set To       ${name}    ${value}


Bouton Radio Ne Doit Pas Etre Selectionne
    [Arguments]    ${name}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Contains Element       ${name}
    SeleniumLibrary.Radio Button Should Not Be Selected    ${name}


#         SeleniumLibrary.Wait Until Page Contains Element    ${locator}    timeout=${timeout}    error=${error}
#         SeleniumLibrary.Wait Until Element Is Visible  ${locator}    timeout=${timeout}    error=${error}

Element Doit Etre Inactif
    [Arguments]    ${locator}

    SeleniumLibrary.Element Should Be Disabled    ${locator}


Attendre Disparition Element
    [Arguments]    ${locator}
    ...            ${timeout}
    ...            ${error}

    SeleniumLibrary.Wait Until Element Is Not Visible    ${locator}    ${timeout}    ${error}


Element Doit Etre Coche
    [Arguments]    ${locator}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    ${timeout}    ${error}
    SeleniumLibrary.Checkbox Should Be Selected         ${locator}


Element Doit Etre Decoche
    [Arguments]    ${locator}
    ...            ${timeout}
    ...            ${error}

    selenium2.Confirmer Page Prete
    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    ${timeout}    ${error}
    SeleniumLibrary.Checkbox Should Not Be Selected     ${locator}


# -- Popup
Accepter Pop-up
    [Arguments]    ${timeout}

    ${text}=    SeleniumLibrary.Handle Alert    ACCEPT    ${timeout}

    [Return]    ${text}


Verifier Message Pop-up Et Accepter
    [Arguments]    ${text}
    ...            ${timeout}

    SeleniumLibrary.Alert Should Be Present    ${text}    ACCEPT    ${timeout}


# [SPEC] - Comment est organisé le %WORKSPACE% ?
# TODO: Supprimer et uniformiser à la manière d'Ouvrir Navigateur
# Determiner Le Repertoire Des Captures D'Ecran
#     [Arguments]    ${path}

#     SeleniumLibrary.Set Screenshot Directory    ${path}
