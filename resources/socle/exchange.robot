# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource wrapper de MicrosoftExchangeLibrary.
...
...                 Cette librairie sert à gérer les échanges d'e-mail.
...
...                 A l'aide de ses mots-clés, il est possible d'obtenir des informations
...                 sur un mail spécifique et d'en envoyer.
...
...                 Cette librairie permet, également, de manipuler les e-mails pour les
...                 placer dans des bannettes différentes en fonction de leur intitulé.

Library             ExchangeLibrary
Library             OperatingSystem

Resource            log.robot


*** Variables ***


*** Keywords ***
Obtenir L'Objet Du Mail
    [Documentation]     Récuperer l'objet d'un mail.
    ...
    ...                 *Arguments :*
    ...                 - ``myMailBox``         est la boîte mail dans laquelle se trouve le mail.
    ...                 - ``myMailId``          est l'identifiant du mail.
    ...                 - ``mySearchFolder``    est la banette du mail dans laquelle se trouve le mail.
    ...                 *Return* : l'objet du mail.
    [Arguments]         ${myMailBox}    ${myMailId}    ${mySearchFolder}

    ${objet}=    ExchangeLibrary.get subject    ${myMailBox}    ${myMailId}    ${mySearchFolder}

    [Return]    ${objet}


Envoyer Le Mail Avec Une Piece Jointe Sans Trace
    [Documentation]     Envoyer un mail sans trace avec une pièce-jointe.
    ...
    ...                 *Arguments :*
    ...                 - ``myMail`` est un objet mail contenant divers éléments :
    ...                |                - un nom de compte ;
    ...                |                - un sujet ;
    ...                |                - un corps de texte ;
    ...                |                - ? ;
    ...                |                - un ou des pièces-jointes éventuelles.
    [Arguments]         ${myMail}

    ExchangeLibrary.Send Mail Without Local Copy    ${myMail.account}
    ...                                             ${myMail.subject}
    ...                                             ${myMail.body}
    ...                                             ${myMail.toRecipient}
    ...                                             ${myMail.path2AttachFile}
    log.Success    Le mail ${myMail} est envoye


Se Connecter A La Boite Mail
    [Documentation]     Se connecter à la boîte mail.
    ...
    ...                 *Arguments :*
    ...                 - ``myUser``            est le nom de l'utilisateur.
    ...                 - ``myPassword``        est le mot de passe de l'utilisateur.
    ...                 - ``myDomain``          est le domaine de connexion.
    ...                 - ``myAddressMail``     est l'adresse mail de l'utilisateur.
    ...                 - ``myServerMail``      est le serveur de la boîte mail.
    ...                 *Return* : le contenu de la boîte mail.
    [Arguments]         ${myUser}    ${myPassword}    ${myDomain}    ${myAddressMail}    ${myServerMail}

    ${mailBox}=    ExchangeLibrary.Connect To Exchange Mailbox    ${myUser}    ${myPassword}    ${myDomain}    ${myAddressMail}    ${myServerMail}
    log.Success    Je suis connecte sur la boite mail ${myAddressMail} avec le compte ${myUser}

    [Return]    ${mailBox}


Obtenir Un Mail Avec Une Piece Jointe
    [Documentation]     Rechercher un mail dans le dossier qui contient une pièce-jointe avec l'extension.
    ...
    ...                 *Arguments :*
    ...                 - ``myMailBox``         est la boîte mail dans laquelle s'effectue la recherche.
    ...                 - ``myExtension``       est l'extension du fichier à rechercher.
    ...                 - ``mySearchFolder``    est la bannette où chercher le mail.
    ...                 *Return* : l'identifiant du mail.
    [Arguments]         ${myMailBox}    ${myExtension}    ${mySearchFolder}

    ${passed}    ${mailId}=    Run Keyword And Ignore Error    ExchangeLibrary.search mail by attachment    ${myMailBox}    ${myExtension}    ${mySearchFolder}
    # En cas d'erreur, le message d'erreur est retourne dans mailId
    Run Keyword If    '${passed}'=='FAIL'    Fail    Je n'ai pas acces a la bannette ${mySearchFolder}: ${mailId}
    Run Keyword If    '${mailId}' == '${EMPTY}'    log.Warning    Aucun mail trouve avec une piece jointe d'extension ${myExtension} dans la bannette ${mySearchFolder}
    ...    ELSE    log.Success    J'ai trouve le mail ${mailId} qui contient une piece jointe avec l'extension ${myExtension}

    [Return]    ${mailId}


Obtenir Les Noms De Pieces Jointes Qui Correspondent
    [Documentation]     Rechercher les pièces-jointes avec un nom conforme à l'expression régulière.
    ...
    ...                 *Arguments :*
    ...                 - ``myMailBox``         est la boîte mail dans laquelle s'effectue la recherche.
    ...                 - ``myMailId``          est l'identifiant du mail.
    ...                 - ``myRegexp``          est l'expression régulière employée.
    ...                 - ``mySearchFolder``    est la bannette où chercher le mail.
    ...                 *Return* : les noms de pièces-jointes correspondantes.
    [Arguments]         ${myMailBox}    ${myMailId}    ${myRegexp}    ${mySearchFolder}

    ${namepj}=    ExchangeLibrary.Get Attachments Name By Regexp    ${myMailBox}    ${myMailId}    ${mySearchFolder}    ${myRegexp}

    [Return]    ${namePj}


Obtenir Le Contenu De La Piece Jointe Qui Correspond
    [Documentation]     Rechercher le contenu d'une pièce-jointe avec un nom conforme à l'expression régulière.
    ...
    ...                 *Arguments :*
    ...                 - ``myMailBox``         est la boîte mail dans laquelle s'effectue la recherche.
    ...                 - ``myMailId``          est l'identifiant du mail.
    ...                 - ``myRegexp``          est l'expression régulière employée.
    ...                 - ``mySearchFolder``    est la bannette où chercher le mail.
    ...                 *Return* : le contenu de la pièce-jointe correspondante.
    [Arguments]         ${myMailBox}    ${myMailId}    ${myRegexp}    ${mySearchFolder}

    ${content}=    ExchangeLibrary.Get Attachment Content By Regexp    ${myMailBox}    ${myMailId}    ${mySearchFolder}    ${myRegexp}

    [Return]    ${content}


Obtenir La Date De Reception
    [Documentation]     Obtenir la date de réception du mail.
    ...
    ...                 *Arguments :*
    ...                 - ``myMailBox``         est la boîte mail dans laquelle s'effectue la recherche.
    ...                 - ``myMailId``          est l'identifiant du mail.
    ...                 - ``mySearchFolder``    est la bannette où chercher le mail.
    ...                 *Return* : la date de réception du mail.
    [Arguments]         ${myMailBox}    ${myMailId}    ${mySearchFolder}

    ${datetimeReceived}=    ExchangeLibrary.Get Datetime Received    ${myMailBox}    ${myMailId}    ${mySearchFolder}

    [Return]    ${datetimeReceived}


Enregistrer Les Pieces Jointes
    [Documentation]     Enregistrer les pièces-jointes du mail.
    ...
    ...                 *Arguments :*
    ...                 - ``myMailBox``         est la boîte mail dans laquelle s'effectue la recherche.
    ...                 - ``myMailId``          est l'identifiant du mail.
    ...                 - ``myFolder``          est la bannette où chercher le mail.
    ...                 - ``path2directory``    est le dossier où le mail doit être enregistré.
    [Arguments]         ${myMailBox}    ${myMailId}    ${myFolder}    ${path2directory}

    Create Directory    ${path2directory}
    ExchangeLibrary.Save Attachment    ${myMailBox}    ${myMailId}    ${myFolder}    ${path2directory}
    log.Success    J'ai enregistre les pieces jointes du mail ${mailId} dans ${path2directory}


Deplacer Le Mail
    [Documentation]     Déplacer un mail dans une autre bannette.
    ...
    ...                 *Arguments :*
    ...                 - ``myMailBox``         est la boite mail dans laquelle s'effectue la recherche.
    ...                 - ``myMailId``          est l'identifiant du mail.
    ...                 - ``myDestFolder``      est la bannette vers laquelle déplacer le mail.
    ...                 - ``myOrigFolder``      est la bannette d'origine du mail à déplacer.
    [Arguments]         ${myMailBox}    ${myMailId}    ${myDestFolder}    ${myOrigFolder}

    ExchangeLibrary.Move Mail    ${myMailBox}    ${myMailId}    ${myDestFolder}    ${myOrigFolder}
    log.Success    J'ai deplace le mail ${myMailId} de ${myOrigFolder} vers ${myDestFolder}
