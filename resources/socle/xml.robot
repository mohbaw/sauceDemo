# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource de gestion de fichiers XML.
...
...                 Cette librairie a pour but de parcourir un fichier XML pour en
...                 extraire des informations après avoir vérifié leur existence.

Library             XML              lxml=True
Library             ../utils/XmlUtils.py
Library             ../utils/XmlComparatorLibrary.py

Resource            log.robot

*** Keywords ***
L'Element Identifie Par Xpath Doit Etre Present
    [Documentation]     Vérifier la présence de l'élément indique par le Xpath dans le document XML.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpath``     est l'élément à trouver.
    ...                 - ``myXml``       est le fichier XML dans lequel doit se trouver l'élément.
    ...                 *Return* : le nombre d'occurrence de l'élément.
    [Arguments]         ${myXpath}          ${myXml}

    ${count}=       Evaluate Xpath      ${myXml}    count(${myXpath})
    Should Be True  ${count}>0  l'element identifie par ${myXpath} n'a pas ete trouve dans le document XML

    [return]        ${count}


Obtenir Element Identifie Par Xpath
    [Documentation]     Extraire le texte de l'élément indiqué par le Xpath dans le document XML.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpath``     est l'élément à trouver.
    ...                 - ``myXml``       est le fichier XML dans lequel doit se trouver l'élément.
    ...                 *Return* : la conversion en chaîne de caractères de l'élément trouvé.
    [Arguments]         ${myXpath}    ${myXml}

    xml.L'Element Identifie Par Xpath Doit Etre Present    ${myXpath}    ${myXml}
    ${string}=    Evaluate Xpath    ${myXml}    string(${myXpath})

    [return]    ${string}


Obtenir Attribut Identifie Par Xpath
    [Documentation]     Extraire le texte de l'attribut de l'élément indiqué par le Xpath dans le document XML.
    ...
    ...                 *Arguments :*
    ...                 - ``myXpath``     est l'élément à trouver.
    ...                 - ``myXml``       est le fichier XML dans lequel doit se trouver l'élément.
    ...                 *Return* : la conversion en chaîne de caractères de l'élément trouvé.
    [Arguments]         ${myXpath}    ${myAttribut}    ${myXml}

    xml.L'Element Identifie Par Xpath Doit Etre Present    ${myXpath}    ${myXml}
    ${string}=    Get Element Attribute    ${myXml}    ${myAttribut}    string(${myXpath})

    [return]    ${string}



#-----  Modification -----#

Modifier le texte d'un élément
    [Arguments]  ${xmlSource}   ${texte}    ${xpath}
    XML.Set Element Text  source=${xmlSource}  text=${texte}  xpath=${xpath}



Sauvegarder le flux XML
    [Arguments]  ${xml}  ${pathToSave}
    XML.Save Xml    source=${xml}     path=${pathToSave}



#-----  Comparaison de flux XML -----#
Comparer deux flux XML
    [Arguments]     ${fluxRef}  ${fluxTest}  ${activateLogs}=${False}  @{exclusions}
    [Documentation]     Compare deux flux XML
    ...
    ...                 *Arguments :*
    ...                       - ``fluxRef`` : le flux XML de référence
    ...                       - ``fluxTest`` : le flux XML à tester
    ...                       - ``activateLogs`` :
    ...                       - ``exclusions (facultatif)`` : liste d'exclusion contenant les "XmlPath" des champs à exclure de la comparaison
    ...
    ...                 *Return :*
    ...                       - ``erreurs`` : la liste des divergences constatées
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Flux de référence=  | =Flux de test= | =Activation des logs= | =Liste d'exclusion= |
	...					| @{divergences}= | xml.Comparer deux flux xml | ${fluxRef} | ${fluxTest} | ${activateLogs} | ${exclusions} |

    @{erreurs}=  XmlComparatorLibrary.Comparer Flux Xml  ${fluxRef}  ${fluxTest}  ${activateLogs}  @{exclusions}
    Run Keyword If  "@{erreurs}"!="@{EMPTY}" and "${activateLogs}"=="${True}"    lister les divergences  @{erreurs}

    [Return]  @{erreurs}

Comparer deux flux xml deprecated
    [Arguments]     ${fluxRef}    ${fluxTest}   ${exclusions}=@{EMPTY}  ${activateLogs}=${False}
    [Documentation]     Compare deux flux XML
    ...
    ...                 *Arguments :*
    ...                       - ``fluxRef`` : le flux XML de référence
    ...                       - ``fluxTest`` : le flux XML à tester
    ...                       - ``exclusions (facultatif)`` : liste d'exclusion contenant les "XmlPath" des champs à exclure de la comparaison
    ...
    ...                 *Return :*
    ...                       - ``erreurs`` : la liste des divergences constatées
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Flux de référence=  | =Flux de test= | =Liste d'exclusion= |
	...					| @{divergences}= | xml.Comparer deux flux xml | ${fluxRef} | ${fluxTest} | ${exclusions} |

	Set Test Variable   ${exclusions}
    Set Test Variable   @{erreurs}  @{EMPTY}
    #XML.Elements Should Be Equal    ${fluxRef}    ${fluxTest}
    ${racine}=  XML.Get Element     ${fluxRef}
    xml.comparer deux objets    ${fluxRef}    ${fluxTest}   ${racine.tag}
    Run Keyword If  "@{erreurs}"!="@{EMPTY}" and "${activateLogs}"=="${True}"    lister les divergences
    [Return]    @{erreurs}

comparer deux objets
    [Arguments]     ${objRef}    ${objTest}   ${xmlPath}
    Log  ${xmlPath}
    @{childrenRef}=    Get Child Elements  ${objRef}
    @{childrenTest}=    Get Child Elements  ${objTest}
    Run Keyword If      '@{childrenRef}'!= '@{EMPTY}'    xml.comparer les enfants    ${childrenRef}   ${childrenTest}   ${xmlPath}
    ...     ELSE        xml.comparer deux champs    ${objRef}  ${objTest}   ${xmlPath}

comparer deux champs
    [Arguments]  ${objRef}  ${objTest}  ${xmlPath}
    ${donneeExclue}   ${excl}=  Run keyword and ignore error    List Should Contain Value   ${exclusions}    ${xmlPath}
    ${status}   ${erreur}=  Run keyword and ignore error    XML.Elements Should Be Equal	 ${objRef}  ${objTest}
    Run keyword if    '${status}'=='FAIL' and '${donneeExclue}'=='FAIL'   Append To List  ${erreurs}  La valeur ${xmlPath} obtenue (${objTest.text}) est différente de la valeur attendue (${objRef.text})

comparer les enfants
    [Arguments]  ${childrenRef}    ${childrenTest}    ${xmlPath}

    @{childrenRefModified}=    traiter les listes d'enfants     ${childrenRef}
    @{childrenTestModified}=    traiter les listes d'enfants     ${childrenTest}

    Log List  ${childrenRefModified}
    FOR  ${childRef}   IN  @{childrenRefModified}
        ${indexElt}=    Get Index From List  ${childrenRefModified}  ${childRef}
        ${childTest}=   Get From List     ${childrenTestModified}     ${indexElt}
        #${status}  ${child}=  Run keyword and ignore error   Get From List  ${childRef}  0
        #${tag}=     Set Variable If     "${status}"=="PASS"    ${child.tag}    ${childRef.tag}
        ${status}   ${objType}=  Run Keyword And Ignore Error   Log  ${childRef.tag}
        ${extendedXmlPath}=   Run Keyword If      '${status}'=='PASS'     Catenate  SEPARATOR=/    ${xmlPath}    ${childRef.tag}
        #${extendedXmlPath}=   Catenate  SEPARATOR=/    ${xmlPath}    ${childRef.tag}
        Run Keyword If  "${status}"=="PASS"     comparer deux objets    ${childRef}    ${childTest}    ${extendedXmlPath}
        ...       ELSE      comparer deux listes  ${childRef}    ${childTest}    ${extendedXmlPath}
        #Run Keyword If    "${status}"=="PASS"     comparer deux listes    ${childRef}    ${childTest}    ${extendedXmlPath}
    END

comparer deux listes
    [Arguments]  ${listeRef}    ${listeTest}    ${xmlPath}
    FOR  ${objectRef}   IN  @{listeRef}
        ${index}=  Get Index From List  ${listeRef}  ${objectRef}
        ${status}   ${objectTest}=  Run keyword and ignore error      Get From List  ${listeTest}  ${index}
        ${extendedXmlPath}=   Catenate  SEPARATOR=${EMPTY}    ${xmlPath}    [${index}]
        Run Keyword If  '${objectRef.tag}'=='BlocRole'  xml.comparer deux champs  ${objectRef}  ${objectTest}  ${extendedXmlPath}
        ...    ELSE IF  '${status}'=='PASS'  xml.comparer deux objets  ${objectRef}  ${objectTest}  ${extendedXmlPath}
        ...    ELSE     Append To List  ${erreurs}  L'objet ${xmlPath} est présent dans l'attendu mais absent de l'obtenu
    END

lister les divergences
    [Arguments]     @{erreurs}
    FOR  ${divergence}  IN  @{erreurs}
        Run Keyword If  '${divergence}'=='Aucune différence détectée'  log.SUCCESS  Les deux flux sont identiques
        ...       ELSE  log.WARNING   ${divergence}
    END

lister les tags des enfants
    [Arguments]  ${childrenList}
    @{childrenTags}=    Create List  @{EMPTY}
    FOR  ${childRef}   IN  @{childrenList}
        Append To List      ${childrenTags}     ${childRef.tag}
    END
    [Return]  @{childrenTags}

déterminer si l'enfant fait partie d'une liste
    [Arguments]  ${childrenTags}    ${childTag}
        ${nbreOccurs}=    Get Match Count    ${childrenTags}    ${childTag}
        ${isList}=    Set Variable If  '${nbreOccurs}'>='2'   ${true}  ${false}
    [Return]  ${isList}

construire une liste d'enfants
   [Arguments]  @{eltList}  ${eltsTag}

   @{childList}=    Create List  @{EMPTY}
   FOR  ${elt}  IN  @{eltList}
        Run Keyword If  '${elt.tag}'=='${eltsTag}'  Append To List  @{childList}   ${elt}
   END

   [Return]  @{childList}

traiter les listes d'enfants
    [Arguments]  ${childrenList}

    @{childrenListModified}=    Create List  @{childrenList}
    @{childrenListTags}=    lister les tags des enfants     ${childrenList}

    @{childLists}=              Create List  @{EMPTY}
    FOR  ${child}   IN  @{childrenList}
        ${nbreOccurs}=    Get Match Count    ${childrenListTags}    ${child.tag}
        ${isList}=    Set Variable If  '${nbreOccurs}'>='2'   ${true}  ${false}
        Run Keyword If  '${isList}'=='${True}'  Append To List  ${childLists}  ${child.tag}
        Run Keyword If  '${isList}'=='${True}'  Remove Values From List  ${childrenListModified}  ${child}
    END
    @{CHILDREN_LISTS}=    Remove Duplicates   ${childLists}
    Log List    ${CHILDREN_LISTS}
    Set Test Variable  @{CHILDREN_LISTS}

    FOR  ${elt}  IN  @{CHILDREN_LISTS}
        @{elts}=    _créer la liste d'éléments    ${elt}    ${childrenList}
        Append To List  ${childrenListModified}  ${elts}
    END

    #FOR  ${child}   IN  @{childrenList}
    #    ${nbreOccurs}=    Get Match Count    ${eltLists}    ${child.tag}
    #    ${isList}=    Set Variable If  ${nbreOccurs} > 0   ${true}  ${false}
    #    Run Keyword If  '${isList}'=='${True}'  Remove Values From List     ${childrenListModified}     ${child}
    #    @{list}=    Run Keyword If  '${isList}'=='${True}'  creer la liste d'enfants    ${childrenList}     ${child.tag}
    #    Run Keyword If  '${isList}'=='${True}'  Append To List  ${childrenListModified}  ${list}
        #Remove Values From List     ${childrenListTags}     ${child.tag}
    #END

    [Return]  @{childrenListModified}

passer la variable CONTAIN_CHILDREN_LIST à vrai
    ${CONTAIN_CHILDREN_LIST}=    Set Variable  ${True}

creer la liste d'enfants
    [Arguments]     ${childrenList}     ${childTagElt}
    @{list}=    Create List  @{EMPTY}
    FOR  ${child}   IN  @{childrenList}
        Run Keyword If  '${child.tag}'=='${childTagElt}'  Append To List    ${list}  ${child}
    END
    [Return]  @{list}

_créer la liste d'éléments
    [Arguments]  ${tag}   ${elements}
    @{eltsList}=    Create List  @{EMPTY}
    FOR  ${element}  IN  @{elements}
        Run Keyword If  "${element.tag}"=="${tag}"  Append To List  ${eltsList}  ${element}
    END
    [Return]    @{eltsList}

Charger le flux XML
    [Arguments]    ${emplacement}

    ${flux}     XML.Parse Xml    ${emplacement}
    [Return]    ${flux}

Indenter une chaîne XML
    [Documentation]   Indente une chaine de caractères XML
    [Arguments]   ${xmlString}
    ${prettyXmlString}   XmlUtils.PrettyPrint XML    ${xmlString}
    [Return]  ${prettyXmlString}