*** Settings ***
Documentation       Ressource wrapper de AppiumLibrary pour Android.
...
...                 Cette librairie permet de démarrer une application sur un
...                 appareil Android.
...
...                 = /!\\ Ne pas utiliser /!\\ =
...                 *[http://localhost:7070/doc/keywords/14/|Appium] effectue*
...                 *l'aiguillage vers cette librairie selon l'appareil utilisé.*

Library             OperatingSystem

Resource            appium.robot
Resource            log.robot


*** Keywords ***
Deployer Et Ouvrir L'Application Sur Le Device
    [Documentation]     Déployer et ouvrir une application sur un appareil Android.
    ...
    ...                 *Arguments :*
    ...                 - ``myApp``         est le nom de l'application à deployer.
    ...                 - ``myDevice``      est le type de l'appareil utilisé.
    ...                 _Appium Android._
    [Arguments]         ${myApp}    ${myDevice}

    log.Info            Deployer l'app: \n${C4_PACKAGES.android}/${myApp.androidapp}\nsur le device: \n${myDevice}
    ${uri2appium}=      appium.Obtenir L'Uri Vers Appium Server
    log.Info            URI vers le service Appium: ${uri2appium}
    Open Application    ${uri2appium}
    ...                 platformName=android
    ...                 platformVersion=${myDevice.platformVersion}
    ...                 deviceName=${myDevice.deviceName}
    ...                 udid=${myDevice.udid}
    ...                 app=${C4_PACKAGES.android}/${myApp.androidapp}


Ouvrir L'Application
    [Documentation]     Ouvrir une application sur un appareil Android.
    ...
    ...                 *Arguments :*
    ...                 - ``myApp``         est le nom de l'application à ouvrir.
    ...                 - ``myDevice``      est le type de l'appareil utilisé.
    ...                 _Appium Android._
    [Arguments]         ${myApp}

    ${uri2appium}=    appium.Obtenir L'Uri Vers Appium Server
    ${optionsChrome}    Create Dictionary    args=--disable-fre
    AppiumLibrary.Open Application    ${uri2appium}
    ...                               platformName=android
    ...                               platformVersion=${CNP_DEVICES.%{MY_DEVICE}platformVersion}
    ...                               deviceName=${CNP_DEVICES.%{MY_DEVICE}deviceName}
    # ...                               udid=${myDevice.udid}
    ...                               appPackage=${myApp.androidappPackage}
    ...                               appActivity=${myApp.androidappActivity}


Ouvrir Le Navigateur
    [Documentation]     Ouvrir un navigateur web sur un appareil Android.
    ...
    ...                 *Arguments :*
    ...                 - ``myBrowser``     est le nom du navigateur à ouvrir.
    ...                 _Appium Android._
    [Arguments]         ${myBrowser}

    ${uri2appium}=    appium.Obtenir L'Uri Vers Appium Server
    ${platformVersion}    Set Variable    ${CNP_DEVICES.%{MY_DEVICE}platformVersion}
    ${deviceName}    Set Variable    ${CNP_DEVICES.%{MY_DEVICE}deviceName}
    AppiumLibrary.Open Application    ${uri2appium}
    ...                               platformName=Android
    ...                               platformVersion=${platformVersion}
    ...                               deviceName=${deviceName}
    ...                               browserName=${myBrowser}
