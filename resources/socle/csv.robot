# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource de gestion de fichiers CSV.
...
...                 Cette librairie sert à gérer les fichiers CSV. Elle dispose de
...                 trois mots-clés servant à convertir le CSV en JSON, envoyer les
...                 données dans un dictionnaire Python pour mieux les traiter et
...                 rajouter le contenu d'un dictionnaire Python dans un fichier CSV.

Library             CSVLibrary

Resource            Log.robot
Resource            json.robot


*** Variables ***


*** Keywords ***
Lire Le Fichier Csv Dans Un Json
    [Documentation]     Charger le fichier CSV et le convertir au format JSON avec une racine nommé _rows_.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin vers le fichier CSV.
    ...                 *Return* : le fichier CSV sous forme de JSON.
    [Arguments]         ${myPath2File}

    ${csvContent}=      csv.Lire Le Fichier CSV Dans Un Dictionnaire    ${myPath2File}
    ${type}=            Evaluate    str(type(${csvContent}))
    # Ajouter la racine explicite pour permettre la recherche sur la cle avec JSON Path (https://github.com/h2non/jsonpath-ng/issues/16)
    ${rows}=            Set Variable    { 'rows': ${csvContent} }
    ${type}=            Evaluate    str(type(${rows}))
    # convertir en JSON object
    ${jsonData}=        json.Convertir En Json    ${rows}
    ${type}=            Evaluate    str(type(${jsonData}))

    [Return]    ${jsonData}


Lire Le Fichier Csv Dans Un Dictionnaire
    [Documentation]     Lire le fichier CSV et le transformer en dictionnaire Python.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2File``   est le chemin vers le fichier CSV.
    ...                -  ``myDelimiter``   est le caractère de séparation des données.
    ...                 *Return* : le fichier CSV sous forme de dictionnaire Python.
    [Arguments]         ${myPath2File}    ${myDelimiter}=;

    @{dict}=    CSVLibrary.Read Csv File To Associative    ${myPath2File}    delimiter=${myDelimiter}

    [Return]    ${dict}


Ajouter Le Dictionnaire Dans Le Fichier Csv
    [Documentation]     Ajouter un dictionnaire Python dans un fichier CSV.
    ...
    ...                 *Arguments :*
    ...                 - ``myDict est le dictionnaire à ajouter dans le fichier CSV.
    ...                 - ``myPath2File``   est le chemin vers le fichier CSV.
    ...                 - ``myDelimiter``   est le caractère de séparation des données.
    [Arguments]         ${myDict}    ${myPath2File}    ${myDelimiter}=;

    ${keys}=            Get Dictionary Keys       ${myDict}
    ${values}=          Get Dictionary Values     ${myDict}
    ${data}=            Create List
    Append To List      ${data}     ${keys}
    Append To List      ${data}     ${values}
    ${delimiter}=       Evaluate    str(u'${myDelimiter}').encode('utf-8')
    ${quoting}=         Convert To Integer    1
    CSVLibrary.Append To Csv File    ${myPath2File}    ${data}    quoting=${quoting}    delimiter=${delimiter}
