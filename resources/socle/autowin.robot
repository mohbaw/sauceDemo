# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource wrapper de la librairie Autowin.
...
...                 Cette librairie sert à effectuer de la reconnaissance d'image, clavier et souris.
...                 Elle sert principalement pour la gestion des images.
...
...                 = Fonctionnalités =
...                 Cette librairie a la capacité de lancer des logiciels, à condition qu'on lui
...                 indique le chemin d'accès de celui-ci. Par la suite, elle est capable d'effectuer
...                 des actions sur le programme lancé.
...
...                 == Gestion des images ==
...                 Tous les mots-clés travaillant avec les images nécessitent que
...                 l'utilisateur indique un degré de similarité de reconnaissance
...                 d'une image ``myImage`` avec un argument ``mySimilarity``.
...
...                 Lors de la recherche, un temps d'attente maximal est précisé avec ``myTimeout``.
...                 Par défaut, il est de *10* secondes.
...
...                 == Saisie de texte ==
...                 Il existe un mot-clé permettant de taper du texte :
...                 | _autowin.Taper Le Texte_ \ \ \ \ \ \ Mon texte
...                 Contrairement à d'autres librairies comme Appium ou Selenium, la saisie de texte
...                 ne nécessite pas d'indiquer de localisateur. La saisie s'effectuera à l'endroit où
...                 le dernier clic aura eu lieu.
...
...                 Dans le cas où l'utilisateur n'a pas effectué de clic, la saisie de texte se fera
...                 tout de même.
...
...                 Avec ``autowin.robot`` il est aussi possible de saisir du texte sur des images.
...                 Voici les différents mots-clés dédiés :
...                 | = Mot-clé = | = Arguments = | = _Note_ = |
...                 | *Saisir Sur L'Image* | ``myImage``, ``myText``, ``mySimilarity``, ``myTimeout`` | _Garde une trace de l'action effectuée._ |
...                 | *Saisir Un Secret Sur L'Image* | ``myImage``, ``myText``, ``mySimilarity``, ``myTimeout`` | _Permet de ne pas garder de trace de l'action._ |
...                 | *Cliquer Et Saisir Le Texte* | ``myText`` | _Clique sur la dernière image trouvée, puis saisie le texte._ |
...                 | *Saisir Dans Region Sur L'Image* | ``myRegion``, ``myImage``, ``myText``, ``mySimilarity``, ``myTimeout`` | _Permet un clic sur une image localsiée dans une région précise._ |
...

Library             OperatingSystem
Library             Process
Library             Collections
Library             opencv

Resource            log.robot


*** Variables ***
&{OPENCV_SETTINGS}      similarity=0.8    timeout=10    path2images=    workdir=


*** Keywords ***
Fermer Les Logiciels
    [Documentation]     Fermer les logiciels ouverts.

    Process.Terminate All Processes


Lancer Le Logiciel
    [Documentation]     Démarrer le logiciel.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2App``    est le chemin vers le logiciel à exécuter.
    ...                 - ``arguments``     sont les arguments à donner au lancement du logiciel.
    ...                 *Return* : la fenêtre du programme exécuté.
    [Arguments]         ${myPath2App}    ${myArguments}

    log.Debug       Lancement du programme ${myPath2App}
    ${handle}=      Process.Start Process    ${myPath2App}    ${myArguments}

    [Return]        ${handle}


Fixer La Racine Du Chemin Vers Les Images
    [Documentation]     Positionner le répertoire des images à charger et positionner le workspace.
    ...
    ...                 *Arguments :*
    ...                 - ``myPath2images``     est le chemin vers les images.
    [Arguments]         ${myPath2images}

    log.Debug    Le repertoire racine de recherche des \ images est ${myPath2images}
    ${OPENCV_SETTINGS.path2images}=    Set Variable    ${myPath2images}
    # Fixe le repertoire de stockage des images ecrans dans le workspace Jenkins du build
    opencv.Set Screenshot Directory    %{WORKSPACE}


Chercher L'Image
    [Documentation]     Attendre l'apparition d'une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``       est l'image recherchée.
    ...                 - ``mySimilarity``  est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    log.Debug    Attente de l'image ${myImage}
    # ajouter la racine des images
    ${path2image}=          Set Variable    ${OPENCV_SETTINGS.path2images}/${myImage}
    opencv.Find Image       ${path2image}    ${myTimeout}    ${mySimilarity}


Capturer L'Ecran
    [Documentation]     Effectuer une capture d'écran.

    opencv.Screenshot It


Saisir Sur L'Image
    [Documentation]     Saisir du texte sur la position d'une image avec une trace.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``myText``            est le texte à saisir sur l'image.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${myText}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher L'Image        ${myImage}    ${mySimilarity}    ${myTimeout}
    log.Debug                       Saisir sur l'image ${myImage}: ${myText}
    autowin.Cliquer Et Saisir Le Texte      ${myText}


Saisir Un Secret Sur L'Image
    [Documentation]     Saisir le texte sur la position de l'image sans trace.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``myText``            est le texte à saisir sur l'image.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${myText}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher L'Image    ${myImage}    ${mySimilarity}    ${myTimeout}
    log.Debug                   Saisir le secret sur l'image ${myImage}
    autowin.Cliquer Et Saisir Le Texte    ${myText}


Cliquer Et Saisir Le Texte
    [Documentation]     Saisir le texte sur la dernière image trouvée.
    ...
    ...                 *Arguments :*
    ...                 - ``myText``    est le texte à saisir sur l'image.
    [Arguments]         ${myText}

    # robustesse - selectionner l'image avant de saisir
    opencv.Click On Last Image
    Sleep    2s
    # Saisir le texte
    opencv.Send Keys    ${myText}


Cliquer Sur L'Image Et Sortir En Erreur
    [Documentation]     Cliquer sur une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher L'Image    ${myImage}    ${mySimilarity}    ${myTimeout}
    # Click
    log.Debug               Clic sur l'image ${myImage}
    ${passed}=              Run Keyword And Return Status    opencv.Click On Last Image
    Run Keyword Unless      ${passed}    autowin.Capturer Et Sortir En Erreur
    autowin.Capturer L'Ecran


L'Image Doit Etre Presente
    [Documentation]     Vérifier la presence de l'image et stopper si elle n'est pas trouvée.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    log.Info                Attente de l'image ${myImage}
    ${path2image}=          Set Variable    ${OPENCV_SETTINGS.path2images}/${myImage}
    ${passed}=              Run Keyword And Return Status    opencv.Find Image    ${path2image}    ${myTimeout}    ${mySimilarity}
    Run Keyword Unless      ${passed}    fail    autowin.L'Image Ne Doit Pas Exister
    log.Success             Image trouvee !


Capturer Et Sortir En Erreur
    [Documentation]     Effectuer une capture d'écran.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``   est l'image recherchée.
    [Arguments]         ${myImage}

    opencv.Screenshot It
    Fatal Error    l'image ${myImage} n'a pas ete trouvee


Cliquer Sur L'Image
    [Documentation]     Cliquer sur une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher L'Image    ${myImage}    ${mySimilarity}    ${myTimeout}
    # Click
    log.Debug           Clic sur l'image ${myImage}
    ${passed}=          Run Keyword And Return Status    opencv.Click On Last Image
    Run Keyword If      ${passed}    autowin.Capturer L'Ecran


Cliquer Sur Deux Images Possibles
    [Documentation]     Cliquer sur une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage1``          est une des deux images possibles.
    ...                 - ``myImage2``          est une des deux images possibles.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage1}    ${myImage2}    ${mySimilarity}=${SIMILARITY}    ${myTimeout}=${TIMEOUT}

    log.Info                Clic sur ${myImage1} ou ${myImage2}
    ${passed}=              Run Keyword And Return Status    autowin.Cliquer Sur L'Image Et Sortir En Erreur    ${myImage1}    ${myTimeout}    ${mySimilarity}
    Run Keyword Unless      ${passed}    autowin.Cliquer Sur L'Image Et Sortir En Erreur    ${myImage2}    ${myTimeout}    ${mySimilarity}
    autowin.Capture L'Ecran


Glisser Deposer
    [Documentation]     Réaliser un "glisser déposer" d'une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myStartingImage``   est l'image de départ.
    ...                 - ``myEndingImage``     est l'image d'arrivée.
    [Arguments]         ${myStartingImage}    ${myEndingImage}

    # Saisir le texte
    opencv.Drag And Drop    ${myStartingImage}    ${myEndingImage}


Effacer Une Ancienne Valeur D'Un Champ
    [Documentation]    Cliquer sur une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher L'Image   ${myImage}    ${mySimilarity}    ${myTimeout}
    # Click
    log.Debug           Clic sur l'image ${myImage}
    ${passed}=          Run Keyword And Return Status    opencv.Click On Last Image
    Run Keyword If      ${passed}    opencv.Send Keys    {Ctrl Down}a{Ctrl Up}
    opencv.Send Keys    {Del}


Taper Le Texte
    [Documentation]     Taper le texte directement.
    ...
    ...                 *Arguments :*
    ...                 - ``myText``    est le texte à saisir.
    [Arguments]         ${myText}

    # taper le texte
    opencv.Send Keys    ${myText}


Trier Humainement Les Captures
    [Documentation]     Permettre de trier les captures d'écran en respectant l'ordre numérique.
    ...
    ...                 *Arguments :*
    ...                 - ``myScreenShotList``      est la liste des captures d'ecran prises.
    [Arguments]         ${myScreenShotList}

    # trier humainement
    opencv.Sort Screenshot List    ${myScreenShotList}


L'Image Ne Doit Pas Exister
    [Documentation]     Vérifier la présence de l'image et stopper si elle n'est pas trouvée.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``       est l'image recherchée.
    ...                 - ``mySimilarity``  est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    log.Info            Attente de l'image ${myImage}
    ${path2image}=      Set Variable    ${OPENCV_SETTINGS.path2images}/${myImage}
    ${passed}=          Run Keyword And Return Status    opencv.Find Image    ${path2image}    ${myTimeout}    ${mySimilarity}
    Run Keyword If      ${passed}    fail    autowin.L'Image Doit Etre Presente
    log.Success         Image Absente!


Double Cliquer Sur L'Image
    [Documentation]     Double-cliquer sur une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myImage``       est l'image recherchée.
    ...                 - ``mySimilarity``  est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``     est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher L'Image    ${myImage}    ${mySimilarity}    ${myTimeout}
    # Click
    log.Debug           Clic sur l'image ${myImage}
    ${passed}=          Run Keyword And Return Status    opencv.Double Click On Last Image
    Run Keyword If      ${passed}    autowin.Capturer L'Ecran


Double Cliquer Dans Region Sur L'Image
    [Documentation]     Double cliquer sur une image située dans une région précise.
    ...
    ...                 *Arguments :*
    ...                 - ``myRegion``          est la zone où doit se trouver l'image.
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myRegion}    ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher Dans Region L'Image    ${myRegion}    ${myImage}    ${mySimilarity}    ${myTimeout}
    # Click
    log.Debug           Double-cliquer sur l'image ${myImage}
    ${passed}=          Run Keyword And Return Status    opencv.Double Click On Last Image
    Run Keyword If      ${passed}    autowin.Capturer L'Ecran


Chercher Dans Region L'Image
    [Documentation]     Attendre l'apparition d'une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myRegion``          est la zone où doit se trouver l'image.
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myRegion}    ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    log.Debug    Attente de l'image ${myImage}
    # ajouter la racine des images
    ${path2image}=      Set Variable    ${OPENCV_SETTINGS.path2images}/${myImage}
    ${path2region}=     Set Variable    ${OPENCV_SETTINGS.path2images}/${myRegion}
    opencv.Find Image In Region         ${path2region}    ${path2image}    ${myTimeout}    ${mySimilarity}


Cliquer Dans Region Sur L'Image
    [Documentation]     Cliquer sur une image.
    ...
    ...                 *Arguments :*
    ...                 - ``myRegion``          est la zone où doit se trouver l'image.
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myRegion}    ${myImage}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${timeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher Dans Region L'Image    ${myRegion}    ${myImage}    ${mySimilarity}    ${myTimeout}
    # Click
    log.Debug           Clic sur l'image ${myImage}
    ${passed}=          Run Keyword And Return Status    opencv.Click On Last Image
    Run Keyword If      ${passed}    autowin.Capturer L'Ecran


Saisir Dans Region Sur L'Image
    [Documentation]     Saisir le texte sur la position de l'image avec une trace.
    ...
    ...                 *Arguments :*
    ...                 - ``myRegion``          est la zone où doit se trouver l'image.
    ...                 - ``myImage``           est l'image recherchée.
    ...                 - ``myText``            est le texte à saisir sur l'image.
    ...                 - ``mySimilarity``      est le degré de similarité acceptable de reconnaissance.
    ...                 - ``myTimeout``         est le temps d'attente maximum pour trouver une image ressemblante.
    [Arguments]         ${myRegion}    ${myImage}    ${myText}    ${mySimilarity}=${OPENCV_SETTINGS.similarity}    ${myTimeout}=${OPENCV_SETTINGS.timeout}

    autowin.Chercher Dans Region L'Image    ${myRegion}    ${myImage}    ${mySimilarity}    ${myTimeout}
    log.Debug           Saisir sur l'image  ${myImage}: ${myText}
    autowin.Cliquer Et Saisir Le Texte      ${myText}


Fermer Le Programme Plugin Container Si Necessaire
    [Documentation]     Fermer le programme.

    # tente de fermer le plugin container
    # si succes on recommence jusqu'a ce qu'on en trouve un de plus, maximum 3 fois
    # si pas trouve on sort de la boucle
    FOR    ${iterator}    IN RANGE    4
        ${foundIt}=    Run Keyword And Return Status    autowin.Cliquer Sur Fermer Le Programme Plugin Container
	    Exit For Loop If    ${foundIt}!=${TRUE}
    END


Cliquer Sur Fermer Le Programme Plugin Container
    [Documentation]     Cliquer sur fermer le programme.

    ${OPENCV_SETTINGS.path2images}=    Set Variable    ${CURDIR}${/}..${/}resources${/}images
    # on attend l'apparition de l'image 10 secondes par defaut
    autowin.Cliquer Sur L'Image Dans Region    pluginContainer.png    pluginContainerFermer.png
