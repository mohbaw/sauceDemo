# -*- coding: utf-8 -*-
*** Settings ***
Documentation       Ressource de gestion des fichiers JSON.
...
...                 Cette librairie dispose de quelques mots-clés permettant de manipuler
...                 des fichiers JSON.
...
...                 Avec cette librairie, il est possible d'envoyer des données issues d'un
...                 JSON dans un dictionnaire Python et réciproquement. De plus, elle permet
...                 aussi de convertir des chaînes de caractères au format JSON.

Library             JSONLibrary
Library             JsonValidator
Library             String
Library             Collections
Library             ../utils/JsonComparatorLibrary.py

Resource            utils.robot

*** Keywords ***
Convertir En Dictionnaire
    [Documentation]     Convertir une chaîne de caractères en un dictionnaire Python.
    ...
    ...                 *Arguments :*
    ...                 - ``myJsonString``      est la chaîne de caractères à convertir.
    ...                 *Return* : le dictionnaire Python.
    [Arguments]         ${myJsonString}

    #${type}=        Evaluate    str(type(${myJsonString}))
    ${dicData}=     Evaluate    json.loads('''${myJsonString}''')    json
    #${type}=        Evaluate    str(type(${dicData}))

    [Return]    ${dicData}

Convertir En Json
    [Documentation]     Convertir une chaîne de caractères en un jeu de données JSON.
    ...
    ...                 *Arguments :*
    ...                 - ``myJsonString``      est la chaîne de caractères à convertir.
    ...                 *Return* : le jeu de données JSON.
    [Arguments]         ${myJsonString}

    ${type}=        Evaluate    str(type(${myJsonString}))
    ${jsonData}=    Evaluate    json.dumps('''${myJsonString}''')    json
    ${type}=        Evaluate    str(type(${jsonData}))

    [Return]    ${jsonData}

Obtenir La valeur De La Cle Du Json
    [Documentation]     Récupérer une valeur selon une clé donnée dans le fichier JSON.
    ...
    ...                 *Arguments :*
    ...                 - ``myJsonString``      est le cotenu du fichier JSON à parcourir.
    ...                 - ``myKey``             est la clé recherchée.
    ...                 - ``isWithStrip``       si le résultat doit être découpé.
    ...                 *Return* : la valeur de la clé cherchée.
    [Arguments]         ${myJsonString}    ${myKey}    ${isWithStrip}=True

    ${jsonObject}=      JSONLibrary.Convert String to JSON    ${myJsonString}
    ${listValue}=       JSONLibrary.Get Value From Json       ${jsonObject}    $..${myKey}
    ${isEmpty}=         Get Length    ${listValue}
    Run Keyword If      ${isEmpty} == 0      Log.Error        La cle ${myKey} est absente de la reponse ${myJsonString}
    ${value}=           Get From List       ${listValue}    0
    ${isString}=        Run Keyword And Return Status       Should Be String    ${value}
    # Si c'est une chaine on prend le premier mot
    ${stripValue}=      Run Keyword If    ${isWithStrip} and ${isString}    utils.Obtenir Le Premier Mot    ${value}
    ...                           ELSE                                      json.convertir les variables    ${value}

    [Return]    ${stripValue}

convertir les variables
    [Documentation]    Convertir le type de variable de resultat du Json
    ...
    ...                 *Arguments :*
    ...                 - ``value``     est la valeur à convertir.
    [Arguments]    ${value}
    ${isBoolean}=    Run Keyword And Return Status    Should Be Boolean    ${value}

    ${Values}=    Convert To String    ${value}
    # Cas d'une valeur boolean
    # TODO à  améliorer
    ${BoolValueTrue}=     Run Keyword And Return Status    Should Be Equal    ${Values}     True
    ${BoolValueFalse}=    Run Keyword And Return Status    Should Be Equal    ${Values}     False
    # On force le lowercase pour correspondre au fichier Excel
    ${stripValue}=    Run Keyword If   ${BoolValueTrue} or ${BoolValueFalse}    Convert To Lowercase    ${Values}
    # Cas du token
    ...                         ELSE                                            json.convertir le variable decimal    ${value}

    [Return]    ${stripValue}

convertir le variable decimal
    [Arguments]    ${value2}    ${withStrip}=True

    ${isInteger}=    Run Keyword And Return Status    Should Not Be String    ${value2}
    ${stripValue}=   Run Keyword If     ${withStrip} or ${isInteger}    Convert To String    ${value2}
    ...                        ELSE                                     json.convertir le variable integer    ${requestId}
    [Return]    ${stripValue}

convertir le variable integer
    [Arguments]    ${requestId}    ${withStrip}=True

    ${isInteger}=    Run Keyword And Return Status    Should Not Be String    ${requestId}
    ${stripValue}=   Run Keyword If     ${withStrip} or ${isInteger}    Set Variable    ${requestId}
    [Return]    ${stripValue}

Extraire Les Donnees Du Json Dans Le Dictionnaire
    [Documentation]     Extraire les données d'un fichier JSON dans un dictionnaire Python.
    ...
    ...                 *Arguments :*
    ...                 - ``jsonResponse``                              est le fichier JSON.
    ...                 - ``extractAtJsonPathForGivenKeyDictionary``    est la carte des clés avec le chemin JSON à extraire.
    ...                 - ``targetedDictionnary``                       est le dictionnaire visé.
    ...                 - ``isWithStrip``                               si le résultat doit être découpé (prendre le premier mot).
    [Arguments]         ${jsonResponse}   ${extractAtJsonPathForGivenKeyDictionary}    ${targetedDictionnary}    ${isWithStrip}=True

    FOR    ${key}    IN    @{extractAtJsonPathForGivenKeyDictionary.keys()}
        ${jsonPath}=            Set Variable    ${extractAtJsonPathForGivenKeyDictionary.${key}}
        ${jsonPathValue}=       json.Obtenir La valeur De La Cle Du Json    ${jsonResponse}    ${jsonPath}    ${isWithStrip}
        Log    ${key} // ${jsonPath} // ${jsonPathValue}
        Set To Dictionary   ${targetedDictionnary}    ${key}=${jsonPathValue}
    END

Extraire Les Donnees Du Json Dans Le Dictionnaire Pour Une Liste De Paires
    [Documentation]     Extraire les données d'un fichier JSON dans un dictionnaire Python.
    ...
    ...                 *Arguments :*
    ...                 - ``jsonResponse``                          est le fichier JSON.
    ...                 - ``extractAtJsonPathForGivenKeyList``      est la carte des clés avec le chemin JSON à extraire.
    ...                 - ``targetedDictionnary``                   est le dictionnaire visé.
    [Arguments]         ${jsonResponse}   ${extractAtJsonPathForGivenKeyList}    ${targetedDictionnary}

    ${extractAtJsonPathForGivenKeyDictionary}=    utils.Creer Le Dictionnaire Pour Une Liste De Paires    ${extractAtJsonPathForGivenKeyList}
    json.Extraire Les Donnees Du Json Dans Le Dictionnaire    ${jsonResponse}   ${extractAtJsonPathForGivenKeyDictionary}    ${targetedDictionnary}

Obtenir Les valeurs De La Cle Du JsonPath
    [Documentation]     Récupérer une valeur selon une clé donnée dans le fichier JSON.
    ...
    ...                 *Arguments :*
    ...                 - ``myJsonString``      est le cotenu du fichier JSON à parcourir.
    ...                 - ``myKey``             est la clé recherchée.
    ...                 *Return* : la valeur de la clé cherchée.
    [Arguments]         ${myJsonString}    ${myJsonPath}

    ${jsonObject}=      JSONLibrary.Convert String to JSON    ${myJsonString}
    @{resultValue}=       JSONLibrary.Get Value From Json       ${jsonObject}    ${myJsonPath}

    #Log To Console  @{resultValue}

    [Return]    @{resultValue}

Obtenir la valeur d'un élément via son JsonPath
    [Arguments]     ${json}    ${jsonPath}  ${activeWarn}=true

    ${value}=   Set Variable

    ${jsonObject}=      JSONLibrary.Convert String to JSON    ${json}
    ${status}   ${values}=     Run Keyword And Ignore Error  JSONLibrary.Get Value From Json     ${jsonObject}    ${jsonPath}
    Return From Keyword If     '${status}' == 'FAIL'     JSONPATH_NOT_FOUND
    ${status}   ${value}=      Run Keyword And Ignore Error     Get From List  ${values}  0
    Run Keyword If      '${status}'=='FAIL' and '${activeWarn}'=='true'     Log.Warning       SocleAutomatisation/json : La cle '${jsonPath}' est absente de la reponse
    Return From Keyword If     '${status}' == 'FAIL'     JSONPATH_NOT_FOUND

    [Return]  ${value}

Parser un fichier Json
    [Arguments]     ${filePath}
    [Documentation]     Parse un fichier Json
    ...
    ...                 *Arguments :*
    ...                       - ``filePath`` : le fichier Json à parser
    ...
    ...                 *Return :*
    ...                       - ``json`` : Json sous forme de dictionnaire/liste
    ...
    ...                 *Exemple d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Fichier=  |
	...					| ${json}= |  json.Parser un fichier Json  | C:${/}Documents${/}exemple.json  |

    ${json}=    JSONLibrary.Load JSON From File     ${filePath}
    [Return]    ${json}

Indenter un flux Json
    [Arguments]     ${jsonString}
    ${prettyJson}=    Set Variable  ${EMPTY}
    ${statusPretty}     ${prettyJson}=    Run Keyword And Ignore Error       JsonValidator.Pretty Print Json     ${jsonString}
    ${prettyJsonString}=    Set Variable If  '${statusPretty}'=='PASS'      ${prettyJson}     ${EMPTY}
    [Return]    ${prettyJsonString}

Comparer deux Json
    [Arguments]  ${fluxRef}  ${fluxTest}  ${activateLogs}=${False}  @{exclusions}
    @{result}=  JsonComparatorLibrary.Comparer Flux Json  ${fluxRef}  ${fluxTest}  ${activateLogs}  @{exclusions}
    [Return]  @{result}

Comparer deux flux Json
    [Arguments]     ${fluxRef}    ${fluxTest}   ${exclusions}=@{EMPTY}  ${hasAmountThreshold}=${False}  ${activateLogs}=${False}
    [Documentation]     Compare deux flux Json
    ...
    ...                 *Arguments :*
    ...                       - ``fluxRef`` : le flux Json de référence
    ...                       - ``fluxTest`` : le flux Json à tester
    ...                       - ``exclusions (facultatif)`` : liste d'exclusion contenant les "JsonPath" des champs à exclure de la comparaison
    ...
    ...                 *Return :*
    ...                       - ``erreurs`` : la liste des divergences constatées
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Flux de référence=  | =Flux de test= | =Liste d'exclusion= |
	...					| @{divergences}= | json.Comparer deux flux Json | ${fluxRef} | ${fluxTest} | ${exclusions} |

	Set Test Variable   ${exclusions}
	Set Test Variable   ${hasAmountThreshold}
	${amountThreshold}=    Get Environment Variable    SEUIL_MONTANTS    10
	Set Test Variable   ${amountThreshold}
    Set Test Variable   @{erreurs}  @{EMPTY}

    #json.comparer deux objets    ${fluxRef}    ${fluxTest}   $
    #On shunte un mot clé par rapport à la limite de Keywords actifs avec RobotFramework
    #La fonction de comparaison serait à re-développer en Python
    ${status}  ${objectType}=  Run keyword and ignore error   Evaluate  type(${fluxRef})
    Run Keyword If      "${objectType}" == "<class 'list'>"    json.comparer deux listes    ${fluxRef}   ${fluxTest}   $
    ...     ELSE IF     "${objectType}" == "<class 'dict'>"   json.comparer deux dictionnaires  ${fluxRef}   ${fluxTest}   $
    ...     ELSE        json.comparer deux champs    ${fluxRef}  ${fluxTest}   ${jsonPath}

    Run Keyword If  @{erreurs}!=@{EMPTY} and "${activateLogs}"=="${True}"     lister les divergences
    [Return]    @{erreurs}

comparer deux objets
    [Arguments]     ${objRef}    ${objTest}   ${jsonPath}
    ${status}  ${objectType}=  Run keyword and ignore error   Evaluate  type(${objRef})
    Run Keyword If      "${objectType}" == "<class 'list'>"    json.comparer deux listes    ${objRef}   ${objTest}   ${jsonPath}
    ...     ELSE IF     "${objectType}" == "<class 'dict'>"   json.comparer deux dictionnaires  ${objRef}   ${objTest}   ${jsonPath}
    ...     ELSE        json.comparer deux champs    ${objRef}  ${objTest}   ${jsonPath}

comparer deux champs
    [Arguments]  ${objRef}  ${objTest}  ${jsonPath}
    ${jsonKey}=  Fetch From Right  ${jsonPath}  .
    ${amountStatus}  ${error}=  Run keyword and ignore error    Should Contain Any   ${jsonKey}  Amount  amount  SurrenderValue
    ${clauseStatus}  ${error}=  Run keyword and ignore error    Should Contain Any   ${jsonKey}  clauseText
    ${isAmount}=  Set Variable If  '${amountStatus}'=='PASS'  ${True}  ${False}
    ${isClause}=  Set Variable If  '${clauseStatus}'=='PASS'  ${True}  ${False}
    IF  ${hasAmountThreshold} and ${isAmount}
        ${valeurAttendue}=  Evaluate  abs(float(${objRef}))
        ${valeurObtenue}=  Evaluate  abs(float(${objTest}))

        # Calcul de la marge d'erreur 10% par défaut
        ${pourcentageErreur}=  Evaluate  ${amountThreshold}/100
        ${margeErreur}=  Evaluate  ${valeurAttendue}*${pourcentageErreur}

        # Calcul des bornes pour la comparaison
        ${margeInferieure}=  Evaluate  ${valeurAttendue}-${margeErreur}
        ${margeSuperieure}=  Evaluate  ${valeurAttendue}+${margeErreur}

        ${comparaison}=  Evaluate  ${margeInferieure} <= ${valeurObtenue} <= ${margeSuperieure}
        ${status}   ${erreur}=  Run keyword and ignore error    Should Be True  ${comparaison}  msg=${jsonPath} : La valeur obtenue (${valeurObtenue}) ne respecte pas la marge de %{MY_POURCENTAGE_MARGE_ERREUR}% par rapport à l'attendu (${valeurAttendue}): ${margeInferieure} <---> ${margeSuperieure}
        Run keyword if    '${status}'=='FAIL'   Append To List  ${erreurs}  ${erreur}
        Return From Keyword
    ELSE IF  ${isClause}
        ${refClauseMultispacesUnless}=    Replace String Using Regexp   ${objRef}  ${SPACE}+  ${SPACE}
        ${testClauseMultispacesUnless}=    Replace String Using Regexp   ${objTest}  ${SPACE}+  ${SPACE}
        ${refClause}=  Strip String  ${refClauseMultispacesUnless}  mode=right  characters=.
        ${testClause}=  Strip String  ${testClauseMultispacesUnless}  mode=right  characters=.
        ${objRef}=  Set Variable    ${refClause}
        ${objTest}=  Set Variable  ${testClause}
    END
    ${donneeExclue}   ${excl}=  Run keyword and ignore error    List Should Contain Value   ${exclusions}    ${jsonPath}
    ${status}   ${erreur}=  Run keyword and ignore error    Should be equal	 ${objRef}  ${objTest}  msg=La valeur ${jsonPath} obtenue (${objTest}) est différente de la valeur attendue (${objRef})
    Run keyword if    '${status}'=='FAIL' and '${donneeExclue}'=='FAIL'   Append To List  ${erreurs}  ${erreur}

comparer deux listes
    [Arguments]  ${listeRef}    ${listeTest}    ${jsonPath}
    FOR  ${objectRef}   IN  @{listeRef}
        ${index}=  Get Index From List  ${listeRef}  ${objectRef}
        ${status}   ${objectTest}=  Run keyword and ignore error      Get From List  ${listeTest}  ${index}
        ${extendedJsonPath}=   Catenate  SEPARATOR=${EMPTY}    ${jsonPath}    [${index}]
        #Run Keyword If  '${status}'=='PASS'  json.comparer deux objets  ${objectRef}  ${objectTest}  ${extendedJsonPath}
        #        ...     ELSE    Append To List  ${ERREURS}  L'objet ${jsonPath} est présent dans l'attendu mais absent de l'obtenu
        ${statusType}  ${objectType}=  Run keyword and ignore error   Evaluate  type(${objectRef})
        Run Keyword If  '${status}'!='PASS'     Append To List  ${ERREURS}  L'objet ${jsonPath} est présent dans l'attendu mais absent de l'obtenu
        Run Keyword If  '${status}'=='PASS' and "${objectType}" == "<class 'list'>"  json.comparer deux listes    ${objectRef}   ${objectTest}   ${extendedJsonPath}
        ...    ELSE IF  '${status}'=='PASS' and "${objectType}" == "<class 'dict'>"  json.comparer deux dictionnaires  ${objectRef}   ${objectTest}   ${extendedJsonPath}
        ...    ELSE        json.comparer deux champs    ${objectRef}  ${objectTest}   ${extendedJsonPath}
    END

comparer deux dictionnaires
    [Arguments]  ${dictRef}    ${dictTest}   ${jsonPath}
    @{paths}=   Get Dictionary Keys     ${dictRef}

    FOR  ${path}   IN  @{paths}
        ${extendedJsonPath}=   Catenate  SEPARATOR=.    ${jsonPath}    ${path}
        ${attendu}=  Get From Dictionary    ${dictRef}     ${path}
        ${etat}     ${obtenu}=  Run keyword and ignore error    Get From Dictionary    ${dictTest}     ${path}
        ${donneeExclue}   ${excl}=  Run keyword and ignore error    List Should Contain Value   ${exclusions}    ${extendedJsonPath}
        Run Keyword If  '${etat}'=='FAIL' and '${donneeExclue}'=='FAIL'   Append To List  ${erreurs}  Le champ ${extendedJsonPath} n'est pas présent dans l'environnement de test (valeur sous env reférence : ${attendu})
        Continue For Loop If  '${etat}'=='FAIL'
        #json.comparer deux objets  ${attendu}  ${obtenu}  ${extendedJsonPath}
        ${status}  ${objectType}=  Run keyword and ignore error   Evaluate  type(${attendu})
        Run Keyword If      "${objectType}" == "<class 'list'>"    json.comparer deux listes    ${attendu}   ${obtenu}   ${extendedJsonPath}
        ...     ELSE IF     "${objectType}" == "<class 'dict'>"   json.comparer deux dictionnaires  ${attendu}   ${obtenu}   ${extendedJsonPath}
        ...     ELSE        json.comparer deux champs    ${attendu}  ${obtenu}   ${extendedJsonPath}
    END

lister les divergences
    log to console  ${EMPTY}
    FOR  ${divergence}  IN  @{erreurs}
        log.ERROR   ${divergence}
    END

Modifier la valeur d'un élément
    [Arguments]         ${json}  ${jsonPath}  ${newValue}
    [Documentation]     Modifie la valeur par ``newValue`` d'un élément dans le ``json`` à partir de son ``jsonPath``

    ${modifiedJson}=     JSONLibrary.Update Value To Json	${json}  ${jsonPath}  ${newValue}

    [Return]    ${modifiedJson}

Modifier un fichier Json
    [Documentation]  Modifie un fichier Json à partir d'un dictionnaire des champs à modifier
    ...
    ...                 *Arguments :*
    ...                       - ``pathFile`` : le flux Json de référence
    ...                       - ``values`` : le dictionnaire des clés/valeurs nécessaires à la modification
    ...                              une clés étant le jsonPath de l'élément à modifier, et sa valeur la nouvelle valeur à affecter
    ...
    ...                 *Return :*
    ...                       - ``json`` : le json modifié sous forme de dictionnaires/listes
    ...
    ...                 *Exemples d'utilisation :*
	...					| =Résultat= |  =Mot clé=  | =Flux de référence=  | =Flux de test= | =Liste d'exclusion= |
	...					| @{divergences}= | json.Comparer deux flux Json | ${fluxRef} | ${fluxTest} | ${exclusions} |
    [Arguments]  ${pathFile}  ${values}

    ${json}=     json.Parser un fichier Json        filePath=${pathFile}
    @{jsonPaths}=  Get Dictionary Keys  ${values}
    FOR  ${jsonPath}  IN  @{jsonPaths}
        ${value}=   Get From Dictionary  ${values}  ${jsonPath}
        ${json}=   Run Keyword If  '${value}'!='JSONPATH_NOT_FOUND'   json.Modifier la valeur d'un élément    ${json}   ${jsonPath}   ${value}
        ...   ELSE  json.Modifier la valeur d'un élément    ${jsonRequestContext}   ${jsonPath}   ${EMPTY}
    END

    [Return]  ${json}

Ajouter un élément
    [Arguments]         ${json}  ${jsonPath}  ${newValue}
    [Documentation]     Ajoute un élément dans la position ``jsonPath`` avec la valeur ``newValue`` dans le ``json``

    ${modifiedJson}=     JSONLibrary.Add Object To Json  ${json}  ${jsonPath}  ${newValue}

    [Return]    ${modifiedJson}

Valoriser Un Élément
    [Arguments]         ${json}  ${jsonPath}  ${newValue}
    [Documentation]  Valorise un élément dans la position ``jsonPath`` avec la valeur ``newValue`` dans le ``json``

    @{elements}=  Split String  ${jsonPath}  .
    Remove From List  ${elements}  0
    ${parent}=  Set Variable  $
    ${pathSoFar}=  Set Variable  $

    FOR  ${element}  IN  @{elements}
        ${pathSoFar}=  Set Variable  ${pathSoFar}.${element}
        ${status}  ${curValue}=  Run Keyword And Ignore Error
        ...  JsonLibrary.Get Value From JSON  ${json}  ${pathSoFar}

        ${valueFound}=  Evaluate  '${status}'=='PASS'
        ${valueIsEmptyList}=  Run Keyword And Return Status
        ...  Should Be Equal  '${curValue}'  '@{EMPTY}'

        IF  (not ${valueFound}) or ${valueIsEmptyList}
            ${jsonObject}=  JsonLibrary.Convert String To JSON  {"${element}": null}
            ${json}=  JsonLibrary.Add Object To JSON  ${json}  ${parent}  ${jsonObject}
            ${json}=  JsonLibrary.Update Value To JSON  ${json}  ${parent}  ${jsonObject}
        END

        ${parent}=  Set Variable  ${pathSoFar}
    END

    ${status}  ${newValueAsJson}=  Run Keyword And Ignore Error
    ...  Convert String To JSON  ${newValue}

    IF  '${status}'=='PASS'
        ${json}=  Update Value To Json  ${json}  ${jsonPath}  ${newValueAsJson}
    ELSE
        ${json}=  Update Value To Json  ${json}  ${jsonPath}  ${newValue}
    END

    [Return]    ${json}

Supprimer un objet dans le Json
    [Arguments]     ${json}     ${jsonPath}
    [Documentation]  Supprime l'objet en position ``jsonPath`` dans le ``json`

    ${newJson}  JSONLibrary.Delete Object From Json    ${json}     ${jsonPath}

    [Return]    ${newJson}