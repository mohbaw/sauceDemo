*** Settings ***
Resource    ../../resources/socle/web2.robot

*** Variables ***

# cette section  n'est pas destinée à rester...
# c'est le role du datadriven de setter ces variables en tache de fond (job background)
&{DONNEES}
...    USER_NAME=standard_user
...    PWD=secret_sauce
...    FIRST_NAME=Mohamed
...    LAST_NAME=Baouab
...    ZIP=44300
...    NOM_ARTICLE=
...    PRIX_ARTICLE=



*** Keywords ***
SCE Test Setup
    Eclater mon dictionnaire
    log.Success    TEST SETUP SUCEEDED !

Eclater mon dictionnaire
# Log    "@{DONNEES}"
    # Log    "&{DONNEES}"
    # Log    "${DONNEES}"

    # FOR  ${variable}  IN  &{DONNEES}
    #     log to console  ${variable}[0]
    #     log to console  ${variable}[1]
    #     Set test variable    ${variable}
    #     Log to console    ${variable}
    # END
    
    FOR  ${variable}  IN  @{DONNEES}
        Log to console    "key: ${variable}"
        Log to console    "val: ${DONNEES}[${variable}]"
        Set Test Variable  ${${variable}}    ${DONNEES}[${VARIABLE}]
    END
    Log variables

SCE Test Teardown
    web2.Fermer Tous Navigateurs
    log.Success    TEST TEARDOWN SUCEEDED !