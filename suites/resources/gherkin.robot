*** Settings ***
# %ROBOT_DEPLOY% = c:\robot\test-socle-web2

# Resource        %{ROBOT_DEPLOY}/services/SCE/SCE.parcours.robot

# C'est la meme chose que lA ligne 4, mais ça évite d'avoir du rouge partout...
Resource        ../../services/SCE/SCE.parcours.robot

# Resource        %{ROBOT_DEPLOY}/services/EE/EE.parcours.robot : non implémentée

*** Keywords ***
# GIVEN
Le Client est authentifié
    SCE.parcours.S'authentifier

# WHEN
L'utilisateur ajoute un produit au panier
    SCE.parcours.Ajouter un produit au panier et le visualiser

# AND
L'utilisateur passe au paiement
    SCE.parcours.Passage au paiement

# # THEN
La commande est validée
    SCE.parcours.Commande validée