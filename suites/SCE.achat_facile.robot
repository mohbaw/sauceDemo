*** Settings ***

Resource    %{ROBOT_DEPLOY}/suites/resources/common.robot
Resource    %{ROBOT_DEPLOY}/suites/resources/gherkin.robot


Test Setup         common.SCE Test Setup
Test Teardown      common.SCE Test Teardown


Test Template    test de parcours


*** Keywords ***
test de parcours
    [Arguments]    ${username}
    # ${username}: useless but mandatory !!!
    GIVEN Le Client est authentifié
    WHEN L'utilisateur ajoute un produit au panier
    AND L'utilisateur passe au paiement
    THEN La commande est validée

*** Test Cases ***
Parcours passant    ${EMPTY}

#Parcours non passant