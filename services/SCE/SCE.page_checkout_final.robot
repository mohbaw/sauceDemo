*** Settings ***

Resource    ../../resources/socle/web2.robot

*** Keywords ***

Verifier que le Client est sur la page finale du checkout
    web2.Page Doit Contenir
    ...    texte=Checkout: Complete!
    web2.Capturer Ecran
    Log.Success    Je suis sur la page finale du checkout
