*** Settings ***

Resource    ../../resources/socle/web2.robot
Library     FakerLibrary

*** Keywords ***

Passer la premiere etape du checkout
    SCE.page_checkout_info.verifier que le Client est sur la page de chekout-Infos
    SCE.page_checkout_info.Saisir prenom Client
    SCE.page_checkout_info.Saisir nom Client
    SCE.page_checkout_info.Saisir CP Client
    Valider les infos de checkout saisies

verifier que le Client est sur la page de chekout-Infos
    web2.Page Doit Contenir
    ...    texte=Checkout: Your Information
    web2.Capturer Ecran
    Log.Success    Je suis sur la page du chekout-Infos

Saisir prenom Client
    ${prenom}=     FakerLibrary.First name
    web2.Saisir Dans Champ
    ...    locator=first-name
    ...    texte=${prenom}
    #...    texte=${DONNEES.FIRST_NAME} # variable située dans common.robot

Saisir nom Client
    ${nom}=     FakerLibrary.Last name
    web2.Saisir Dans Champ
    ...    locator=last-name
    ...    texte=${nom}
    #...    texte=${DONNEES.LAST_NAME} # variable située dans common.robot

Saisir CP Client
    ${cp}=    FakerLibrary.Postalcode
    web2.Saisir Dans Champ
    ...    locator=postal-code
    ...    texte=${cp}
    # ...    texte=${DONNEES.ZIP} # variable située dans common.robot

Valider les infos de checkout saisies
    web2.Cliquer Sur Element
    ...    locator=continue
