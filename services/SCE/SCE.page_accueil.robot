*** Settings ***

Resource    ../../resources/socle/web2.robot

*** Keywords ***


Le Client s'authentifie
    SCE.page_accueil.Verifier que le Client est sur la page d'Accueil
    SCE.page_accueil.Le Client rentre le username
    SCE.page_accueil.Le Client rentre le mot de passe
    SCE.page_accueil.Le Client valide ses identifiants

Verifier que le Client est sur la page d'Accueil
    web2.Ouvrir Navigateur
    ...    url=${SITE_URL.%{MY_ENV}}
    web2.Capturer Ecran
    Log.Success    Je suis sur la page d'accueil


Le Client rentre le username
    web2.Saisir Dans Champ    id:user-name    ${USER_NAME}

Le Client rentre le mot de passe
    web2.Saisir Dans Champ    id:password    ${PWD}

Le Client valide ses identifiants
    web2.Cliquer Sur Element      id:login-button

Verifier que le Client reste sur l'Acceuil suite erreur
    web2.Page Doit Contenir    Epic sadface
    web2.Capturer Ecran
    log.Success    On est resté sur la page d'acceuil avec un message d'erreur affiché


