*** Settings ***

Resource    ../../resources/socle/web2.robot

*** Keywords ***
Valider le panier
    SCE.page_panier.Verifier que le Client est sur la page panier
    SCE.page_panier.verifier que la reference et le prix de l'article sont conformes
    SCE.page_panier.Passer au checkout

Verifier que le Client est sur la page panier
    web2.Page Doit Contenir
    ...    texte=Your Cart
    web2.Capturer Ecran
    Log.Success    Je suis sur la page Panier

Passer au checkout
    web2.Cliquer Sur Element
    ...    locator=checkout

verifier que la reference et le prix de l'article sont conformes
    web2.Texte Element Doit Correspondre
    ...    locator=//div[@class='inventory_item_name'][1]
    ...    texte=${DONNEES.NOM_ARTICLE}

    web2.Texte Element Doit Correspondre
    ...    locator=//div[@class='inventory_item_price'][1]
    ...    texte=${DONNEES.PRIX_ARTICLE}
    log.Success    L'article ajouté est bien celui présent dans le panier, nom et prix vérifiés
