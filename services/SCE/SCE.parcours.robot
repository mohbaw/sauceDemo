*** Settings ***

Resource    ./SCE.page_accueil.robot
Resource    ./SCE.page_checkout_info.robot
Resource    ./SCE.page_checkout_final.robot
Resource    ./SCE.page_checkout_overview.robot
Resource    ./SCE.page_panier.robot
# Resource    ./SCE.page_produit.robot
Resource    ./SCE.page_produits.robot



*** Keywords ***
# GIVEN
S'authentifier
    SCE.page_accueil.Le Client s'authentifie


Ajouter un produit au panier et le visualiser
    SCE.page_produits.Remplir le panier et le visualiser

# WHEN
passage au paiement
    SCE.page_panier.Valider le panier
    SCE.page_checkout_info.Passer la premiere etape du checkout
    SCE.page_checkout_overview.Finaliser l'achat

# THEN
Commande validée
    SCE.page_checkout_final.Verifier que le Client est sur la page finale du checkout