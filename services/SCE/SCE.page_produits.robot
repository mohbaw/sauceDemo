*** Settings ***

Resource    ../../resources/socle/web2.robot


*** Keywords ***
Remplir le panier et le visualiser
    SCE.page_produits.Verifier que le Client est sur la page des produits
    SCE.page_produits.Tri Prix Croissant
    web2.Capturer Ecran
    SCE.page_produits.Ajouter un produit au panier
    SCE.page_produits.Acceder au panier

Verifier que le Client est sur la page des produits
    web2.Page Doit Contenir Element
    ...    locator=react-burger-menu-btn
    web2.Capturer Ecran
    Log.Success    On est sur la page des produits

Tri Prix Croissant
    web2.Selectionner Dans Liste
    ...    locator=//select[@data-test='product_sort_container']
    ...    option=Price (low to high)

Tri Prix decroissant
    web2.Selectionner Dans Liste
    ...    locator=//select[@data-test='product_sort_container']
    ...    option=Price (high to low)

Tri Alpha descendant
    web2.Selectionner Dans Liste
    ...    locator=//select[@data-test='product_sort_container']
    ...    option=Name (Z to A)

Ajouter un produit au panier
    web2.Cliquer Sur Element
    ...    locator=//button[contains(text(),'Add to cart')][1]
                    # Premier produit de la liste
    ${DONNEES.NOM_ARTICLE}=    web2.Obtenir Texte Element
    ...    locator=//div[@class='inventory_item_name'][1]
    ${DONNEES.PRIX_ARTICLE}=    web2.Obtenir Texte Element
    ...    locator=//div[@class='inventory_item_price'][1]
    log.Success    Le nom de l'article est: ${DONNEES.NOM_ARTICLE}
    log.Success    Le prix de l'article est: ${DONNEES.PRIX_ARTICLE}


Acceder au panier
    web2.Cliquer Sur Element
    ...    locator=//a[@class='shopping_cart_link']